<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

Class Appsession {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	private $CI;

	public function __construct() {
		$this->CI =& get_instance();
	}

	public function getSession() 
	{		
		return $this->CI->session->all_userdata('logged_in');
	}

	public function diesession() {
		die(var_dump($this->CI->session->all_userdata()));
	}

	public function setSession($data = array()) 
	{		
		$this->CI->session->set_userdata($data);
		return true;
	}

	public function sessionDestroy() {
		$this->CI->session->sess_destroy();
		return true;
	}

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */