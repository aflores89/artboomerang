<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

Class User {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */

	private $CI;

	public function __construct() {
		$this->CI =& get_instance();
		$this->CI->load->model('user_model');	
		$this->CI->load->library('appsession');			
	}	

	public function get($type) {
		$result = $this->CI->user_model->getUsersByUserType($type);
		return $result;
	}

	public function getUser() {	

		$userSession = $this->CI->appsession->getSession();
		$idUser = $userSession['idusers'];	
		$user = $this->CI->user_model->getUserById($idUser);
		return $user[0];

	}

	public function getUserById($idUser) {	
		$user = $this->CI->user_model->getUserById($idUser);		
		return $user[0];
	}

	public function saveUser($username,$password,$mail,$first_name,$last_name,$phone,$user_type) {
		$result = $this->CI->user_model->saveUser($username,$password,$mail,$first_name,$last_name,$phone,$user_type);
		if(!$result) {
			return false;
		} else {
			return $this->getUserById($result);
		}
	}

	public function updateUser($first_name,$last_name,$username,$phone,$mail,$iduser) {
		$result = $this->CI->user_model->updateUser($first_name,$last_name,$username,$phone,$mail,$iduser);
		return $result;
	}

	public function changePassword($password,$iduser) {
		$result = $this->CI->user_model->changePassword($password,$iduser);
		return $result;
	}

	public function getUserByUsernamePassword($username, $password) {
		$result = $this->CI->user_model->getUserByUsernamePassword($username,$password);
		return $result;
	}

	public function getUserByUsername($username) {
		$result = $this->CI->user_model->getUserByUsername($username);
		return $result;
	}

	public function getUserByMail($mail) {
		$result = $this->CI->user_model->getUserByMail($mail);
		return $result;
	}

	public function isLoggedIn() 
	{		
		$session = $this->CI->appsession->getSession();	
		if(isset($session['logged_in'])) {
			if($session['logged_in']) {
				return true;	
			} else {
				return false;
			}			
		} else {
			return false;
		}
	}

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */