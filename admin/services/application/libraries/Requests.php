<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

Class Requests {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */

	private $CI;

	public function __construct() {
		$this->CI =& get_instance();
		$this->CI->load->model('requests_model');	
		$this->CI->load->library('appsession');			
	}	

	public function get($iduser = '') {	
		$result = $this->CI->requests_model->get($iduser);
		if($result) {
			array_walk($result, function(&$value, $key) {
				$project = $this->CI->requests_model->getProjectById($value['idproject']);
				$value['project'] = $project[0]['name'];			
			});	
		}
			
		return $result;
		
	}

	public function save($first_name,$last_name,$mail,$phone,$project,$location,$consultation) {	

		$result = $this->CI->requests_model->add($first_name,$last_name,$mail,$phone,$project,$location,$consultation);
			
		return $result;
		
	}

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */