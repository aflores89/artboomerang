<?php
Class Projects_model extends CI_Model {

	public function get($id = '') {
		if(!empty($id)) {
			$this->db->where('idprojects', $id);
		}
		$result = $this->db->get('projects');
        if(!empty($result->result_array())) {
            return $result->result_array();
        } else {
            return false;
        }		
	}

	public function add($name) {
		$data = array(
			'name' => $name
		);
		$result = $this->db->insert('projects', $data);
		return $result;
	}

	public function edit($idproject,$name) {
		$data = array(
			'name' => $name
		);
		$this->db->where('idprojects', $idproject);
		$result = $this->db->update('projects', $data);
		return $result;
	}

	public function delete($idproject) {
		$this->db->where('idprojects', $idproject);
		$result = $this->db->delete('projects');
		return $result;
	}

	public function getProjectById($idproject) {
		$this->db->where('idprojects', $idproject);
		$result = $this->db->get('projects');
        if(!empty($result->result_array())) {
            return $result->result_array();
        } else {
            return false;
        }		
	}

	
	
}