<?php
Class Reports_model extends CI_Model {

	public function get($idreport = '') {
        if(!empty($idreport)) {
            $this->db->where('idreports', $idreport);
        }
		$result = $this->db->get('reports');
        if(!empty($result->result_array())) {
            return $result->result_array();
        } else {
            return false;
        }		
	}

    public function add($answer,$mail,$interested,$chanel,$medio,$observations,$idrequest) {
        date_default_timezone_set('America/Buenos_Aires');
        $data = array(
            'answer' => $answer,
            'mail' => $mail,
            'interested' => $interested,
            'chanel' => $chanel,
            'medio' => $medio,
            'observations' => $observations,
            'idrequest' => $idrequest
        );
        $result = $this->db->insert('reports', $data);
        return $result;
    }

	public function getProjectById($idproject) {
		$this->db->where('idprojects', $idproject);
		$result = $this->db->get('projects');
        if(!empty($result->result_array())) {
            return $result->result_array();
        } else {
            return false;
        }		
	}

    public function asignRequestToUser($iduser,$idrequests) {
        $data = array(
            'iduser' => $iduser,
        );
        $this->db->where('idrequests', $idrequests);
        $result = $this->db->update('requests', $data);        
        return $result; 
    }

    public function deleteAsign($idrequests) {
        $data = array(
            'iduser' => '',
        );
        $this->db->where('idrequests', $idrequests);
        $result = $this->db->update('requests', $data);        
        return $result; 
    }
	
}