<?php
Class Artistas_model extends CI_Model {


    function __construct() {
        parent::__construct();
    }

    public function saveArtista($nombre,$biografia,$statment,$url,$provincia,$filename,$filepath) {
        $data = array(
            'nombre' => $nombre,
            'biografia' => $biografia,
            'statment' => $statment,
            'url' => $url,
            'idprovincia' => $provincia,
            'filename' => $filename,
            'filepath' => $filepath
        );
        
        $result = $this->db->insert('artistas', $data);

        /*$this->addCantProvincias($provincia);*/

        if($result) {
            return $this->db->insert_id();
        } else {
            return false;
        }
    }

    /*public function addCantProvincias($id) {

        $this->db->where('idprovincias', $id);
        $result = $this->db->update('provincias');
        $prov = $result->result_array();

        $data = array(
            'cant' => $prov[0]['cant']+1
        );

        $this->db->where('idprovincias', $id);
        $resultInsert = $this->db->insert('provincias', $data);
        
        if($resultInsert) {
            return $this->db->insert_id();
        } else {
            return false;
        }
    }    */

    public function updateArtistaPerfil($idartista,$filename,$filepath) {

        $data = array(
            'filename' => $filename,
            'filepath' => $filepath
        );
        $this->db->where('idartistas', $idartista);
        $result = $this->db->update('artistas', $data);
        
        if($result) {
            return $this->db->insert_id();
        } else {
            return false;
        }

    }

    public function updateArtista($nombre,$biografia,$statment,$url,$idartista) {
        $data = array(
            'nombre' => $nombre,
            'biografia' => $biografia,
            'statment' => $statment,
            'url' => $url
        );
        $this->db->where('idartistas', $idartista);
        $result = $this->db->update('artistas', $data);
        
        if($result) {
            return $this->db->insert_id();
        } else {
            return false;
        }
    }

    public function getArtistas() {        
        $result = $this->db->get('artistas');
        if(!empty($result->result_array())) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function getArtistaById($id) {
        $this->db->where('idartistas', $id);
        $result = $this->db->get('artistas');
        if(!empty($result->result_array())) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function deleteArtista($idartista) {
        $this->db->where('idartistas', $idartista);
        $result = $this->db->delete('artistas');    
        return $result;     
    }

    public function getUserByMail($mail) {
        $data = array(
            'mail' => $mail
        );
        $this->db->where($data);
        $result = $this->db->get('users');
        if(!empty($result->result_array())) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function updateuser($first_name,$last_name,$username,$phone,$mail,$iduser) {
        $data = array(
            'first_name' => $first_name,
            'last_name' => $last_name,
            'username' => $username,
            'phone' => $phone,
            'mail' => $mail
        );

        $this->db->where('idusers', $iduser);
        $result = $this->db->update('users', $data);        
        return $result;
    }

    public function changePassword($password,$iduser) {
        $data = array(
            'password' => $password
        );

        $this->db->where('idusers', $iduser);
        $result = $this->db->update('users', $data);        
        return $result;
    }

    public function getUserByUsername($username) {
        $data = array(
            'username' => $username
        );
        $this->db->where($data);
        $result = $this->db->get('users');
        if(!empty($result->result_array())) {
            return $result->result_array();
        } else {
            return false;
        }
    }
    
    public function getUserById($idUser) {
        $data = array(
            'idusers' => $idUser
        );
        $this->db->where($data);
        $result = $this->db->get('users');
        if(!empty($result->result_array())) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function getUsersByUserType($user_type) {
        $data = array(
            'user_type' => $user_type
        );
        $this->db->where($data);
        $result = $this->db->get('users');
        if(!empty($result->result_array())) {
            return $result->result_array();
        } else {
            return false;
        }
    }


    public function getUserByUsernamePassword($username,$password) {

        $data = array(
            'username' => $username,
            'password' => $password
        );

        $this->db->where($data);
        $query = $this->db->get('users');
        $result = $query->result_array();
        $user = $result[0];

        $this->db->where('iduser_types',$user['user_type']);
        $query = $this->db->get('user_types');
        $result = $query->result_array();
        $user_type = $result[0]['type'];

        $user['user_type'] = $user_type;
        
        if(!empty($user)) {
            return $user;
        } else {
            return false;
        }
    }

    public function getLinks($id) {
        $result = $this->db->get('links');
        if(!empty($result->result_array())) {
            return $result->result_array();
        } else {
            return false;
        }
    }

}