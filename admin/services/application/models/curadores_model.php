<?php

Class Curadores_model extends CI_Model {

	public function getCuradores() {
        $this->db->order_by('orden', 'desc');
        $result = $this->db->get('curadores');
		if($result) {
			return $result->result_array();
		} else {
			return false;
		}

	}

	public function saveCurador($nombre,$extracto,$filepath, $subtitulo, $orden) {
		$data = array(
            'nombre' => $nombre,
            'extracto' => $extracto,
            'imagen' => $filepath,
            'invitado' => 1,
            'subtitulo' => $subtitulo,
            'orden' => $orden
        );
        $result = $this->db->insert('curadores', $data);
        if($result) {
            return $this->db->insert_id();
        } else {
            return false;
        }
	}

    public function updateCurador($nombre,$extracto,$idcuradores, $subtitulo, $orden) {
        $data = array(
            'nombre' => $nombre,
            'extracto' => $extracto,
            'subtitulo' => $subtitulo,
            'orden' => $orden
        );
        $this->db->where('idcuradores', $idcuradores);
        $result = $this->db->update('curadores', $data);
        
        if($result) {
            return $this->db->insert_id();
        } else {
            return false;
        }
    }

    public function updateCuradorPerfil($idcurador,$filename,$filepath) {

        $data = array(
            'imagen' => $filepath
        );
        $this->db->where('idcuradores', $idcurador);
        $result = $this->db->update('curadores', $data);

        if($result) {
            return $this->db->insert_id();
        } else {
            return false;
        }

    }

    public function getCuradorById($id) {
        $this->db->where('idcuradores', $id);
        $result = $this->db->get('curadores');
        if(!empty($result->result_array())) {
            return $result->result_array();
        } else {
            return false;
        }
    }

	public function active($id) {
		$data = array(
           'invitado' => 1              
        );

		$this->db->where('idcuradores', $id);
		$result = $this->db->update('curadores', $data); 

		if($result) {
            return true;
        } else {
            return false;
        }
	}

	public function inactive($id) {
		$data = array(
           'invitado' => 0             
        );

		$this->db->where('idcuradores', $id);
		$result = $this->db->update('curadores', $data); 

		if($result) {
            return true;
        } else {
            return false;
        }
	}

	public function delete($id) {
        $this->db->where('idcuradores', $id);
        $result = $this->db->delete('curadores');    
        return $result;     
    }
	
} 