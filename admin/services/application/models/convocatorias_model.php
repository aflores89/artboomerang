<?php

Class Convocatorias_model extends CI_Model {

	public function getConvocatorias() {
		$result = $this->db->get('convocatorias');

		if($result) {
			return $result->result_array();
		} else {
			return false;
		}

	}

	public function saveConvocatoria($nombre,$filepath) {
		$data = array(
            'nombre' => $nombre,
            'path' => $filepath,
            'activa' => 0
        );
        $result = $this->db->insert('convocatorias', $data);
        if($result) {
            return $this->db->insert_id();
        } else {
            return false;
        }
	}

	public function active($id) {
		$data = array(
           'activa' => 1              
        );

		$this->db->where('idconvocatorias', $id);
		$result = $this->db->update('convocatorias', $data); 

		if($result) {
            return true;
        } else {
            return false;
        }
	}

	public function inactive($id) {
		$data = array(
           'activa' => 0             
        );

		$this->db->where('idconvocatorias', $id);
		$result = $this->db->update('convocatorias', $data); 

		if($result) {
            return true;
        } else {
            return false;
        }
	}

	public function delete($id) {
        $this->db->where('idconvocatorias', $id);
        $result = $this->db->delete('convocatorias');    
        return $result;     
    }
	
} 