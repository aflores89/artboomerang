<?php
Class Requests_model extends CI_Model {

	public function get($iduser = '') {
        if(!empty($iduser)) {
            $this->db->where('iduser', $iduser);
        }
		$result = $this->db->get('requests');
        if(!empty($result->result_array())) {
            return $result->result_array();
        } else {
            return false;
        }		
	}

    public function add($first_name,$last_name,$mail,$phone,$project,$location,$consultation) {
        date_default_timezone_set('America/Buenos_Aires');
        $data = array(
            'first_name' => $first_name,
            'last_name' => $last_name,
            'mail' => $mail,
            'phone' => $phone,
            'idproject' => $project,
            'location' => $location,
            'consultation' => $consultation,
            'date' => date('d-m-Y'),
            'hour' => date('H:i:s')
        );
        $result = $this->db->insert('requests', $data);
        return $result;
    }

    public function getContenido() {
        $this->db->where('idcontenidos', 1);  
        $this->db->limit(1);  
        $query = $this->db->get('contenidos');
        $result = $query->result_array();

        if(isset($result[0]))
            return $result[0];
    }

    public function saveContenido($titulo,$subtitulo,$contenido) {
        $data = array(
            'titulo' => $titulo,
            'subtitulo' => $subtitulo,
            'contenido' => $contenido
        );
        $this->db->where('idcontenidos', 1);
        $result = $this->db->update('contenidos', $data);
        return $result;
    }

	public function getProjectById($idproject) {
		$this->db->where('idprojects', $idproject);
		$result = $this->db->get('projects');
        if(!empty($result->result_array())) {
            return $result->result_array();
        } else {
            return false;
        }		
	}

    public function asignRequestToUser($iduser,$idrequests) {
        $data = array(
            'iduser' => $iduser,
        );
        $this->db->where('idrequests', $idrequests);
        $result = $this->db->update('requests', $data);        
        return $result; 
    }

    public function deleteAsign($idrequests) {
        $data = array(
            'iduser' => '',
        );
        $this->db->where('idrequests', $idrequests);
        $result = $this->db->update('requests', $data);        
        return $result; 
    }
	
}