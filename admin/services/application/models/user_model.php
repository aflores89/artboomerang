<?php
Class User_model extends CI_Model {

    public function getUserByMail($mail) {
        $data = array(
            'mail' => $mail
        );
        $this->db->where($data);
        $result = $this->db->get('users');
        if(!empty($result->result_array())) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function updateuser($first_name,$last_name,$username,$phone,$mail,$iduser) {
        $data = array(
            'first_name' => $first_name,
            'last_name' => $last_name,
            'username' => $username,
            'phone' => $phone,
            'mail' => $mail
        );

        $this->db->where('idusers', $iduser);
        $result = $this->db->update('users', $data);        
        return $result;
    }

    public function changePassword($password,$iduser) {
        $data = array(
            'password' => $password
        );

        $this->db->where('idusers', $iduser);
        $result = $this->db->update('users', $data);        
        return $result;
    }

    public function getUserByUsername($username) {
        $data = array(
            'username' => $username
        );
        $this->db->where($data);
        $result = $this->db->get('users');
        if(!empty($result->result_array())) {
            return $result->result_array();
        } else {
            return false;
        }
    }
    
    public function getUserById($idUser) {
        $data = array(
            'idusers' => $idUser
        );
        $this->db->where($data);
        $result = $this->db->get('users');
        if(!empty($result->result_array())) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function getUsersByUserType($user_type) {
        $data = array(
            'user_type' => $user_type
        );
        $this->db->where($data);
        $result = $this->db->get('users');
        if(!empty($result->result_array())) {
            return $result->result_array();
        } else {
            return false;
        }
    }


    public function getUserByUsernamePassword($username,$password) {

        $data = array(
            'username' => $username,
            'password' => $password
        );

        $this->db->where($data);
        $query = $this->db->get('users');
        $result = $query->result_array();
        if(count($result) == 0) {
            return false;
        }        
        $user = $result[0];

        $this->db->where('iduser_types',$user['user_type']);
        $query = $this->db->get('user_types');
        $result = $query->result_array();
        $user_type = $result[0]['type'];

        $user['user_type'] = $user_type;
        
        if(!empty($user)) {
            return $user;
        } else {
            return false;
        }
    }   
    
}