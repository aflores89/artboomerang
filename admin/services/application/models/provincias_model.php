<?php
Class Provincias_model extends CI_Model {


    function __construct() {
        parent::__construct();
    }

    public function getProvincias() {
        $result = $this->db->get('provincias');        

        if($result) {
            return $result->result_array();
        } else {
            return false;
        }
    }

}