<?php

Class Videos_model extends CI_Model {

	public function getVideos() {
		$result = $this->db->get('videos');

		if($result) {
			return $result->result_array();
		} else {
			return false;
		}

	}

	public function saveVideo($titulo,$descripcion,$link) {
		$data = array(
            'titulo' => $titulo,
            'descripcion' => $descripcion,
            'link' => $link
        );
        $result = $this->db->insert('videos', $data);
        if($result) {
            return $this->db->insert_id();
        } else {
            return false;
        }
	}

    public function getVideoById($id) {
        $this->db->where('idvideos', $id);
        $result = $this->db->get('videos');
        if(!empty($result->result_array())) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function updateVideos($titulo,$descripcion,$link,$idvideos) {
        $data = array(
            'titulo' => $titulo,
            'descripcion' => $descripcion,
            'link' => $link            
        );
        $this->db->where('idvideos', $idvideos);
        $result = $this->db->update('videos', $data);
        
        if($result) {
            return $this->db->insert_id();
        } else {
            return false;
        }
    }

	public function active($id) {
		$data = array(
           'destacado' => 1              
        );

		$this->db->where('idvideos', $id);
		$result = $this->db->update('videos', $data); 

		if($result) {
            return true;
        } else {
            return false;
        }
	}

	public function inactive($id) {
		$data = array(
           'destacado' => 0             
        );

		$this->db->where('idvideos', $id);
		$result = $this->db->update('videos', $data); 

		if($result) {
            return true;
        } else {
            return false;
        }
	}

	public function delete($id) {
        $this->db->where('idvideos', $id);
        $result = $this->db->delete('videos');    
        return $result;     
    }
	
} 