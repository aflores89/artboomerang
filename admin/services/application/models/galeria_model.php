<?php

Class Galeria_model extends CI_Model {

	public function getGaleria($id) {
		$this->db->where('idartistas', $id);
		$result = $this->db->get('imagenes');

		if($result) {
			return $result->result_array();
		} else {
			return false;
		}

	}

	public function saveImagen($statment,$filename,$filepath,$idartista) {
		$data = array(
            'nombre' => $filename,
            'statment' => $statment,
            'path' => $filepath,
            'idartistas' => $idartista
        );
        $result = $this->db->insert('imagenes', $data);
        if($result) {
            return $this->db->insert_id();
        } else {
            return false;
        }
	} 

	public function getImageById($id) {
 		$this->db->where('idimages', $id);
        $result = $this->db->get('imagenes');    
        return $result->result_array();     
	}

	public function deleteImagen($id) {
        $this->db->where('idimages', $id);
        $result = $this->db->delete('imagenes');    
        return $result;     
    }
	
} 