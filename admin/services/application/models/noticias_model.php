<?php

Class Noticias_model extends CI_Model {

	public function getNoticias() {
		$result = $this->db->get('noticias');

		if($result) {
			return $result->result_array();
		} else {
			return false;
		}

	}

	public function saveNoticia($titular,$lead,$contenido,$filepath,$epigrafe,$autor) {
		$data = array(
            'titular' => $titular,
            'lead' => $lead,
            'contenido' => $contenido,
            'filepath' => $filepath,
            'epigrafe' => $epigrafe,
            'autor' => $autor
        );
            $result = $this->db->insert('noticias', $data);
        if($result) {
            return $this->db->insert_id();
        } else {
            return false;
        }
	}

	public function active($id) {
		$data = array(
           'invitado' => 1              
        );

		$this->db->where('idcuradores', $id);
		$result = $this->db->update('curadores', $data); 

		if($result) {
            return true;
        } else {
            return false;
        }
	}

	public function inactive($id) {
		$data = array(
           'invitado' => 0             
        );

		$this->db->where('idcuradores', $id);
		$result = $this->db->update('curadores', $data); 

		if($result) {
            return true;
        } else {
            return false;
        }
	}

	public function delete($id) {
        $this->db->where('idcuradores', $id);
        $result = $this->db->delete('curadores');    
        return $result;     
    }
	
} 