<?php 

// Mensajes de error

$config['messages']['success']['seller'] 		= 'Se agrego correctamente un nuevo Vendedor.';
$config['messages']['success']['adviser'] 		= 'Se agrego correctamente un nuevo Asesor Telefónico.';
$config['messages']['success']['project'] 		= 'Se agrego correctamente un nuevo Proyecto.';

$config['messages']['error']['emptyRecords'] 	= 'Alguno de los campos ingresado es vacío.';
$config['messages']['error']['username'] 		= 'Ya existe un usuario con ese mismo Nombre de Usuario.';
$config['messages']['error']['mail'] 			= 'El email elegido ya se encuentra registrado y asignado a otro usuario.';