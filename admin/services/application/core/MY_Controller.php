<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

Class MY_Controller extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct() {
		parent::__construct();	
		$this->load->library('appsession');
	}

	public function goToLogin() {		
		redirect('init/login');
	}

	public function debug($array) {
		echo '<pre>';
		die(var_dump($array));
	}

	public function load_view($page = false, $data = array()) 
	{
		$data['url'] = base_url();

		$data['user'] = $this->appsession->getSession();

		$this->load->view('templates/head', $data);
		$this->load->view('templates/header', $data);
		$this->load->view('templates/menu', $data);
		if($page) {
			$this->load->view($page, $data);
		}		
		$this->load->view('templates/footer', $data);

	}	

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */