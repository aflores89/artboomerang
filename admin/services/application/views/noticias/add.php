		<!-- Page content -->
	 	<div class="page-content">


			<!-- Page header -->
			<div class="page-header">
				<div class="page-title">
					<h3>Noticias</h3>
				</div>
			</div>
			<!-- /page header -->


			<!-- Breadcrumbs line -->
			<div class="breadcrumb-line">
				<ul class="breadcrumb">
					<li><a href="<?=$url?>dashboard">Menu Principal</a></li>
					<li><a href="<?=$url?>dashboard">Noticias</a></li>
					<li class="active">Agregar Nuevo</li>
				</ul>

				<div class="visible-xs breadcrumb-toggle">
					<a class="btn btn-link btn-lg btn-icon" data-toggle="collapse" data-target=".breadcrumb-buttons"><i class="icon-menu2"></i></a>
				</div>

			</div>
			<!-- /breadcrumbs line -->

			<?php if(isset($success)) : ?>
				<div class="bg-success with-padding">Éxito!: <?=$message?></div>
			<?php endif; ?>

			<?php if(isset($error)) : ?>
				<div class="bg-danger with-padding">Error: <?=$message?></div>
			<?php endif; ?>

	        <form class="form-horizontal" role="form" action="<?=$url?>noticias/add" method="POST" enctype="multipart/form-data">

				<!-- Basic inputs -->
		        <div class="panel panel-default">
			        <div class="panel-heading"><h6 class="panel-title"><i class="icon-bubble4"></i> Nueva Noticia</h6></div>
	                <div class="panel-body">

	                	<!--<div class="alert alert-success fade in block-inner">
	                		<button type="button" class="close" data-dismiss="alert">×</button>
	                		Los usuarios ingresan al sistema por medio de los dos siguientes datos, Nombre de Usuario y Contraseña.
	                	</div>-->

						<div class="form-group <?php echo (isset($titular_error)) ? 'has-error' : '';?>">
							<label class="col-sm-2 control-label">Titular: </label>
							<div class="col-sm-10">
								<input value="<?php echo (isset($user_form['titular'])) ? $user_form['titular'] : '';?>" type="text" name="titular" class="form-control">								
							</div>
						</div>

						<div class="form-group <?php echo (isset($lead_error)) ? 'has-error' : '';?>">
							<label class="col-sm-2 control-label">Lead: </label>
							<div class="col-sm-10">
								<textarea class="form-control" name="lead"></textarea>
							</div>
						</div>

						<div class="form-group <?php echo (isset($contenido_error)) ? 'has-error' : '';?>">
							<label class="col-sm-2 control-label">Contenido de Noticia: </label>
							<div class="col-sm-10">
								<textarea class="form-control" name="contenido"></textarea>
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-2" for="">Imagen:</label>
							<div class="col-sm-10">
								<input type="file" name="userfile" class="form-control" />
							</div>

						</div>	

						<div class="form-group <?php echo (isset($epigrafe_error)) ? 'has-error' : '';?>">
							<label class="col-sm-2 control-label">Epígrafe: </label>
							<div class="col-sm-10">
								<input value="<?php echo (isset($user_form['epigrafe'])) ? $user_form['epigrafe'] : '';?>" type="text" name="epigrafe" class="form-control">								
							</div>
						</div>

						<div class="form-group <?php echo (isset($autor_error)) ? 'has-error' : '';?>">
							<label class="col-sm-2 control-label">Autor: </label>
							<div class="col-sm-10">
								<input value="<?php echo (isset($user_form['autor'])) ? $user_form['autor'] : '';?>" type="text" name="autor" class="form-control">								
							</div>
						</div>

						<div class="form-actions text-right">
	                    	<input type="submit" value="Guardar" class="btn btn-primary">
	                    </div>
	                </div>

				</div>
				<!-- /basic inputs -->


            </form>