		<!-- Page content -->
	 	<div class="page-content">


			<!-- Page header -->
			<div class="page-header">
				<div class="page-title">
					<h3>Artistas</h3>
				</div>
			</div>
			<!-- /page header -->


			<!-- Breadcrumbs line -->
			<div class="breadcrumb-line">
				<ul class="breadcrumb">
					<li><a href="<?=$url?>dashboard">Menu Principal</a></li>
					<li class="active">Artistas	</li>
				</ul>

				<div class="visible-xs breadcrumb-toggle">
					<a class="btn btn-link btn-lg btn-icon" data-toggle="collapse" data-target=".breadcrumb-buttons"><i class="icon-menu2"></i></a>
				</div>

			</div>
			<!-- /breadcrumbs line -->


			<!-- Alert -->
			<!--<div class="alert alert-success fade in block">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <i class="icon-info"></i> Nullam tincidunt dapibus nisi. Aenean porttitor egestas dolor, ut pretium enim vehicula at. Vivamus vulputate risus felis, eget blandit urna aliquam at
            </div>-->
            <!-- /alert -->


	        <!-- Tasks table -->
	        <div class="block">
	        	<h6 class="heading-hr"><i class="icon-grid"></i> Lista</h6>
	            <div class="datatable-tasks">
	                <table class="table table-bordered">
	                    <thead>
	                        <tr>	                        
	                            <th class="task-priority">Perfil</th>
	                            <th class="task-date-added">Nombre</th>
	                            <th class="task-progress">Biografía</th>
	                            <th class="task-progress">Statment</th>
	                            <th class="task-progress">Link</th>
	                            <th></th>
	                        </tr>
	                    </thead>
	                    <tbody>
	                    	<?php if(!empty($artistas))  : ?>
		                    	<?php foreach ($artistas as $key => $value) : ?>
		                        <tr>
		                        	<td><img src="<?=$value['filepath']?>" class="img-responsive" alt=""></td>
		                            <td><?=$value['nombre']?></td>
		                            <td class="col-sm-4"><?=substr($value['biografia'], 0,200)?>...</td>
		                            <td><?=substr($value['statment'], 0, 200)?>...</td>
		                            <td><a href="<?=$value['url']?>"><?=$value['url']?></a></td>
			                        <td class="text-center">
			                            <div class="btn-group col-lg-4">
				                            <button type="button" class="btn btn-icon btn-success dropdown-toggle" data-toggle="dropdown"><i class="icon-cog4"></i></button>
											<ul class="dropdown-menu icons-right dropdown-menu-right">
												<li><a href="#edit_modal" class="edit-button" data-idartista="<?=$value['idartistas']?>" data-toggle="modal" role="button"><i class="icon-pencil"></i> Editar</a></li>
												<li><a href="#imagen_modal" class="imagen-button" data-idartista="<?=$value['idartistas']?>" data-toggle="modal" role="button"><i class="icon-pencil"></i> Imagen perfil</a></li>
												<li><a href="<?=$url?>galeria/listar/<?=$value['idartistas']?>" class="password-button" data-idartista="<?=$value['idartistas']?>" data-toggle="modal" role="button"><i class="icon-image"></i> Ver Galería</a></li>
												<li><a href="#delete_modal" class="delete-button" data-idartista="<?=$value['idartistas']?>" data-toggle="modal" role="button"><i class="icon-cancel-circle2"></i> Eliminar</a></li>
											</ul>
			                            </div>
			                        </td>
		                        </tr>	   
		                        <?php endforeach; ?>                     
		                    <?php endif; ?>
	                    </tbody>
	                </table>
	            </div>
	        </div>
	        <!-- /tasks table -->


	        <!-- modal change password -->
			<div id="password_modal" class="modal fade in" tabindex="-1" role="dialog" aria-hidden="false">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
							<h4 class="modal-title"><i class="icon-paragraph-justify2"></i>Editar Asesor Telefónico</h4>
						</div>

						<!-- Form inside modal -->
						<form action="<?=$url?>adviser/change_password/" role="form" class="validate" novalidate="novalidate" method="post">

							<input type="hidden" name="iduser" id="iduser-password">

							<div class="modal-body with-padding">								

								<div class="form-group">
									<div class="row">
										<div class="col-sm-6">
											<label>Nueva Contraseña</label>
											<input name="enter_password" type="text" class="required form-control" id="enter_password">
										</div>

										<div class="col-sm-6">
											<label class="control-label">Repetir Contraseña</label>
											<input name="repeat_password" type="text" class="required form-control" id="repeat_password">
										</div>
									</div>
								</div>						
							</div>

							<div class="modal-footer">
								<button type="button" class="btn btn-warning" data-dismiss="modal">Cerrar</button>
								<button type="submit" class="btn btn-primary">Editar Contraseña</button>
							</div>

						</form>
					</div>
				</div>
			</div>
			<!-- -->

			<!-- modal editar -->
			<div id="edit_modal" class="modal fade in" tabindex="-1" role="dialog" aria-hidden="false">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
							<h4 class="modal-title"><i class="icon-paragraph-justify2"></i>Editar Artista</h4>
						</div>

						<!-- Form inside modal -->
						<form action="<?=$url?>artistas/edit/" role="form" class="validate" novalidate="novalidate" method="post">

							<input type="hidden" name="idartista" id="idartista-edit">

							<div class="modal-body with-padding">
								<div class="block-inner text-danger">
									<h6 class="heading-hr" id="title"> <small class="display-block"></small></h6>
								</div>

								<div class="form-group">
									<div class="row">
										<div class="col-sm-12">
											<label>Nombre</label>
											<input name="nombre" id="nombre" type="text" class="required form-control">
										</div>
									</div>
								</div>

								<div class="form-group">
									<div class="row">
										<div class="col-lg-12">
											<label for="">Biografía</label>
											<textarea name="biografia" id="biografia" class="form-control"></textarea>
										</div>
									</div>
								</div>

								<div class="form-group">
									<div class="row">
										<div class="col-lg-12">
											<label for="">Statment</label>
											<textarea name="statment" id="statment" class="form-control"></textarea>
										</div>
									</div>
								</div>

								<div class="form-group">
									<div class="row">
										<div class="col-lg-12">
											<label for="">Link</label>
											<input type="text" class="form-control" name="link" id="link">
										</div>
									</div>
								</div>							
							</div>

							<div class="modal-footer">
								<button type="button" class="btn btn-warning" data-dismiss="modal">Cerrar</button>
								<button type="submit" class="btn btn-primary">Editar</button>
							</div>

						</form>
					</div>
				</div>
			</div>
			<!-- -->
			

			<div id="imagen_modal" class="modal fade in" tabindex="-1" role="dialog" aria-hidden="false">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
							<h4 class="modal-title"><i class="icon-paragraph-justify2"></i>Editar Imagen de Perfil</h4>
						</div>

						<!-- Form inside modal -->
						<form action="<?=$url?>artistas/editperfil/" role="form" class="validate" novalidate="novalidate" method="post" enctype="multipart/form-data">

							<input type="hidden" name="idartista" id="idartista-imagen">

							<div class="modal-body with-padding">
								<div class="block-inner text-danger">
									<h6 class="heading-hr" id="title"> <small class="display-block"></small></h6>
								</div>

								<div class="form-group">
									<div class="row">
										<div class="col-sm-12">
											<label>Imagen:</label>
											<input type="file" name="userfile" class="form-control">
										</div>
									</div>
								</div>
													
							</div>

							<div class="modal-footer">
								<button type="button" class="btn btn-warning" data-dismiss="modal">Cerrar</button>
								<button type="submit" class="btn btn-primary">Editar</button>
							</div>

						</form>
					</div>
				</div>
			</div>
			<!-- -->


			<!-- modal change password -->
			<div id="delete_modal" class="modal fade in" tabindex="-1" role="dialog" aria-hidden="false">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
							<h4 class="modal-title"><i class="icon-paragraph-justify2"></i>Eliminar Proyecto</h4>
						</div>

						<!-- Form inside modal -->
						<form action="<?=$url?>artistas/delete/" role="form" class="validate" novalidate="novalidate" method="post">

							<input type="hidden" name="idartista" id="idartista-delete">

							<div class="modal-body with-padding">								

								<p>¿Esta seguro que desea eliminar el Artista?</p>					
							</div>

							<div class="modal-footer">
								<button type="button" class="btn btn-warning" data-dismiss="modal">Cerrar</button>
								<button type="submit" class="btn btn-primary">Eliminar</button>
							</div>

						</form>
					</div>
				</div>
			</div>
			<!-- -->

			<script>

				$(function() {

					$('a.imagen-button').click(function(event) {
						event.preventDefault();
						var idartista = $(this).data('idartista');	
						console.log(idartista);										
						$('#idartista-imagen').val(idartista);
					});

					$('a.delete-button').click(function(event) {
						event.preventDefault();
						var idartista = $(this).data('idartista');											
						$('#idartista-delete').val(idartista);
					});

					$('a.password-button').click(function(event) {
						var iduser = $(this).data('iduser');											
						$('#iduser-password').val(iduser);																				
					});

					$('a.edit-button').click(function(event) {
						var idartista = $(this).data('idartista');
						$('#idartista-edit').val(idartista);
						$.ajax({
							url: '<?=$url?>artistas/getArtista/',
							type: 'POST',
							dataType: 'json',
							data: {
								idartista: idartista
							}
						})
						.done(function(data) {			
							console.log(data);		
							$('#nombre').val(data[0]['nombre'])		
							$('#biografia').val(data[0]['biografia']);	
							$('#statment').val(data[0]['statment']);							
							$('#link').val(data[0]['url']);
						});
						
					});

				});				

				
			</script>
        