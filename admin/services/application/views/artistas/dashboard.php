		<!-- Page content -->
	 	<div class="page-content">


			<!-- Page header -->
			<div class="page-header">
				<div class="page-title">
					<h3>Dashboard <small>Bienvenid@ <?=$user['first_name']?>. </small></h3>
				</div>
			</div>
			<!-- /page header -->


			<!-- Breadcrumbs line -->
			<div class="breadcrumb-line">
				<ul class="breadcrumb">
					<li><a href="index.html">Home</a></li>
					<li class="active">Dashboard</li>
				</ul>

				<div class="visible-xs breadcrumb-toggle">
					<a class="btn btn-link btn-lg btn-icon" data-toggle="collapse" data-target=".breadcrumb-buttons"><i class="icon-menu2"></i></a>
				</div>

			</div>
			<!-- /breadcrumbs line -->
			
	        <!-- Tasks table -->
	        <!-- Datatable with tools menu -->
            <div class="panel panel-default">
                <div class="panel-heading"><h6 class="panel-title"><i class="icon-table"></i> Todas las Consultas</h6></div>
                <div class="datatable-tools">
	                <table class="table">
	                    <thead>
	                        <tr>
	                            <th>Nombre</th>
	                            <th class="task-priority">Apellido</th>	                            
	                            <th class="task-progress">Email</th>
	                            <th class="task-deadline">Teléfono</th>
	                            <th class="task-deadline">Lugar</th>
	                            <th class="task-deadline">Consulta</th>
	                            <th class="task-deadline">Asignar</th>
	                            <th class="task-tools text-center">Tools</th>
	                        </tr>
	                    </thead>
	                    <tbody>		                    	
	                    	<?php if($requests) : ?>
	                    		<?php foreach ($requests as $key => $value) : ?>
	                    			<?php
			                    	// die(var_dump($value['idrequests']));
			                    	?>                    	
			                        <tr>
			                            <td><?=$value['first_name']?></td>
			                            <td><?=$value['last_name']?></td>			                            
			                            <td><?=$value['mail']?></td>			                            
			                            <td><?=$value['phone']?></td>	
			                            <td><?=$value['location']?></td>		
			                            <td><?=$value['consultation']?></td>			                            	                            
			                            <td>
			                            	<select name="adviser" id="adviser" class="form-control">
			                            		<option value="">Vendedor</option>
			                            		<?php if($sellers) : ?>
				                            		<?php foreach ($sellers as $key => $value2) : ?>
				                            			<option value="<?=$value2['idusers']?>"><?=$value2['first_name'].' '.$value2['last_name']?></option>
				                            		<?php endforeach; ?>
			                            		<?php endif; ?>
			                            	</select>
			                            </td>
				                        <td class="text-center">
				                            <div class="btn-group">
					                            <button type="button" class="btn btn-icon btn-success dropdown-toggle" data-toggle="dropdown"><i class="icon-cog4"></i></button>
												<ul class="dropdown-menu icons-right dropdown-menu-right">
													<li><a href="#" id="asignar" data-id="<?=$value['idrequests']?>"><i class="icon-user"></i> Asignar</a></li>
													<li><a href="#report_modal" class="report_button" data-id="<?=$value['idrequests']?>" data-toggle="modal" role="button"><i class="icon-libreoffice"></i> Informe</a></li>
													<!-- <li><a href="#"><i class="icon-quill2"></i> Edit task</a></li>
													<li><a href="#"><i class="icon-share2"></i> Reassign</a></li>
													<li><a href="#"><i class="icon-checkmark3"></i> Complete</a></li>
													<li><a href="#"><i class="icon-stack"></i> Archive</a></li> -->
												</ul>
				                            </div>
				                        </td>
			                        </tr>	   
			                    <?php endforeach; ?>                     
			                <?php endif; ?>
	                    </tbody>
	                </table>
	            </div>
	        </div>
	        <!-- /tasks table -->

	        <!-- modal editar -->
			<div id="report_modal" class="modal fade in" tabindex="-1" role="dialog" aria-hidden="false">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
							<h4 class="modal-title"><i class="icon-paragraph-justify2"></i>Informe de Consulta</h4>
						</div>

						<!-- Form inside modal -->
						<form action="<?=$url?>reports/add/" role="form" class="validate" novalidate="novalidate" method="post">

							<input type="hidden" name="idrequest" id="idrequest">

							<div class="modal-body with-padding">

								<div class="form-group">
									<div class="row">
										<div class="col-sm-6">
											<label class="control-label">Responde</label>
											<select name="answer" id="" class="form-control">
												<option value="1">SI</option>
												<option value="0">NO</option>
											</select>
										</div>

										<div class="col-sm-6">
											<label>Envio de Mail con información:</label>
											<select name="mail" id="" class="form-control">
												<option value="1">SI</option>
												<option value="0">NO</option>
											</select>
										</div>
									</div>
								</div>

								<div class="form-group">
									<div class="row">
										<div class="col-sm-6">
											<label>Grado de interés del contacto:</label>
											<select name="interested" id="" class="form-control">
												<option value="1">Bajo</option>
												<option value="2">Medio</option>
												<option value="3">Alto</option>
											</select>
										</div>

										<div class="col-sm-6">
											<label>Canal de atención:</label>
											<select name="chanel" id="" class="form-control">
												<option value="1">Web</option>
												<option value="2">Chat</option>
												<option value="3">Telefónico</option>
												<option value="3">E-mail</option>
												<option value="3">Whats App</option>
												<option value="3">Redes Sociales</option>
												<option value="3">Personal</option>
											</select>
										</div>
									</div>
								</div>							

								<div class="form-group">
									<div class="row">
										<div class="col-sm-6">
											<label>Medio por el que llega:</label>
											<select name="medio" id="" class="form-control">
												<option value="1">Mail</option>
												<option value="2">Publicidad Digital</option>
												<option value="3">Vía Pública</option>
												<option value="3">Recomendación</option>
											</select>
										</div>

										<div class="col-sm-6">
											<label>Observaciones:</label>
											<textarea name="observations" id="" cols="30" rows="10" class="form-control required"></textarea>
										</div>
									</div>
								</div>

							</div>

							<div class="modal-footer">
								<button type="button" class="btn btn-warning" data-dismiss="modal">Cerrar</button>
								<button type="submit" id="guardar-consulta" class="btn btn-primary">Guardar</button>
							</div>

						</form>
					</div>
				</div>
			</div>
			<!-- -->

	        <script type="text/javascript">

	        	$('.report_button').on('click', function(e) {
	        		var idrequest = $(this).data('id');
	        		$('#idrequest').val(idrequest);
	        	});

        		$('#asignar').on('click', function(e) {
        			e.preventDefault();
        			var adviser = $('#adviser').val();
        			var request = $(this).data('id');
        			if(adviser.length == 0) {
        				alert('Debe asignar un Asesor Telefónico');	        				
        			} else {
        				$.ajax({
	        				url: '<?=$url?>adviser/asign_request',
	        				type: 'POST',
	        				dataType: 'json',
	        				data: {
	        					idadviser: adviser,
	        					idrequest: request
	        				},
	        				success: function(data) {
	        					if(data) {
	        						alert('Se Asigno correctamente la consulta');
	        					} else {
	        						alert('hubo un problema al asignar la consulta');
	        					}
	        				},
	        				error: function(data) {
	        					console.log(data);
	        				}
	        			});
        			}
	        	});

	        </script>

