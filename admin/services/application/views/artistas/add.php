		<!-- Page content -->
	 	<div class="page-content">


			<!-- Page header -->
			<div class="page-header">
				<div class="page-title">
					<h3>Artistas</h3>
				</div>
			</div>
			<!-- /page header -->


			<!-- Breadcrumbs line -->
			<div class="breadcrumb-line">
				<ul class="breadcrumb">
					<li><a href="<?=$url?>dashboard">Menu Principal</a></li>
					<li><a href="<?=$url?>dashboard">Artistas</a></li>
					<li class="active">Agregar Nuevo</li>
				</ul>

				<div class="visible-xs breadcrumb-toggle">
					<a class="btn btn-link btn-lg btn-icon" data-toggle="collapse" data-target=".breadcrumb-buttons"><i class="icon-menu2"></i></a>
				</div>

			</div>
			<!-- /breadcrumbs line -->

			<?php if(isset($success)) : ?>
				<div class="bg-success with-padding">Éxito!: <?=$message?></div>
			<?php endif; ?>

			<?php if(isset($error)) : ?>
				<div class="bg-danger with-padding">Error: <?=$message?></div>
			<?php endif; ?>

	        <form class="form-horizontal" role="form" action="<?=$url?>artistas/add" method="POST" enctype="multipart/form-data">

				<!-- Basic inputs -->
		        <div class="panel panel-default">
			        <div class="panel-heading"><h6 class="panel-title"><i class="icon-bubble4"></i> Nuevo Artista</h6></div>
	                <div class="panel-body">

	                	<!--<div class="alert alert-success fade in block-inner">
	                		<button type="button" class="close" data-dismiss="alert">×</button>
	                		Los usuarios ingresan al sistema por medio de los dos siguientes datos, Nombre de Usuario y Contraseña.
	                	</div>-->

						<div class="form-group <?php echo (isset($nombre_error)) ? 'has-error' : '';?>">
							<label class="col-sm-2 control-label">Nombre: </label>
							<div class="col-sm-10">
								<input value="<?php echo (isset($user_form['nombre'])) ? $user_form['nombre'] : '';?>" type="text" name="nombre" class="form-control">								
							</div>
						</div>

						<div class="form-group <?php echo (isset($biografia_error)) ? 'has-error' : '';?>">
							<label class="col-sm-2 control-label">Biografia: </label>
							<div class="col-sm-10">
								<textarea name="biografia" id="" class="form-control" cols="100%" rows="10">
									<?php echo (isset($user_form['biografia'])) ? $user_form['biografia'] : '';?>
								</textarea>
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-2 control-label">Statment: </label>
							<div class="col-sm-10">								
								<textarea name="statment" id="statment" width="100%" class="form-control"></textarea>
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-2 control-label">URL: </label>
							<div class="col-sm-10">
								<input value="<?php echo (isset($user_form['url'])) ? $user_form['url'] : '';?>" type="text" name="url" class="form-control">
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-2 control-label">Provincia:</label>
							<div class="col-sm-10">
								<select name="provincia" id="provincia" class="form-control">
									<option value="0">Seleccione una Provincia</option>
									<?php foreach ($provincias as $key => $value) : ?>										
										<option value="<?=$value['idprovincias']?>"><?=$value['nombre']?></option>
									<?php endforeach; ?>
								</select>
							</div>
						</div>				

						<div class="form-group">
							<label class="col-sm-2" for="">Imagen de Perfil:</label>
							<div class="col-sm-10">
								<input type="file" name="userfile" class="form-control" />
							</div>

						</div>	

						<div class="form-actions text-right">
	                    	<input type="submit" value="Guardar" class="btn btn-primary">
	                    </div>
	                </div>

				</div>
				<!-- /basic inputs -->


            </form>


        