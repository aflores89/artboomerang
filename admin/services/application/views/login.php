	<!-- Navbar -->
	<div class="navbar navbar-inverse" role="navigation">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-right">
				<span class="sr-only">Toggle navbar</span>
				<i class="icon-grid3"></i>
			</button>
			<a class="navbar-brand" href="#">Art Boomerang</a>
		</div>

		<ul class="nav navbar-nav navbar-right collapse">
			<li><a href="#"><i class="icon-screen2"></i></a></li>
			<li><a href="#"><i class="icon-paragraph-justify2"></i></a></li>
			<li>
				<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-cogs"></i></a>
                <ul class="dropdown-menu icons-right dropdown-menu-right">
					<!-- <li><a href="#"><i class="icon-cogs"></i> This is</a></li>
					<li><a href="#"><i class="icon-grid3"></i> Dropdown</a></li>
					<li><a href="#"><i class="icon-spinner7"></i> With right</a></li>
					<li><a href="#"><i class="icon-link"></i> Aligned icons</a></li> -->
                </ul>
			</li>
		</ul>
	</div>
	<!-- /navbar -->


	<!-- Login wrapper -->
	<div class="login-wrapper">
    	<form action="<?=$url?>init/login" role="form" method="POST">
			<div class="popup-header">
				<a href="#" class="pull-left"><i class="icon-user-plus"></i></a>
				<span class="text-semibold">Login Usuario</span>
				<div class="btn-group pull-right">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-cogs"></i></a>
                    <ul class="dropdown-menu icons-right dropdown-menu-right">
						<!-- <li><a href="#"><i class="icon-people"></i> Change user</a></li> -->
						<li><a href="#"><i class="icon-info"></i> Olvidaste el password?</a></li>
						<!-- <li><a href="#"><i class="icon-support"></i> Contact admin</a></li>
						<li><a href="#"><i class="icon-wrench"></i> Settings</a></li> -->
                    </ul>
				</div>
			</div>
			<div class="well">
				<div class="form-group has-feedback <?php echo (isset($username_error)) ? 'has-error' : '';?>">
					<label>Username</label>
					<input value="" name="username" type="text" class="form-control" placeholder="Username">
					<i class="icon-users form-control-feedback"></i>
				</div>

				<div class="form-group has-feedback <?php echo (isset($password_error)) ? 'has-error' : '';?>">
					<label>Password</label>
					<input value="" name="password" type="password" class="form-control" placeholder="Password">
					<i class="icon-lock form-control-feedback"></i>
				</div>

				<div class="row form-actions">
					<div class="col-xs-6">
						<div class="checkbox checkbox-success">
						<label>
							<input type="checkbox" class="styled">
							Remember me
						</label>
						</div>
					</div>

					<div class="col-xs-6">
						<button type="submit" class="btn btn-warning pull-right"><i class="icon-menu2"></i> Sign in</button>
					</div>
				</div>
			</div>
    	</form>
	</div>  
	<!-- /login wrapper -->


    <!-- Footer -->
    <div class="footer clearfix">
        <div class="pull-left">&copy; 2013. Londinium Admin Template by <a href="http://themeforest.net/user/Kopyov">Eugene Kopyov</a></div>
    	<div class="pull-right icons-group">
    		<a href="#"><i class="icon-screen2"></i></a>
    		<a href="#"><i class="icon-balance"></i></a>
    		<a href="#"><i class="icon-cog3"></i></a>
    	</div>
    </div>
    <!-- /footer -->


</body>
</html>