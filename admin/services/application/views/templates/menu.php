<!-- Page container -->
<div class="page-container">


	<!-- Sidebar -->
	<div class="sidebar">
		<div class="sidebar-content">

			<!-- User dropdown -->
			<div class="user-menu dropdown">
				<a href="#" class="dropdown-toggle" data-toggle="">
					<img src="http://placehold.it/300">
					<div class="user-info">
						<?=$user['first_name'].' '.$user['last_name']?> <span><?=$user['user_type']?></span>
					</div>
				</a>
				<div class="popup dropdown-menu dropdown-menu-right">
				    <div class="thumbnail">
				    	<div class="thumb">
							<img src="http://placehold.it/300">
							<div class="thumb-options">
								<span>
									<a href="#" class="btn btn-icon btn-success"><i class="icon-pencil"></i></a>
									<a href="#" class="btn btn-icon btn-success"><i class="icon-remove"></i></a>
								</span>
							</div>
					    </div>
				    
				    	<div class="caption text-center">
				    		<h6><?=$user['first_name'].' '.$user['last_name']?>  <small><?=$user['user_type']?></small></h6>
				    	</div>
			    	</div>

			    	<ul class="list-group">
						<li class="list-group-item"><i class="icon-pencil3 text-muted"></i> My posts <span class="label label-success">289</span></li>
						<li class="list-group-item"><i class="icon-people text-muted"></i> Users online <span class="label label-danger">892</span></li>
						<li class="list-group-item"><i class="icon-stats2 text-muted"></i> Reports <span class="label label-primary">92</span></li>
						<li class="list-group-item"><i class="icon-stack text-muted"></i> Balance <h5 class="pull-right text-danger">$45.389</h5></li>
					</ul>
				</div>
			</div>
			<!-- /user dropdown -->


			<!-- Main navigation -->
			<ul class="navigation">
				<li class="active"><a href="<?=$url.'dashboard'?>"><span>Dashboard</span> <i class="icon-screen2"></i></a></li>
				<?php if ($user['user_type'] == 'Admin') : ?>
				<li>
					<a href="#"><span>Artistas</span> <i class="icon-pencil"></i></a>
					<ul>
						<li><a href="<?=$url?>artistas/add">Agregar Nuevo</a></li>
						<li><a href="<?=$url?>artistas">Listar</a></li>
					</ul>
				</li>
				<li>
					<a href="#"><span>Curadores</span> <i class="icon-quill"></i></a>
					<ul>
						<li><a href="<?=$url?>curadores/add">Agregar Nuevo</a></li>
						<li><a href="<?=$url?>curadores">Listar</a></li>
					</ul>
				</li>
				<li>
					<a href="#"><span>Videos</span> <i class="icon-camera5"></i></a>
					<ul>
						<li><a href="<?=$url?>videos/add">Agregar Nuevo</a></li>
						<li><a href="<?=$url?>videos">Listar</a></li>
					</ul>
				</li>
				<li>
					<a href="#"><span>Noticias</span> <i class="icon-newspaper"></i></a>
					<ul>
						<li><a href="<?=$url?>noticias/add">Agregar Nueva</a></li>
						<li><a href="<?=$url?>noticias">Listar</a></li>
					</ul>
				</li>	
				<li>
					<a href="#"><span>Convocatorias</span> <i class="icon-file-pdf"></i></a>
					<ul>
						<li><a href="<?=$url?>convocatorias/add">Agregar Nueva</a></li>
						<li><a href="<?=$url?>convocatorias">Listar</a></li>
					</ul>
				</li>	
				<?php endif; ?>			
			</ul>
			<!-- /main navigation -->
			
		</div>
	</div>
	<!-- /sidebar -->