		<!-- Page content -->
	 	<div class="page-content">


			<!-- Page header -->
			<div class="page-header">
				<div class="page-title">
					<h3>Dashboard <small>Bienvenid@ <?=$user['first_name']?>.</small></h3>
				</div>
			</div>
			<!-- /page header -->


			<!-- Breadcrumbs line -->
			<div class="breadcrumb-line">
				<ul class="breadcrumb">
					<li><a href="index.html">Home</a></li>
					<li class="active">Dashboard</li>
				</ul>

				<div class="visible-xs breadcrumb-toggle">
					<a class="btn btn-link btn-lg btn-icon" data-toggle="collapse" data-target=".breadcrumb-buttons"><i class="icon-menu2"></i></a>
				</div>

			</div>
			<!-- /breadcrumbs line -->

			<?php if ($user['user_type'] == 'Admin') : ?>
			<!-- Info blocks -->
			<ul class="info-blocks">
				<li class="bg-danger">
					<div class="top-info">
						<a href="<?=$url?>artistas" data-toggle="modal" role="button">ARTISTAS</a>
						<small>Nuevo artista</small>
					</div>
					<a href="<?=$url?>artistas/add" data-toggle="modal" role="button"><i class="icon-pencil"></i></a>
				</li>			
				<li class="bg-success">
					<div class="top-info">
						<a href="<?=$url?>curadores">CURADORES</a>
						<small>Nuevo curador</small>
					</div>
					<a href="<?=$url?>curadores/add"><i class="icon-quill"></i></a>
				</li>
				<li class="bg-primary">
					<div class="top-info">
						<a href="<?=$url?>convocatorias">CONVOCATORIAS</a>
						<small>Nueva convocatoria</small>
					</div>
					<a href="<?=$url?>convocatorias/add"><i class="icon-file-pdf"></i></a>
					
				</li>
				<li class="bg-info">
					<div class="top-info">
						<a href="<?=$url?>videos">VIDEOS</a>
						<small>Nuevo video</small>
					</div>
					<a href="<?=$url?>videos/add"><i class="icon-camera5"></i></a>					
				</li>
				<li class="bg-warning">
					<div class="top-info">
						<a href="<?=$url?>videos">NOTICIAS</a>
						<small>Nueva noticia</small>
					</div>
					<a href="<?=$url?>noticias/add"><i class="icon-newspaper"></i></a>
				</li><!--
				<li class="bg-primary">
					<div class="top-info">
						<a href="#">Invoices stats</a>
						<small>invoices archive</small>
					</div>
					<a href="#"><i class="icon-coin"></i></a>
					<span class="bottom-info bg-danger">9 new invoices</span>
				</li> -->
			</ul>
			<!-- /info blocks -->
			<?php endif; ?>


			<!-- Alert -->
			<div class="alert alert-success fade in block">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <i class="icon-info"></i> El Administrador puede agregar editar y eliminar Artistas, Curadores así también eliminar o crear nuevas Convocatorias.
            </div>
            <!-- /alert -->
            <form class="form-horizontal" role="form" action="<?=$url?>dashboard/saveContenido" method="POST" enctype="multipart/form-data">
			
				<!-- Basic inputs -->
		        <div class="panel panel-default">
			        <div class="panel-heading"><h6 class="panel-title"><i class="icon-bubble4"></i> Contenido Principal</h6></div>
	                <div class="panel-body">

	                	<!--<div class="alert alert-success fade in block-inner">
	                		<button type="button" class="close" data-dismiss="alert">×</button>
	                		Los usuarios ingresan al sistema por medio de los dos siguientes datos, Nombre de Usuario y Contraseña.
	                	</div>-->

						<div class="form-group <?php echo (isset($titulo_error)) ? 'has-error' : '';?>">
							<label class="col-sm-2 control-label">Titulo: </label>
							<div class="col-sm-10">
								<input value="<?php echo (isset($contenido['titulo'])) ? $contenido['titulo'] : '';?>" type="text" name="titulo" class="form-control">								
							</div>
						</div>

						<div class="form-group <?php echo (isset($subtitulo_error)) ? 'has-error' : '';?>">
							<label class="col-sm-2 control-label">Subtitulo: </label>
							<div class="col-sm-10">
								<input value="<?php echo (isset($contenido['subtitulo'])) ? $contenido['subtitulo'] : '';?>" type="text" name="subtitulo" class="form-control">								
							</div>
						</div>

						<div class="form-group <?php echo (isset($contenido_error)) ? 'has-error' : '';?>">
							<label class="col-sm-2 control-label">Contenido: </label>
							<div class="col-sm-10">
								<textarea name="contenido" id="" class="form-control" cols="100%" rows="10">
									<?php echo (isset($contenido['contenido'])) ? $contenido['contenido'] : '';?>
								</textarea>
							</div>
						</div>

						<div class="form-actions text-right">
	                    	<input type="submit" value="Guardar" class="btn btn-primary">
	                    </div>
	                </div>

				</div>
				<!-- /basic inputs -->


            </form>

	        <!-- Tasks table -->	        
	        <!-- modal editar -->
			<div id="request_modal" class="modal fade in" tabindex="-1" role="dialog" aria-hidden="false">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
							<h4 class="modal-title"><i class="icon-paragraph-justify2"></i>Agregar Nueva Consulta</h4>
						</div>

						<!-- Form inside modal -->
						<form action="<?=$url?>dashboard/save_requests/" role="form" class="validate" novalidate="novalidate" method="post">

							<input type="hidden" name="iduser" id="iduser">

							<div class="modal-body with-padding">

								<div class="form-group">
									<div class="row">
										<div class="col-sm-6">
											<label>Nombre</label>
											<input name="first_name" id="first_name" type="text" class="required form-control">
										</div>

										<div class="col-sm-6">
											<label class="control-label">Apellido</label>
											<input name="last_name" id="last_name" type="text" class="required form-control">
										</div>
									</div>
								</div>

								<div class="form-group">
									<div class="row">
										<div class="col-sm-6">
											<label>Email</label>
											<input name="mail" id="mail" type="text" class="required form-control">
										</div>

										<div class="col-sm-6">
											<label>Teléfono</label>
											<input name="phone" id="phone" type="text" class="required form-control">
										</div>
									</div>
								</div>							

								<div class="form-group">
									<div class="row">
										<div class="col-lg-12">
											<label>Proyecto</label>
											<select name="project" class="form-control">
												<option value="">Seleccione Proyecto</option>
												<?php foreach ($projects as $key => $value) : ?>
													<option value="<?=$value['idprojects']?>"><?=$value['name']?></option>
												<?php endforeach; ?>
											</select>
										</div>
									</div>
								</div>

								<div class="form-group">
									<div class="row">
										<div class="col-lg-12">
											<label>Lugar</label>
											<input type="text" name="location" class="required form-control" />
										</div>
									</div>
								</div>

								<div class="form-group">
									<div class="row">
										<div class="col-lg-12">
											<label>Consulta</label>
											<textarea name="consultation" id="" cols="30" rows="10" class="form-control"></textarea>
										</div>
									</div>
								</div>

							</div>

							<div class="modal-footer">
								<button type="button" class="btn btn-warning" data-dismiss="modal">Cerrar</button>
								<button type="submit" id="guardar-consulta" class="btn btn-primary">Guardar</button>
							</div>

						</form>
					</div>
				</div>
			</div>
			<!-- -->

	        <script type="text/javascript">

	        	$('.delete_asign').on('click', function(e) {
        			e.preventDefault();        			        		
        			var request = $(this).data('id');

    				$.ajax({
        				url: '<?=$url?>adviser/delete_asign',
        				type: 'POST',
        				dataType: 'json',
        				data: {
        					idrequest: request
        				},
        				success: function(data) {
        					if(data) {
        						alert('Se borro correctamente la asignacion');
        						location.reload();
        					} else {
        						alert('Hubo un problema al borrar la asignacion');
        					}
        				},
        				error: function(data) {
        					console.log(data);
        				}
        			});

	        	});

        		$('.asign_request').on('click', function(e) {
        			e.preventDefault();        			        		
        			var request = $(this).data('id');
        			var adviser = $('#adviser-'+request).val();
        			if(adviser.length == 0) {
        				alert('Debe seleccionar un asesor Telefónico');	        				
        			} else {
        				$.ajax({
	        				url: '<?=$url?>adviser/asign_request',
	        				type: 'POST',
	        				dataType: 'json',
	        				data: {
	        					idadviser: adviser,
	        					idrequest: request
	        				},
	        				success: function(data) {
	        					if(data) {
	        						alert('Se asigno correctamente la consulta');
	        						location.reload();
	        					} else {
	        						alert('Hubo un problema al asignar la consulta');
	        					}
	        				},
	        				error: function(data) {
	        					console.log(data);
	        					alert('Hubo un problema al asignar la consulta');
	        				}
	        			});
        			}
	        	});

	        </script>


        