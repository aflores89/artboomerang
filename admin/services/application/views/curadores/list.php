		<!-- Page content -->
	 	<div class="page-content">


			<!-- Page header -->
			<div class="page-header">
				<div class="page-title">
					<h3>Curadores</h3>
				</div>
			</div>
			<!-- /page header -->


			<!-- Breadcrumbs line -->
			<div class="breadcrumb-line">
				<ul class="breadcrumb">
					<li><a href="<?=$url?>dashboard">Menu Principal</a></li>
					<li class="active">Curadores</li>
				</ul>

				<div class="visible-xs breadcrumb-toggle">
					<a class="btn btn-link btn-lg btn-icon" data-toggle="collapse" data-target=".breadcrumb-buttons"><i class="icon-menu2"></i></a>
				</div>

			</div>
			<!-- /breadcrumbs line -->


			<!-- Alert -->
			<!--<div class="alert alert-success fade in block">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <i class="icon-info"></i> Nullam tincidunt dapibus nisi. Aenean porttitor egestas dolor, ut pretium enim vehicula at. Vivamus vulputate risus felis, eget blandit urna aliquam at
            </div>-->
            <!-- /alert -->


	        <!-- Tasks table -->
	        <div class="block">
	        	<h6 class="heading-hr"><i class="icon-grid"></i> Lista</h6>
	            <div class="datatable-tasks">
	                <table class="table table-bordered">
	                    <thead>
	                        <tr>	                        
	                        	<th class="task-date-added">Perfil</th>
	                            <th class="task-priority">Nombre</th>
	                            <th class="task-date-added">Extracto</th>
								<th class="task-date-added">Subtitulo</th>
								<th class="task-date-added">Orden</th>
	                            <th></th>
	                        </tr>
	                    </thead>
	                    <tbody>
	                    	<?php if(!empty($curadores))  : ?>
		                    	<?php foreach ($curadores as $key => $value) : ?>
		                        <tr>
		                            <td class="col-sm-2"><img src="<?=$url.$value['imagen']?>" class="img-responsive"></td>
		                            <td><?=$value['nombre']?></td>
		                            <td><?=$value['extracto']?></td>
									<td><?=$value['subtitulo']?></td>
									<td><?=$value['orden']?></td>
			                        <td class="col-sm-2">
			                            <div class="btn-group col-lg-4">
				                            <button type="button" class="btn btn-icon btn-success dropdown-toggle" data-toggle="dropdown"><i class="icon-cog4"></i></button>
											<ul class="dropdown-menu icons-right dropdown-menu-right">
												<li><a href="#edit_modal" class="edit-button" data-idcuradores="<?=$value['idcuradores']?>" data-toggle="modal" role="button"><i class="icon-pencil"></i> Editar</a></li>
												<li><a href="#imagen_modal" class="imagen-button" data-idcurador="<?=$value['idcuradores']?>" data-toggle="modal" role="button"><i class="icon-pencil"></i> Imagen perfil</a></li>
												<li><a href="#delete_modal" class="delete-button" data-idcuradores="<?=$value['idcuradores']?>" data-toggle="modal" role="button"><i class="icon-cancel-circle2"></i> Eliminar</a></li>
											</ul>
			                            </div>
			                        </td>
		                        </tr>	   
		                        <?php endforeach; ?>                     
		                    <?php endif; ?>
	                    </tbody>
	                </table>
	            </div>
	        </div>
	        <!-- /tasks table -->

			<div id="imagen_modal" class="modal fade in" tabindex="-1" role="dialog" aria-hidden="false">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
							<h4 class="modal-title"><i class="icon-paragraph-justify2"></i>Editar Imagen de Perfil</h4>
						</div>

						<!-- Form inside modal -->
						<form action="<?=$url?>curadores/editperfil/" role="form" class="validate" novalidate="novalidate" method="post" enctype="multipart/form-data">

							<input type="hidden" name="idcurador" id="idcurador-imagen">

							<div class="modal-body with-padding">
								<div class="block-inner text-danger">
									<h6 class="heading-hr" id="title"> <small class="display-block"></small></h6>
								</div>

								<div class="form-group">
									<div class="row">
										<div class="col-sm-12">
											<label>Imagen:</label>
											<input type="file" name="userfile" class="form-control">
										</div>
									</div>
								</div>

							</div>

							<div class="modal-footer">
								<button type="button" class="btn btn-warning" data-dismiss="modal">Cerrar</button>
								<button type="submit" class="btn btn-primary">Editar</button>
							</div>

						</form>
					</div>
				</div>
			</div>
			<!-- -->

	        <!-- modal change password -->
			<div id="password_modal" class="modal fade in" tabindex="-1" role="dialog" aria-hidden="false">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
							<h4 class="modal-title"><i class="icon-paragraph-justify2"></i>Editar Asesor Telefónico</h4>
						</div>

						<!-- Form inside modal -->
						<form action="<?=$url?>adviser/change_password/" role="form" class="validate" novalidate="novalidate" method="post">

							<input type="hidden" name="iduser" id="iduser-password">

							<div class="modal-body with-padding">								

								<div class="form-group">
									<div class="row">
										<div class="col-sm-6">
											<label>Nueva Contraseña</label>
											<input name="enter_password" type="text" class="required form-control" id="enter_password">
										</div>

										<div class="col-sm-6">
											<label class="control-label">Repetir Contraseña</label>
											<input name="repeat_password" type="text" class="required form-control" id="repeat_password">
										</div>
									</div>
								</div>						
							</div>

							<div class="modal-footer">
								<button type="button" class="btn btn-warning" data-dismiss="modal">Cerrar</button>
								<button type="submit" class="btn btn-primary">Editar Contraseña</button>
							</div>

						</form>
					</div>
				</div>
			</div>
			<!-- -->

			<!-- modal editar -->
			<div id="edit_modal" class="modal fade in" tabindex="-1" role="dialog" aria-hidden="false">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
							<h4 class="modal-title"><i class="icon-paragraph-justify2"></i>Editar Curador</h4>
						</div>

						<!-- Form inside modal -->
						<form action="<?=$url?>curadores/edit/" role="form" class="validate" novalidate="novalidate" method="post">

							<input type="hidden" name="idcuradores" id="idcuradores">

							<div class="modal-body with-padding">
								<div class="block-inner text-danger">
									<h6 class="heading-hr" id="title"> <small class="display-block"><!--Please enter your shipping info--></small></h6>
								</div>

								<div class="form-group">
									<div class="row">
										<div class="col-sm-12">
											<label>Nombre</label>
											<input name="nombre" id="nombre" type="text" class="required form-control">
										</div>
									</div>
								</div>

								<div class="form-group">
									<div class="row">
										<div class="col-sm-12">
											<label>Extracto</label>
											<textarea name="extracto" id="extracto" class="form-control"></textarea>
										</div>
									</div>
								</div>

								<div class="form-group">
									<div class="row">
										<div class="col-sm-12">
											<label for="">Subtitulo</label>
											<input type="text" name="subtitulo" id="subtitulo" class="form-control">
										</div>
									</div>
								</div>

								<div class="form-group">
									<div class="row">
										<div class="col-sm-12">
											<label for="orden">Orden</label>
											<select name="orden" id="orden">
												<option value="1">1</option>
												<option value="2">2</option>
												<option value="3">3</option>
												<option value="4">4</option>
												<option value="5">5</option>
												<option value="6">6</option>
												<option value="7">7</option>
												<option value="8">8</option>
												<option value="9">9</option>
												<option value="10">10</option>
											</select>
										</div>
									</div>
								</div>
													
							</div>

							<div class="modal-footer">
								<button type="button" class="btn btn-warning" data-dismiss="modal">Cerrar</button>
								<button type="submit" class="btn btn-primary">Editar</button>
							</div>

						</form>
					</div>
				</div>
			</div>
			<!-- -->


			<!-- modal change password -->
			<div id="delete_modal" class="modal fade in" tabindex="-1" role="dialog" aria-hidden="false">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
							<h4 class="modal-title"><i class="icon-paragraph-justify2"></i>Eliminar Curador</h4>
						</div>

						<!-- Form inside modal -->
						<form action="<?=$url?>curadores/delete/" role="form" class="validate" novalidate="novalidate" method="post">

							<input type="hidden" name="idcuradores" id="idcuradores-delete">

							<div class="modal-body with-padding">								

								<p>¿Esta seguro que desea eliminar el Curador?</p>					
							</div>

							<div class="modal-footer">
								<button type="button" class="btn btn-warning" data-dismiss="modal">Cerrar</button>
								<button type="submit" class="btn btn-primary">Eliminar</button>
							</div>

						</form>
					</div>
				</div>
			</div>
			<!-- -->

			<script>

				$(function() {

					$('a.imagen-button').click(function(event) {
						event.preventDefault();
						var idcurador = $(this).data('idcurador');
						console.log(idcurador);
						$('#idcurador-imagen').val(idcurador);
					});

					$('a.delete-button').click(function(event) {
						event.preventDefault();
						var idcuradores = $(this).data('idcuradores');											
						$('#idcuradores-delete').val(idcuradores);
					});

					$('a.password-button').click(function(event) {
						var iduser = $(this).data('iduser');											
						$('#iduser-password').val(iduser);																				
					});

					$('a.edit-button').click(function(event) {
						var idcuradores = $(this).data('idcuradores');											
						$('#idcuradores').val(idcuradores);
						$.ajax({
							url: '<?=$url?>curadores/getCurador/',
							type: 'POST',
							dataType: 'json',
							data: {
								idcuradores: idcuradores
							}
						})
						.done(function(data) {			
							$('#nombre').val(data[0]['nombre']);		
							$('#extracto').val(data[0]['extracto']);
							$('#subtitulo').val(data[0]['subtitulo']);
							$('#orden').val(data[0]['orden']);
						});
						
					});

				});				

				
			</script>
        