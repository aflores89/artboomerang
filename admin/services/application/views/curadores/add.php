		<!-- Page content -->
	 	<div class="page-content">


			<!-- Page header -->
			<div class="page-header">
				<div class="page-title">
					<h3>Curadores</h3>
				</div>
			</div>
			<!-- /page header -->


			<!-- Breadcrumbs line -->
			<div class="breadcrumb-line">
				<ul class="breadcrumb">
					<li><a href="<?=$url?>dashboard">Menu Principal</a></li>
					<li><a href="<?=$url?>dashboard">Curadores</a></li>
					<li class="active">Agregar Nuevo</li>
				</ul>

				<div class="visible-xs breadcrumb-toggle">
					<a class="btn btn-link btn-lg btn-icon" data-toggle="collapse" data-target=".breadcrumb-buttons"><i class="icon-menu2"></i></a>
				</div>

			</div>
			<!-- /breadcrumbs line -->

			<?php if(isset($success)) : ?>
				<div class="bg-success with-padding">Éxito!: <?=$message?></div>
			<?php endif; ?>

			<?php if(isset($error)) : ?>
				<div class="bg-danger with-padding">Error: <?=$message?></div>
			<?php endif; ?>

	        <form class="form-horizontal" role="form" action="<?=$url?>curadores/add" method="POST" enctype="multipart/form-data">

				<!-- Basic inputs -->
		        <div class="panel panel-default">
			        <div class="panel-heading"><h6 class="panel-title"><i class="icon-bubble4"></i> Nuevo Curador</h6></div>
	                <div class="panel-body">

	                	<!--<div class="alert alert-success fade in block-inner">
	                		<button type="button" class="close" data-dismiss="alert">×</button>
	                		Los usuarios ingresan al sistema por medio de los dos siguientes datos, Nombre de Usuario y Contraseña.
	                	</div>-->

						<div class="form-group <?php echo (isset($nombre_error)) ? 'has-error' : '';?>">
							<label class="col-sm-2 control-label">Nombre: </label>
							<div class="col-sm-10">
								<input value="<?php echo (isset($user_form['nombre'])) ? $user_form['nombre'] : '';?>" type="text" name="nombre" class="form-control">								
							</div>
						</div>

						<div class="form-group <?php echo (isset($nombre_error)) ? 'has-error' : '';?>">
							<label class="col-sm-2 control-label">Extracto: </label>
							<div class="col-sm-10">
								<textarea class="form-control" name="extracto"></textarea>
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-2" for="">Foto:</label>
							<div class="col-sm-10">
								<input type="file" name="userfile" class="form-control" />
							</div>
						</div>

						<div class="form-group">
							<label for="" class="col-sm-2">Subtitulo:</label>
							<div class="col-sm-10">
								<input type="text" name="subtitulo" class="form-control">
							</div>
						</div>

						<div class="form-group">
							<label for="" class="col-sm-2">Orden:</label>
							<div class="col-sm-10">
								<select name="orden" id="orden">
									<option value="1">1</option>
									<option value="2">2</option>
									<option value="3">3</option>
									<option value="4">4</option>
									<option value="5">5</option>
									<option value="6">6</option>
									<option value="7">7</option>
									<option value="8">8</option>
									<option value="9">9</option>
									<option value="10">10</option>
								</select>
							</div>
						</div>

						<div class="form-actions text-right">
	                    	<input type="submit" value="Guardar" class="btn btn-primary">
	                    </div>
	                </div>

				</div>
				<!-- /basic inputs -->


            </form>