		<!-- Page content -->
	 	<div class="page-content">


			<!-- Page header -->
			<div class="page-header">
				<div class="page-title">
					<h3><?=$artista[0]['nombre']?></h3>
				</div>
			</div>
			<!-- /page header -->


			<!-- Breadcrumbs line -->
			<div class="breadcrumb-line">
				<ul class="breadcrumb">
					<li><a href="<?=$url?>dashboard">Menu Principal</a></li>
					<li class="active">Galería	</li>
				</ul>

				<div class="visible-xs breadcrumb-toggle">
					<a class="btn btn-link btn-lg btn-icon" data-toggle="collapse" data-target=".breadcrumb-buttons"><i class="icon-menu2"></i></a>
				</div>

			</div>
			<!-- /breadcrumbs line -->


			<!-- Alert -->
			<!--<div class="alert alert-success fade in block">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <i class="icon-info"></i> Nullam tincidunt dapibus nisi. Aenean porttitor egestas dolor, ut pretium enim vehicula at. Vivamus vulputate risus felis, eget blandit urna aliquam at
            </div>-->
            <!-- /alert -->

            <form class="form-horizontal" role="form" action="<?=$url?>galeria/add" method="POST" enctype="multipart/form-data">

				<!-- Basic inputs -->
		        <div class="panel panel-default">
			        <div class="panel-heading"><h6 class="panel-title"><i class="icon-bubble4"></i> Nuevo Imagen</h6></div>
	                <div class="panel-body">

	                	<!--<div class="alert alert-success fade in block-inner">
	                		<button type="button" class="close" data-dismiss="alert">×</button>
	                		Los usuarios ingresan al sistema por medio de los dos siguientes datos, Nombre de Usuario y Contraseña.
	                	</div>-->			
	                	<input type="hidden" name="idartista" value="<?=$artista[0]['idartistas']?>">

						<div class="form-group">
							<label class="col-sm-2" for="">Imagen :</label>
							<div class="col-sm-10">
								<input type="file" name="userfile" class="form-control" />
							</div>

						</div>	

						<div class="form-group">
							<label for="" class="col-sm-2">* Descripción</label>
							<div class="col-sm-10">
								<textarea name="statment" id="" class="form-control"></textarea>
							</div>
						</div>

						<div class="form-actions text-right">
	                    	<input type="submit" value="Guardar" class="btn btn-primary">
	                    </div>
	                </div>

				</div>
				<!-- /basic inputs -->


            </form>


	        <!-- Tasks table -->
	        <div class="block">
	        	<h6 class="heading-hr"><i class="icon-grid"></i> Lista</h6>
	            <div class="datatable-tasks">
	                <table class="table table-bordered">
	                    <thead>
	                        <tr>	                        
	                            <th class="task-priority">Imagen</th>
	                            <th class="task-date-added">Nombre</th>
	                            <th class="task-date-added">Statment</th>
	                            <th class="task-progress">Path</th>
	                            <th class="col-sm-2">Herramientas</th>
	                        </tr>
	                    </thead>
	                    <tbody>
	                    	<?php if(!empty($imagenes))  : ?>
		                    	<?php foreach ($imagenes as $key => $value) : ?>
		                        <tr>
		                        	<td><img src="<?=$url.$value['path']?>" class="img-responsive" alt=""></td>
		                            <td><?=$value['nombre']?></td>
		                            <td><?=$value['statment']?></td>
		                            <td><?=$value['path']?></td>
			                        <td class="text-center">
			                            <div class="btn-group col-lg-4">
				                            <button type="button" class="btn btn-icon btn-success dropdown-toggle" data-toggle="dropdown"><i class="icon-cog4"></i></button>
											<ul class="dropdown-menu icons-right dropdown-menu-right">
												<li><a href="#delete_modal" class="delete-button" data-idimages="<?=$value['idimages']?>" data-toggle="modal" role="button"><i class="icon-cancel-circle2"></i> Eliminar</a></li>
											</ul>
			                            </div>
			                        </td>
		                        </tr>	   
		                        <?php endforeach; ?>                     
		                    <?php endif; ?>
	                    </tbody>
	                </table>
	            </div>
	        </div>
	        <!-- /tasks table -->


	        <!-- modal change password -->
			<div id="password_modal" class="modal fade in" tabindex="-1" role="dialog" aria-hidden="false">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
							<h4 class="modal-title"><i class="icon-paragraph-justify2"></i>Editar Asesor Telefónico</h4>
						</div>

						<!-- Form inside modal -->
						<form action="<?=$url?>adviser/change_password/" role="form" class="validate" novalidate="novalidate" method="post">

							<input type="hidden" name="iduser" id="iduser-password">

							<div class="modal-body with-padding">								

								<div class="form-group">
									<div class="row">
										<div class="col-sm-6">
											<label>Nueva Contraseña</label>
											<input name="enter_password" type="text" class="required form-control" id="enter_password">
										</div>

										<div class="col-sm-6">
											<label class="control-label">Repetir Contraseña</label>
											<input name="repeat_password" type="text" class="required form-control" id="repeat_password">
										</div>
									</div>
								</div>						
							</div>

							<div class="modal-footer">
								<button type="button" class="btn btn-warning" data-dismiss="modal">Cerrar</button>
								<button type="submit" class="btn btn-primary">Editar Contraseña</button>
							</div>

						</form>
					</div>
				</div>
			</div>
			<!-- -->

			<!-- modal editar -->
			<div id="edit_modal" class="modal fade in" tabindex="-1" role="dialog" aria-hidden="false">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
							<h4 class="modal-title"><i class="icon-paragraph-justify2"></i>Editar Artista</h4>
						</div>

						<!-- Form inside modal -->
						<form action="<?=$url?>adviser/edit/" role="form" class="validate" novalidate="novalidate" method="post">

							<input type="hidden" name="iduser" id="iduser">

							<div class="modal-body with-padding">
								<div class="block-inner text-danger">
									<h6 class="heading-hr" id="title"> <small class="display-block">Please enter your shipping info</small></h6>
								</div>

								<div class="form-group">
									<div class="row">
										<div class="col-sm-12">
											<label>Nombre de Usuario</label>
											<input name="username" id="username" type="text" class="required form-control">
										</div>
									</div>
								</div>

								<div class="form-group">
									<div class="row">
										<div class="col-sm-6">
											<label>Nombre</label>
											<input name="first_name" id="first_name" type="text" class="form-control">
										</div>

										<div class="col-sm-6">
											<label class="control-label">Apellido</label>
											<input name="last_name" id="last_name" type="text" class="form-control">
										</div>
									</div>
								</div>

								<div class="form-group">
									<div class="row">
										<div class="col-sm-6">
											<label>Email</label>
											<input name="mail" id="mail" type="text" class="required form-control">
										</div>

										<div class="col-sm-6">
											<label>Teléfono</label>
											<input name="phone" id="phone" type="text" class="form-control">
										</div>
									</div>
								</div>								
							</div>

							<div class="modal-footer">
								<button type="button" class="btn btn-warning" data-dismiss="modal">Cerrar</button>
								<!--<button type="submit" class="btn btn-primary">Editar</button>-->
							</div>

						</form>
					</div>
				</div>
			</div>
			<!-- -->


			<!-- modal change password -->
			<div id="delete_modal" class="modal fade in" tabindex="-1" role="dialog" aria-hidden="false">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
							<h4 class="modal-title"><i class="icon-paragraph-justify2"></i>Eliminar Imagen</h4>
						</div>

						<!-- Form inside modal -->
						<form action="<?=$url?>galeria/delete/" role="form" class="validate" novalidate="novalidate" method="post">

							<input type="hidden" name="idgaleria" id="idgaleria-delete">

							<div class="modal-body with-padding">								

								<p>¿Esta seguro que desea eliminar la Imagen?</p>					
							</div>

							<div class="modal-footer">
								<button type="button" class="btn btn-warning" data-dismiss="modal">Cerrar</button>
								<button type="submit" class="btn btn-primary">Eliminar</button>
							</div>

						</form>
					</div>
				</div>
			</div>
			<!-- -->

			<script>

				$(function() {

					$('a.delete-button').click(function(event) {
						event.preventDefault();
						var idgaleria = $(this).data('idimages');											
						console.log(idgaleria);
						$('#idgaleria-delete').val(idgaleria);
					});

					$('a.password-button').click(function(event) {
						var iduser = $(this).data('iduser');											
						$('#iduser-password').val(iduser);																				
					});

					$('a.edit-button').click(function(event) {
						var iduser = $(this).data('iduser');
						$.ajax({
							url: '<?=$url?>adviser/getAdviser/',
							type: 'GET',
							dataType: 'json',
							data: {
								iduser: iduser
							}
						})
						.done(function(data) {					
							$('#title').text(data['first_name']+' '+data['last_name'])		
							$('#iduser').val(data['idusers']);	
							$('#username').val(data['username']);							
							$('#first_name').val(data['first_name']);
							$('#last_name').val(data['last_name']);
							$('#mail').val(data['mail']);
							$('#phone').val(data['phone']);
						});
						
					});

				});				

				
			</script>
        