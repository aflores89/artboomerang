		<!-- Page content -->
	 	<div class="page-content">


			<!-- Page header -->
			<div class="page-header">
				<div class="page-title">
					<h3>Videos</h3>
				</div>
			</div>
			<!-- /page header -->


			<!-- Breadcrumbs line -->
			<div class="breadcrumb-line">
				<ul class="breadcrumb">
					<li><a href="<?=$url?>dashboard">Menu Principal</a></li>
					<li><a href="<?=$url?>dashboard">Videos</a></li>
					<li class="active">Agregar Nuevo</li>
				</ul>

				<div class="visible-xs breadcrumb-toggle">
					<a class="btn btn-link btn-lg btn-icon" data-toggle="collapse" data-target=".breadcrumb-buttons"><i class="icon-menu2"></i></a>
				</div>

			</div>
			<!-- /breadcrumbs line -->

			<?php if(isset($success)) : ?>
				<div class="bg-success with-padding">Éxito!: <?=$message?></div>
			<?php endif; ?>

			<?php if(isset($error)) : ?>
				<div class="bg-danger with-padding">Error: <?=$message?></div>
			<?php endif; ?>

	        <form class="form-horizontal" role="form" action="<?=$url?>videos/add" method="POST" enctype="multipart/form-data">

				<!-- Basic inputs -->
		        <div class="panel panel-default">
			        <div class="panel-heading"><h6 class="panel-title"><i class="icon-bubble4"></i> Nuevo Video</h6></div>
	                <div class="panel-body">

	                	<!--<div class="alert alert-success fade in block-inner">
	                		<button type="button" class="close" data-dismiss="alert">×</button>
	                		Los usuarios ingresan al sistema por medio de los dos siguientes datos, Nombre de Usuario y Contraseña.
	                	</div>-->

						<div class="form-group <?php echo (isset($titulo_error)) ? 'has-error' : '';?>">
							<label class="col-sm-2 control-label">Titulo: </label>
							<div class="col-sm-10">
								<input value="<?php echo (isset($user_form['titulo'])) ? $user_form['titulo'] : '';?>" type="text" name="titulo" class="form-control">								
							</div>
						</div>

						<div class="form-group <?php echo (isset($descripcion_error)) ? 'has-error' : '';?>">
							<label class="col-sm-2 control-label">Descripción: </label>
							<div class="col-sm-10">
								<textarea class="form-control" name="descripcion"></textarea>
							</div>
						</div>						

						<div class="form-group <?php echo (isset($link_error)) ? 'has-error' : '';?>">
							<label class="col-sm-2 control-label">Link: </label>
							<div class="col-sm-10">
								<input value="<?php echo (isset($user_form['link'])) ? $user_form['link'] : '';?>" type="text" name="link" class="form-control">								
								<span class="help-block" id="limit-text">Ingresar solo el ID de Youtube ej: <br> link: https://www.youtube.com/watch?v=DhrZQ40DlFM <br> ID: DhrZQ40DlFM</span>
							</div>
						</div>

						<div class="form-actions text-right">
	                    	<input type="submit" value="Guardar" class="btn btn-primary">
	                    </div>
	                </div>

				</div>
				<!-- /basic inputs -->


            </form>