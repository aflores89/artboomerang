		<!-- Page content -->
	 	<div class="page-content">


			<!-- Page header -->
			<div class="page-header">
				<div class="page-title">
					<h3>Videos</h3>
				</div>
			</div>
			<!-- /page header -->


			<!-- Breadcrumbs line -->
			<div class="breadcrumb-line">
				<ul class="breadcrumb">
					<li><a href="<?=$url?>dashboard">Menu Principal</a></li>
					<li class="active">Videos</li>
				</ul>

				<div class="visible-xs breadcrumb-toggle">
					<a class="btn btn-link btn-lg btn-icon" data-toggle="collapse" data-target=".breadcrumb-buttons"><i class="icon-menu2"></i></a>
				</div>

			</div>
			<!-- /breadcrumbs line -->


			<!-- Alert -->
			<!--<div class="alert alert-success fade in block">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <i class="icon-info"></i> Nullam tincidunt dapibus nisi. Aenean porttitor egestas dolor, ut pretium enim vehicula at. Vivamus vulputate risus felis, eget blandit urna aliquam at
            </div>-->
            <!-- /alert -->


	        <!-- Tasks table -->
	        <div class="block">
	        	<h6 class="heading-hr"><i class="icon-grid"></i> Lista</h6>
	            <div class="datatable-tasks">
	                <table class="table table-bordered">
	                    <thead>
	                        <tr>	                        
	                        	<th class="task-date-added">Titulo</th>
	                            <th class="task-priority">Descripción</th>
	                            <th class="task-date-added">Link</th>	      
	                            <th>Destacado</th>                      
	                            <th></th>
	                        </tr>
	                    </thead>
	                    <tbody>
	                    	<?php if(!empty($videos))  : ?>
		                    	<?php foreach ($videos as $key => $value) : ?>
		                        <tr>
		                            <td><?=$value['titulo']?></td>
		                            <td><?=$value['descripcion']?></td>		                            
		                            <td><a href="<?=$value['link']?>" target="_blank"><?=$value['link']?></a></td>		                            
		                            <td class="col-sm-2 text-center">
		                            	<?php if($value['destacado']) : ?>	
		                            		<a href="<?=$url?>videos/inactive/<?=$value['idvideos']?>" style="display:inline-block;"><i class="fa fa-check-square-o" style="font-size:1.5em;"></i></a>
		                            	<?php else : ?>
		                            		<a href="<?=$url?>videos/active/<?=$value['idvideos']?>" style="display:inline-block;"><i class="fa fa-square-o" style="font-size:1.5em;"></i></a>
		                            	<?php endif; ?>
		                            </td>
			                        <td class="col-sm-2">
			                            <div class="btn-group col-lg-4">
				                            <button type="button" class="btn btn-icon btn-success dropdown-toggle" data-toggle="dropdown"><i class="icon-cog4"></i></button>
											<ul class="dropdown-menu icons-right dropdown-menu-right">												
												<li><a href="#edit_modal" class="edit-button" data-idvideos="<?=$value['idvideos']?>" data-toggle="modal" role="button"><i class="icon-pencil"></i> Editar</a></li>												
												<li><a href="#delete_modal" class="delete-button" data-idvideos="<?=$value['idvideos']?>" data-toggle="modal" role="button"><i class="icon-cancel-circle2"></i> Eliminar</a></li>
											</ul>
			                            </div>
			                        </td>
		                        </tr>	   
		                        <?php endforeach; ?>                     
		                    <?php endif; ?>
	                    </tbody>
	                </table>
	            </div>
	        </div>
	        <!-- /tasks table -->


	        <!-- modal change password -->
			<div id="password_modal" class="modal fade in" tabindex="-1" role="dialog" aria-hidden="false">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
							<h4 class="modal-title"><i class="icon-paragraph-justify2"></i>Editar Asesor Telefónico</h4>
						</div>

						<!-- Form inside modal -->
						<form action="<?=$url?>adviser/change_password/" role="form" class="validate" novalidate="novalidate" method="post">

							<input type="hidden" name="iduser" id="iduser-password">

							<div class="modal-body with-padding">								

								<div class="form-group">
									<div class="row">
										<div class="col-sm-6">
											<label>Nueva Contraseña</label>
											<input name="enter_password" type="text" class="required form-control" id="enter_password">
										</div>

										<div class="col-sm-6">
											<label class="control-label">Repetir Contraseña</label>
											<input name="repeat_password" type="text" class="required form-control" id="repeat_password">
										</div>
									</div>
								</div>						
							</div>

							<div class="modal-footer">
								<button type="button" class="btn btn-warning" data-dismiss="modal">Cerrar</button>
								<button type="submit" class="btn btn-primary">Editar Contraseña</button>
							</div>

						</form>
					</div>
				</div>
			</div>
			<!-- -->

			<!-- modal editar -->
			<div id="edit_modal" class="modal fade in" tabindex="-1" role="dialog" aria-hidden="false">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
							<h4 class="modal-title"><i class="icon-paragraph-justify2"></i>Editar Video</h4>
						</div>

						<!-- Form inside modal -->
						<form action="<?=$url?>videos/edit/" role="form" class="validate" novalidate="novalidate" method="post">

							<input type="hidden" name="idvideos" id="idvideos">

							<div class="modal-body with-padding">
								<div class="block-inner text-danger">
									<!--<h6 class="heading-hr" id="title"> <small class="display-block">Please enter your shipping info</small></h6>-->
								</div>

								<div class="form-group">
									<div class="row">
										<div class="col-sm-12">
											<label>Titulo</label>
											<input name="titulo" id="titulo" type="text" class="required form-control">
										</div>
									</div>
								</div>

								<div class="form-group">
									<div class="row">
										<div class="col-sm-12">
											<label>Descripción</label>
											<textarea name="descripcion" id="descripcion" class="form-control"></textarea>
										</div>
									</div>
								</div>

								<div class="form-group">
									<div class="row">
										<div class="col-sm-12">
											<label>Link</label>
											<input name="link" id="link" type="text" class="required form-control">
										</div>
									</div>
								</div>												
							</div>

							<div class="modal-footer">
								<button type="button" class="btn btn-warning" data-dismiss="modal">Cerrar</button>
								<button type="submit" class="btn btn-primary">Editar</button>
							</div>

						</form>
					</div>
				</div>
			</div>
			<!-- -->


			<!-- modal change password -->
			<div id="delete_modal" class="modal fade in" tabindex="-1" role="dialog" aria-hidden="false">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
							<h4 class="modal-title"><i class="icon-paragraph-justify2"></i>Eliminar Video</h4>
						</div>

						<!-- Form inside modal -->
						<form action="<?=$url?>videos/delete/" role="form" class="validate" novalidate="novalidate" method="post">

							<input type="hidden" name="idvideos" id="idvideos-delete">

							<div class="modal-body with-padding">								

								<p>¿Esta seguro que desea eliminar el Curador?</p>					
							</div>

							<div class="modal-footer">
								<button type="button" class="btn btn-warning" data-dismiss="modal">Cerrar</button>
								<button type="submit" class="btn btn-primary">Eliminar</button>
							</div>

						</form>
					</div>
				</div>
			</div>
			<!-- -->

			<script>

				$(function() {

					$('a.delete-button').click(function(event) {
						event.preventDefault();
						var idvideos = $(this).data('idvideos');											
						$('#idvideos-delete').val(idvideos);
					});

					$('a.password-button').click(function(event) {
						var iduser = $(this).data('iduser');											
						$('#iduser-password').val(iduser);																				
					});

					$('a.edit-button').click(function(event) {
						var idvideos = $(this).data('idvideos');
						$('#idvideos').val(idvideos);	
						$.ajax({
							url: '<?=$url?>videos/getVideo/',
							type: 'POST',
							dataType: 'json',
							data: {
								idvideos: idvideos
							}
						})
						.done(function(data) {					
							$('#titulo').val(data[0]['titulo'])		
							$('#descripcion').val(data[0]['descripcion']);	
							$('#link').val(data[0]['link']);							
						});
						
					});

				});				

				
			</script>
        