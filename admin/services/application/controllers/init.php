<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

Class Init extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct() {  
		parent::__construct();		
		$this->load->library(array('appsession', 'user', 'login'));								
	}

	public function login()
	{		
		if($_POST) {
			$username = $this->input->post('username', true);
			$password = $this->input->post('password', true);
			if(empty($username) && empty($password)) {
				$data['username_error'] = false;
				$data['password_error'] = false;
			} else {
				$user = $this->user->getUserByUsernamePassword($username,$password);
				if(!$user) {
					$data['username_error'] = false;
					$data['password_error'] = false;
				} else {
					$data = $user;
					$data['logged_in'] = true;
					$result = $this->appsession->setSession($data);					
					redirect('/dashboard');			
				}
			}
		}
		$data['url'] = base_url();
		$this->load->view('templates/head_login', $data);
		$this->load->view('login', $data);

	}


	public function getUserSession() {
		echo '<pre>';
		$this->appsession->diesession();
	}

	public function getUser() {		
		$this->user->getUser();
	}

	public function setUser() {		
		$this->user->setUser();
	}

	public function getSession() {
		echo '<pre>';
		$this->appsession->diesession();
	}

	public function sessionDestroy() {
		$this->appsession->sessionDestroy();
	}

	public function index()
	{							
		$this->sessionDestroy();
		if(!$this->user->isLoggedIn()) {			
			$this->login();
		} else {
			redirect('/dashboard');
		}
	}	

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */