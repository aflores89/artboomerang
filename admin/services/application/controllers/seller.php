
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

Class Seller extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */	

	public function __construct() {        
		parent::__construct();			
		$this->load->model('user_model');			
		$this->load->library('user');	
		$this->config->load('messages');
	}

	public function index()
	{						
		$advisers = $this->user_model->getUsersByUserType(VENDEDOR);		
		$data['advisers'] = $advisers;			
		$this->load_view('seller/list', $data);
	}		

	public function add()
	{								

		$data = array();

		if($_POST) {

			$first_name = $this->input->post('first_name', true);
			$last_name 	= $this->input->post('last_name', true);
			$username 	= $this->input->post('username', true);
			$password 	= $this->input->post('password', true);
			$phone 		= $this->input->post('phone', true);
			$mail 		= $this->input->post('mail', true);
			$user_type 	= VENDEDOR;

			$data['mail'] = $mail;			
			$data['user_form'] = array(
				'first_name' 	=> $first_name,
				'last_name' 	=> $last_name,
				'username' 		=> $username,
				'password' 		=> $password,
				'phone' 		=> $phone,
				'mail' 			=> $mail,
				'user_type' 	=> $user_type
			);

			if(empty($username) || empty($password) || empty($mail)) {
				if(empty($username)) {
					$data['username_error'] = true;
				} else {
					$data['username'] = $username;
				}
				if(empty($password)) {
					$data['password_error'] = true;
				} else {
					$data['password'] = $password;
				}
				if(empty($mail)) {
					$data['mail_error'] = true;
				}
					
			} else {
				$boolUsername = $this->user->getUserByUsername($username);
				$messages = $this->config->item('messages');				
				if(!$boolUsername) {
					$boolMail = $this->user->getUserByMail($mail);
					if(!$boolMail) {
						$userReturn = $this->user->saveUser($username,$password,$mail,$first_name,$last_name,$phone,$user_type);				
						$data['success'] = true;
						$data['message'] = $messages['success']['seller'];
					} else {
						$data['error'] = true;
						$data['message'] =  $messages['error']['mail'];
						$data['mail_error'] = true;
					}
				} else {
					$data['error'] = true;
					$data['message'] =  $messages['error']['username'];
					$data['username_error'] = true;
				}				
			}

		}

		$this->load_view('seller/add', $data);
	}	

	public function getSeller() 
	{

		$iduser = $this->input->get('iduser', true);
		$user = $this->user->getUserById($iduser);
		echo json_encode($user);

	}

	public function edit() 
	{

		if($_POST) 
		{
			$iduser 	= $this->input->post('iduser', true);
			$first_name = $this->input->post('first_name', true);
			$last_name 	= $this->input->post('last_name', true);
			$username 	= $this->input->post('username', true);
			$phone 		= $this->input->post('phone', true);
			$mail 		= $this->input->post('mail', true);
			
			$result = $this->user->updateUser($first_name,$last_name,$username,$phone,$mail,$iduser);			
			redirect('adviser');

		}		

	}

	public function change_password() 
	{

		if($_POST) 
		{
			$iduser 		= $this->input->post('iduser', true);
			$enter_password = $this->input->post('enter_password', true);		

			$result = $this->user->changePassword($enter_password,$iduser);		
			redirect('adviser');

		}		

	}

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */