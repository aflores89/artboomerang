<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

Class Dashboard extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */	

	public function __construct() {        		
		parent::__construct();					
		$this->load->library('appsession');	
		$this->load->library('requests');	
		$this->load->library('user');	
	}

	public function index()
	{			
		$this->load->model('projects_model');	
		$this->load->model('requests_model');					
		$user = $this->user->getUser();
		$data['contenido'] = $this->requests_model->getContenido();
		$this->load_view('dashboard', $data);		
	}	

	public function saveContenido() {
		$this->load->model('requests_model');
		if($_POST) {

			$titulo = $this->input->post('titulo', true);
			$subtitulo = $this->input->post('subtitulo', true);
			$contenido = $this->input->post('contenido', true);

			$this->requests_model->saveContenido($titulo,$subtitulo,$contenido);

		}
		redirect('dashboard');
	}

	public function save_requests() {
		if($_POST) {

			$first_name = $this->input->post('first_name', true);
			$last_name = $this->input->post('last_name', true);
			$mail = $this->input->post('mail', true);
			$phone = $this->input->post('phone', true);
			$project = $this->input->post('project', true);
			$location = $this->input->post('location', true);
			$consultation = $this->input->post('consultation', true);

			$this->requests->save($first_name,$last_name,$mail,$phone,$project,$location,$consultation);

		}
		redirect('dashboard');
	}

	public function logout() {
		$this->appsession->sessionDestroy();
		redirect('init');
	}

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */