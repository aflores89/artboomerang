<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

Class Reports extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */	

	public function __construct() {        		
		parent::__construct();					
		$this->load->library('appsession');	
		$this->load->library('requests');	
		$this->load->library('user');	
	}

	public function add() {
		$this->load->model('reports_model');
		if($_POST) {

			$answer = $this->input->post('answer', true);
			$mail = $this->input->post('mail', true);
			$interested = $this->input->post('interested', true);
			$chanel = $this->input->post('chanel', true);
			$medio = $this->input->post('medio', true);
			$observations = $this->input->post('observations', true);
			$idrequest = $this->input->post('idrequest', true);

			$this->reports_model->add($answer,$mail,$interested,$chanel,$medio,$observations,$idrequest);

		}
		redirect('adviser/dashboard');
	}

	public function logout() {
		$this->appsession->sessionDestroy();
		redirect('init');
	}

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */