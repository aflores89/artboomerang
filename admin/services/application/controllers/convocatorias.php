<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

Class Convocatorias extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */	

	public function __construct() {        
		parent::__construct();			
		$this->load->model('artistas_model');			
		$this->load->model('galeria_model');			
		$this->load->model('convocatorias_model');			
		$this->load->model('provincias_model');		
		$this->load->library('user');	
		$this->config->load('messages');
	}

	public function index()	{						
		$convocatorias = $this->convocatorias_model->getConvocatorias();		
		$data['convocatorias'] = $convocatorias;					
		$this->load_view('convocatorias/list', $data);
	}		

	public function listar($id) {
		$artista = $this->artistas_model->getArtistaById($id);
		$imagenes = $this->galeria_model->getGaleria($id);		
		$data['imagenes'] = $imagenes;					
		$data['artista'] = $artista;					
		$this->load_view('convocatoria/list', $data);
	}

	public function add() {

		$resultUpload = $this->uploadFile();
		$nombre = $this->input->post('nombre', true);
		$data = [];

		if(!empty($nombre)) {
			if($resultUpload) {
				$filename = $resultUpload['upload_data']['orig_name'];
				$filepath = 'uploads/convocatorias/'.$resultUpload['upload_data']['orig_name'];
								
				$userReturn = $this->convocatorias_model->saveConvocatoria($nombre,$filepath);				
				$data['success'] = true;
				$data['message'] = 'Se agrego la imagen correctamente.';								
				
			} else {
				$data['error'] = true;
				$data['message'] = 'Ocurrio un error.';								
			}
		} 		

		$this->load_view('convocatorias/add', $data);
	}

	public function delete()	{
		$id = $this->input->post('idconvocatorias', true);
		$result = $this->convocatorias_model->delete($id);

		if($result) {			
			redirect('/convocatorias/');
		}
	}

	public function active($id) {
		$result = $this->convocatorias_model->active($id);
		if($result) {
			redirect('convocatorias');
		}
	}

	public function inactive($id) {
		$result = $this->convocatorias_model->inactive($id);
		if($result) {
			redirect('convocatorias');
		}	
	}

	public function uploadFile() {

		$config['upload_path'] = './uploads/convocatorias/';
		$config['allowed_types'] = 'pdf|doc';
		$config['max_size']	= '10000';

		$this->load->library('upload', $config);

		if ( ! $this->upload->do_upload())
		{
			$error = array('error' => $this->upload->display_errors());
			return false;
		}
		else
		{
			$data = array('upload_data' => $this->upload->data());

			return $data;
		}
	}

	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */