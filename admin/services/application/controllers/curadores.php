<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

Class Curadores extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */	

	public function __construct() {        
		parent::__construct();			
		$this->load->model('artistas_model');			
		$this->load->model('galeria_model');			
		$this->load->model('curadores_model');			
		$this->load->model('provincias_model');		
		$this->load->library('user');	
		$this->config->load('messages');
	}

	public function index()	{						
		$curadores = $this->curadores_model->getCuradores();		
		$data['curadores'] = $curadores;					
		$this->load_view('curadores/list', $data);
	}		

	public function listar($id) {
		$artista = $this->artistas_model->getArtistaById($id);
		$imagenes = $this->galeria_model->getGaleria($id);		
		$data['imagenes'] = $imagenes;					
		$data['artista'] = $artista;					
		$this->load_view('curadores/list', $data);
	}

    public function editperfil() {

        if($_POST) {
            $idcurador 	= $this->input->post('idcurador', true);
            $resultUpload = $this->uploadFile();
            if($resultUpload) {
                $filename = $resultUpload['upload_data']['orig_name'];
                $filepath = 'uploads/curadores/'.$resultUpload['upload_data']['orig_name'];
            } else {
                $filename = 'imagen_no.jpg';
                $filepath = 'uploads/artistas/perfil/imagen_no.jpg';
            }

            $result = $this->curadores_model->updateCuradorPerfil($idcurador,$filename,$filepath);

            redirect('/curadores/');
        }

    }

	public function add() {

		$resultUpload = $this->uploadFile();
		$nombre = $this->input->post('nombre', true);
		$extracto = $this->input->post('extracto', true);
        $subtitulo = $this->input->post('subtitulo', true);
        $orden = $this->input->post('orden', true);
		$data = [];

		if(!empty($nombre) || !empty($extracto)) {
			if($resultUpload) {
				$filename = $resultUpload['upload_data']['orig_name'];
				$filepath = 'uploads/curadores/'.$resultUpload['upload_data']['orig_name'];
								
				$userReturn = $this->curadores_model->saveCurador($nombre,$extracto,$filepath, $subtitulo, $orden);
				$data['success'] = true;
				$data['message'] = 'Se agrego el Curador correctamente.';								
				
			} else {
				$data['error'] = true;
				$data['message'] = 'Ocurrio un error.';								
			}
		} 		

		$this->load_view('curadores/add', $data);
	}

	public function delete()	{
		$id = $this->input->post('idcuradores', true);
		$result = $this->curadores_model->delete($id);

		if($result) {			
			redirect('/curadores/');
		}
	}

	public function edit() {
		if($_POST) {
			$idcuradores 	= $this->input->post('idcuradores', true);
			$nombre 	= $this->input->post('nombre', true);
			$extracto 		= $this->input->post('extracto', true);
            $subtitulo 		= $this->input->post('subtitulo', true);
            $orden 		= $this->input->post('orden', true);
			
			$result = $this->curadores_model->updateCurador($nombre,$extracto,$idcuradores, $subtitulo, $orden);
			redirect('/curadores/');
		}		
	}


	public function active($id) {
		$result = $this->curadores_model->active($id);
		if($result) {
			redirect('curadores');
		}
	}

	public function inactive($id) {
		$result = $this->curadores_model->inactive($id);
		if($result) {
			redirect('curadores');
		}	
	}

	public function getCurador() {
		$idcuradores = $this->input->post('idcuradores', true);
		$result = $this->curadores_model->getCuradorById($idcuradores);
		echo json_encode($result);
	}

	public function uploadFile() {

		$config['upload_path'] = './uploads/curadores/';
		$config['allowed_types'] = 'jpg|gif|png';
		$config['max_size']	= '10000';
		$config['max_width']  = '4929';
		$config['max_height']  = '3265';

		$this->load->library('upload', $config);

		if ( ! $this->upload->do_upload())
		{
			$error = array('error' => $this->upload->display_errors());
			return false;
		}
		else
		{
			$data = array('upload_data' => $this->upload->data());

			return $data;
		}
	}

	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */