
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

Class Projects extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */	

	public function __construct() {        
		parent::__construct();			
		$this->load->model('projects_model');			
		$this->load->library('user');	
		$this->config->load('messages');
	}

	public function index()
	{					
		$projects = $this->projects_model->get();		
		$data['projects'] = $projects;			
		$this->load_view('projects/list', $data);
	}		

	public function add()
	{								

		$data = array();

		if($_POST) {

			$projectname = $this->input->post('projectname', true);

			$data['user_form'] = array(
				'projectname' 	=> $projectname,
			);

			if(empty($projectname)) {
				if(empty($projectname)) {
					$data['projectname_error'] = true;
				} else {
					$data['projectname'] = $projectname;
				}
					
			} else {
				$messages = $this->config->item('messages');				
				$projectReturn = $this->projects_model->add($projectname);				
				$data['success'] = true;
				$data['message'] = $messages['success']['project'];
				
			}

		}

		$this->load_view('projects/add', $data);
	}	

	public function edit() 
	{

		if($_POST) 
		{
			$name 	= $this->input->post('project-name', true);
			$idproject 	= $this->input->post('idproject', true);
			
			$result = $this->projects_model->edit($idproject,$name);						

		}
		redirect('projects');

	}

	public function delete() 
	{
		if($_POST) 
		{
			$idproject 	= $this->input->post('idproject', true);
			$result = $this->projects_model->delete($idproject);						

		}
		redirect('projects');
	}

	public function getProject() 
	{

		$idproject = $this->input->get('idproject', true);
		$project = $this->projects_model->get($idproject);
		echo json_encode($project);

	}

	


}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */