<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

Class Noticias extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */	

	public function __construct() {        
		parent::__construct();			
		$this->load->model('noticias_model');			
		$this->load->library('user');	
		$this->config->load('messages');
	}

	public function index()	{						
		$noticias = $this->noticias_model->getNoticias();		
		$data['noticias'] = $noticias;					
		$this->load_view('noticias/list', $data);
	}		

	public function listar($id) {
		$artista = $this->artistas_model->getArtistaById($id);
		$imagenes = $this->galeria_model->getGaleria($id);		
		$data['imagenes'] = $imagenes;					
		$data['artista'] = $artista;					
		$this->load_view('curadores/list', $data);
	}

	public function add() {

		$resultUpload = $this->uploadFile();
		$titular = $this->input->post('titular', true);
		$lead = $this->input->post('lead', true);
		$contenido = $this->input->post('contenido', true);
		$epigrafe = $this->input->post('epigrafe', true);
		$autor = $this->input->post('autor', true);
		$data = [];

		if(!empty($titular) || !empty($lead) || !empty($contenido) || !empty($epigrafe) || !empty($autor)) {
			if($resultUpload) {
				$filename = $resultUpload['upload_data']['orig_name'];
				$filepath = 'uploads/noticias/'.$resultUpload['upload_data']['orig_name'];
								
				$userReturn = $this->noticias_model->saveNoticia($titular,$lead,$contenido,$filepath,$epigrafe,$autor);				
				$data['success'] = true;
				$data['message'] = 'Se agrego la Noticia correctamente.';								
				
			} else {
				$userReturn = $this->noticias_model->saveNoticia($titular,$lead,$contenido,'',$contenido);				
				$data['success'] = true;
				$data['message'] = 'Se agrego la Noticia correctamente.';		
			}
		} 		

		$this->load_view('noticias/add', $data);
	}

	public function delete()	{
		$id = $this->input->post('idcuradores', true);
		$result = $this->curadores_model->delete($id);

		if($result) {			
			redirect('/curadores/');
		}
	}

	public function active($id) {
		$result = $this->curadores_model->active($id);
		if($result) {
			redirect('curadores');
		}
	}

	public function inactive($id) {
		$result = $this->curadores_model->inactive($id);
		if($result) {
			redirect('curadores');
		}	
	}

	public function uploadFile() {

		$config['upload_path'] = './uploads/noticias/';
		$config['allowed_types'] = 'jpg|gif|png';
		$config['max_size']	= '10000';
		$config['max_width']  = '4929';
		$config['max_height']  = '3265';

		$this->load->library('upload', $config);

		if ( ! $this->upload->do_upload())
		{
			$error = array('error' => $this->upload->display_errors());
			return false;
		}
		else
		{
			$data = array('upload_data' => $this->upload->data());

			return $data;
		}
	}

	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */