<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

Class Artistas extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */	

	public function __construct() {        
		parent::__construct();			
		$this->load->model('artistas_model');			
		$this->load->model('provincias_model');		
		$this->load->library('user');	
		$this->config->load('messages');
	}

	public function index()	{						
		$artistas = $this->artistas_model->getArtistas();		
		$data['artistas'] = $artistas;					
		$this->load_view('artistas/list', $data);
	}		

	public function add() {								
		$data = array();

		if($_POST) {
			$nombre = $this->input->post('nombre', true);		
			$biografia 	= $this->input->post('biografia', true);
			$statment 	= $this->input->post('statment', true);
			$url 		= $this->input->post('url', true);
			$provincia 	= $this->input->post('provincia', true);
			$user_type 	= ARTISTA;
		
			$data['user_form'] = array(
				'nombre' 		=> $nombre,
				'biografia' 	=> $biografia,
				'statment' 		=> $statment,
				'url' 			=> $url,
				'provincia' 	=> $provincia
			);

			$resultUpload = $this->uploadFile();
			if($resultUpload) {
				$filename = $resultUpload['upload_data']['orig_name'];
				$filepath = 'uploads/artistas/perfil/'.$resultUpload['upload_data']['orig_name'];
			} else {
				$filename = 'imagen_no.jpg';
				$filepath = 'uploads/artistas/perfil/imagen_no.jpg';
			}
			
			if(empty($nombre)) {
				if(empty($nombre)) {
					$data['nombre_error'] = true;
				} else {
					$data['nombre'] = $nombre;
				}
					
			} else {					
				$userReturn = $this->artistas_model->saveArtista($nombre,$biografia,$statment,$url,$provincia,$filename,$filepath);				
				$data['success'] = true;
				$data['message'] = 'Se agrego el artista correctamente.';								
			}
		
		}
		$data['provincias'] = $this->provincias_model->getProvincias();	
		$this->load_view('artistas/add', $data);
	}	

	public function editperfil() {



		if($_POST) {
			$idartista 	= $this->input->post('idartista', true);
			$resultUpload = $this->uploadFile();
			if($resultUpload) {
				$filename = $resultUpload['upload_data']['orig_name'];
				$filepath = 'uploads/artistas/perfil/'.$resultUpload['upload_data']['orig_name'];
			} else {
				$filename = 'imagen_no.jpg';
				$filepath = 'uploads/artistas/perfil/imagen_no.jpg';
			}
			
			$result = $this->artistas_model->updateArtistaPerfil($idartista,$filename,$filepath);			

			redirect('/artistas/');
		}	

	}

	public function uploadFile() {

		$config['upload_path'] = './uploads/artistas/perfil/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size']	= '10000';
		$config['max_width']  = '4929';
		$config['max_height']  = '3265';

		$this->load->library('upload', $config);

		if ( ! $this->upload->do_upload())
		{
			$error = array('error' => $this->upload->display_errors());
			return false;
		}
		else
		{
			$data = array('upload_data' => $this->upload->data());

			return $data;
		}
	}

	public function getArtista() {
		$idartista = $this->input->post('idartista', true);
		$result = $this->artistas_model->getArtistaById($idartista);
		echo json_encode($result);
	}

	public function links($id) {
		$result = $this->artistas_model->getLinks($id);
		
	}

	public function asign_request()	{
		$this->load->model('requests_model');
		$iduser = $this->input->post('idadviser', true);
		$idrequest = $this->input->post('idrequest', true);

		$result = $this->requests_model->asignRequestToUser($iduser, $idrequest);

		echo json_encode($result);
	}

	public function delete_asign()	{
		$this->load->model('requests_model');	
		$idrequest = $this->input->post('idrequest', true);
		$result = $this->requests_model->deleteAsign($idrequest);

		echo json_encode($result);
	}

	public function edit() {
		if($_POST) {
			$idartista 	= $this->input->post('idartista', true);
			$nombre 	= $this->input->post('nombre', true);
			$biografia 	= $this->input->post('biografia', true);
			$statment 	= $this->input->post('statment', true);
			$link 		= $this->input->post('link', true);
			
			$result = $this->artistas_model->updateArtista($nombre,$biografia,$statment,$link,$idartista);			
			redirect('/artistas/');
		}		
	}

	public function delete()	{
		$this->load->model('artistas_model');	
		$idartista = $this->input->post('idartista', true);
		$result = $this->artistas_model->deleteArtista($idartista);

		if($result) {
			redirect('/artistas/');
		}
	}

	public function change_password() {
		if($_POST) {
			$iduser 		= $this->input->post('iduser', true);
			$enter_password = $this->input->post('enter_password', true);		

			$result = $this->user->changePassword($enter_password,$iduser);		
			redirect('adviser');
		}		
	}

	public function dashboard() {		
		$this->load->library('requests');
		$this->load->model('projects_model');
		$user = $this->user->getUser();

		$data['requests'] = $this->requests->get($user['idusers']);	
		$data['sellers'] = $this->user->get(VENDEDOR);	
		$data['projects'] = $this->projects_model->get();
				
		$this->load_view('adviser/dashboard', $data);		
	}

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */