<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

Class Galeria extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */	

	public function __construct() {        
		parent::__construct();			
		$this->load->model('artistas_model');			
		$this->load->model('galeria_model');			
		$this->load->model('provincias_model');		
		$this->load->library('user');	
		$this->config->load('messages');
	}

	public function index()	{						
		$artistas = $this->artistas_model->getArtistas();		
		$data['artistas'] = $artistas;					
		$this->load_view('galeria/list', $data);
	}		

	public function listar($id) {
		$artista = $this->artistas_model->getArtistaById($id);
		$imagenes = $this->galeria_model->getGaleria($id);		
		$data['imagenes'] = $imagenes;					
		$data['artista'] = $artista;					
		$this->load_view('galeria/list', $data);
	}

	public function add() {

		$idartista = $this->input->post('idartista');
		$resultUpload = $this->uploadFile();
		$statment = $this->input->post('statment', true);

		if(!empty($statment)) {
			if($resultUpload) {
				$filename = $resultUpload['upload_data']['orig_name'];
				$filepath = 'uploads/artistas/galeria/'.$resultUpload['upload_data']['orig_name'];
								
				$userReturn = $this->galeria_model->saveImagen($statment, $filename,$filepath,$idartista);				
				$data['success'] = true;
				$data['message'] = 'Se agrego la imagen correctamente.';								
				
			} else {
				$data['error'] = true;
				$data['message'] = 'Ocurrio un error.';								
			}
		} 		

		redirect('/galeria/listar/'.$idartista);
	}

	public function delete()	{
		$id = $this->input->post('idgaleria', true);
		$image = $this->galeria_model->getImageById($id);
		$idartista = $image[0]['idartistas'];
		$result = $this->galeria_model->deleteImagen($id);

		if($result) {			
			redirect('/galeria/listar/'.$idartista);
		}
	}

	public function uploadFile() {

		$config['upload_path'] = './uploads/artistas/galeria/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size']	= '10000';
		$config['max_width']  = '4929';
		$config['max_height']  = '3265';

		$this->load->library('upload', $config);

		if ( ! $this->upload->do_upload())
		{
			$error = array('error' => $this->upload->display_errors());
			//die(var_dump($error));
			return false;
		}
		else
		{
			$data = array('upload_data' => $this->upload->data());

			return $data;
		}
	}

	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */