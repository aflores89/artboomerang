<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

Class Videos extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */	

	public function __construct() {        
		parent::__construct();				
		$this->load->model('videos_model');			
		$this->load->library('user');	
		$this->config->load('messages');
	}

	public function index()	{						
		$videos = $this->videos_model->getVideos();		
		$data['videos'] = $videos;					
		$this->load_view('videos/list', $data);
	}		

	public function listar($id) {
		$artista = $this->artistas_model->getArtistaById($id);
		$imagenes = $this->galeria_model->getGaleria($id);		
		$data['imagenes'] = $imagenes;					
		$data['artista'] = $artista;					
		$this->load_view('videos/list', $data);
	}

	public function getVideo() {
		$idvideos = $this->input->post('idvideos', true);
		$result = $this->videos_model->getVideoById($idvideos);
		echo json_encode($result);
	}

	public function add() {

		$titulo = $this->input->post('titulo', true);
		$descripcion = $this->input->post('descripcion', true);
		$link = $this->input->post('link', true);
		$data = [];

		if(!empty($titulo) || !empty($descripcion) || !empty($link)) {
								
			$userReturn = $this->videos_model->saveVideo($titulo,$descripcion,$link);				
			$data['success'] = true;
			$data['message'] = 'Se agrego el Video correctamente.';								

		}

		$this->load_view('videos/add', $data);
	}

	public function delete()	{
		$id = $this->input->post('idvideos', true);
		$result = $this->videos_model->delete($id);

		if($result) {			
			redirect('/videos/');
		}
	}

	public function edit() {
		if($_POST) {
			$idvideos 		= $this->input->post('idvideos', true);
			$titulo 		= $this->input->post('titulo', true);
			$descripcion 	= $this->input->post('descripcion', true);
			$link 			= $this->input->post('link', true);

			
			$result = $this->videos_model->updateVideos($titulo,$descripcion,$link,$idvideos);			
			redirect('/videos/');
		}		
	}


	public function active($id) {
		$result = $this->videos_model->active($id);
		if($result) {
			redirect('videos');
		}
	}

	public function inactive($id) {
		$result = $this->videos_model->inactive($id);
		if($result) {
			redirect('videos');
		}	
	}

	public function uploadFile() {

		$config['upload_path'] = './uploads/curadores/';
		$config['allowed_types'] = 'jpg|gif|png';
		$config['max_size']	= '10000';
		$config['max_width']  = '4929';
		$config['max_height']  = '3265';

		$this->load->library('upload', $config);

		if ( ! $this->upload->do_upload())
		{
			$error = array('error' => $this->upload->display_errors());
			return false;
		}
		else
		{
			$data = array('upload_data' => $this->upload->data());

			return $data;
		}
	}

	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */