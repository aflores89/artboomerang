$(function() {
	var i = 1;	

	var height = $('aside').parent().parent('section').height();
	$('aside').height(height);

	$( window ).scroll(function() {
		var top = $(document).scrollTop();
		var height = $(this).height();

		$('.opacity').css({
			opacity: 1-(top/height)*2
		});
	});	
	adjustHeight();
	owlSlider();

	$('header.interna').find('li a')
		.on('mouseover', function() {		
			$(this).children('span').addClass('animate');
		})
		.on('mouseout', function() {		
			$(this).children('span').removeClass('animate');
		})

	$('#ampliar').on('click', function(e) {
		e.preventDefault();
		$('#map').toggleClass('ampliar');
	});

	$(".video").fitVids();

});

var owlSlider = function() {
	$("#owl-demo").owlCarousel({
 
		navigation : true, // Show next and prev buttons
		slideSpeed : 300,
		paginationSpeed : 400,
		singleItem:true,
		navigationText: ['Anterior','Siguiente'],
		autoHeight : true	
	  });
}

var adjustHeight = function() {
		var contentHeight = parseInt($('.wrapp').css('height'));
		var seguinos = parseInt($('.seguinos').css('height'));
		artista = $('.wrapp').data('artista');
		if(artista) {
			var artistas = parseInt($('.artistas').css('height'));					
			$('.convocatoria').height(contentHeight-seguinos-artistas);	
			
		} else {		
			$('.convocatoria').height(contentHeight-seguinos);	
		}
		console.log(contentHeight);
		console.log(seguinos);
		console.log(artistas);	
	
}
