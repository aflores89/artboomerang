	<?php include('templates/head.php'); ?>

	<header>					
		<div class="container">
			<div class="row">
				<div class="col-lg-3">
					<div class="logo">
						<div class="franja"></div>	
						<img src="assets/images/logo.jpg" alt="" class="img-responsive">
					</div>
				</div>
				<div class="col-lg-6">
					<nav class="nav">
						<ul class="navbar-nav nav">
							<li><a href="#">ART BOOMERANG</a></li>						
							<li><a href="#">ARTISTAS</a></li>
							<li><a href="#">VIDEOS</a></li>
							<li><a href="#">NOTICIAS</a></li>
							<li><a href="#">CONTACTO</a></li>
						</ul>
					</nav>
				</div>
			</div>
		</div>
	</header>

	<section id="artistas" class="wrapp">
		<div class="container">
			<article class="col-lg-8">
				<div class="title">
					<h2>Art Boomerang</h2>
					<span>Artistas.</span>
				</div>				
				<div class="row">
					<div class="col-lg-12 ">
						<h3 class="title color1"><b>.Chaco</b> Art Boomerang</h3>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-4">
						<ul>
							<li><a href="#">Luciano Acosta</a></li>
							<li><a href="#">Patricia Alicia T. Andino</a></li>
							<li><a href="#">Nadia Aida Aquino</a></li>
							<li><a href="#">Adrian Matias Arzu</a></li>
							<li><a href="#">Elizabeth Rosana Guadalupe Bernal</a></li>
							<li><a href="#">Maria Marta Caccioppoli</a></li>
						</ul>
					</div>
					<div class="col-lg-4">
						<ul>
							<li><a href="#">Rita Chans</a></li>
							<li><a href="#">Andrea Soledad Geat</a></li>
							<li><a href="#">Maria Victoria Gonzalez</a></li>
							<li><a href="#">Claudia Elcira Guillen</a></li>
							<li><a href="#">Fabiana Larrea</a></li>
							<li><a href="#">Daniela Ramsfelder</a></li>
						</ul>
					</div>
					<div class="col-lg-4">
						<ul>
							<li><a href="#">Manuel Alejandro Sanchez</a></li>
							<li><a href="#">Monserrat Mercedes Solis Carnicer</a></li>
							<li><a href="#">Laura Isabel Sosa</a></li>
							<li><a href="#">Monica Beatriz Vakaruk</a></li>
							<li><a href="#">Silvia Gabriela Wasch</a></li>
						</ul>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12 ">
						<h3 class="title color2"><b>.Corrientes</b> Art Boomerang</h3>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-4">
						<ul>
							<li><a href="#">Maria Teresa Abelleda</a></li>
							<li><a href="#">Cristian Andres Baez</a></li>
							<li><a href="#">Jose Maria Benitez</a></li>
							<li><a href="#">Camilo Candia</a></li>
							<li><a href="#">Carla Capitani</a></li>
							<li><a href="#">Norma Capponcelli</a></li>
							<li><a href="#">Osiris Nelson Corrales</a></li>
							<li><a href="#">Juan Ramon Gutierrez</a></li>
							<li><a href="#">Barbara Antonini</a></li>
							<li><a href="#">ARTENUESTRO - Maccarini</a></li>
							<li><a href="#">Baldoni, Imperator</a></li>
						</ul>
					</div>
					<div class="col-lg-4">
						<ul>
							<li><a href="#">Debora Andrea Duran</a></li>
							<li><a href="#">Ramiro Gauna</a></li>
							<li><a href="#">Hugo Justiniano</a></li>
							<li><a href="#">Luis Angel Llarens</a></li>
							<li><a href="#">Daniela Alejandra Malvicino</a></li>
							<li><a href="#">Maria Sol Navas</a></li>
							<li><a href="#">Teresita Gonzalez</a></li>
							<li><a href="#">Lelia Ingrid Avalos</a></li>
							<li><a href="#">Maria Paula Bekun</a></li>
							<li><a href="#">Carlos Calmanash</a></li>
							<li><a href="#">Monica Andrea Frette</a></li>
						</ul>
					</div>
					<div class="col-lg-4">
						<ul>
							<li><a href="#">Benjamin Jorge Pujol</a></li>
							<li><a href="#">Hernan Rodolfo Ramirez</a></li>
							<li><a href="#">Lidia Susana Romero Lagrau</a></li>
							<li><a href="#">Claudio Gabriel Rupani</a></li>
							<li><a href="#">Zunilda de Jesus Silvia</a></li>
							<li><a href="#">Cesar Gustavo Tschanz</a></li>
							<li><a href="#">Cristian Velazco</a></li>
							<li><a href="#">Juan Paulino (Pali Paye) Gonzalez</a></li>
							<li><a href="#">Nicolas Kernohan</a></li>
							<li><a href="#">Gustavo Fernando Medina</a></li>
							<li><a href="#">Silvia Insaurralde</a></li>
						</ul>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12 ">
						<h3 class="title color3"><b>.Formosa</b> Art Boomerang</h3>						
					</div>
				</div>
				<div class="row">
					<div class="col-lg-4">
						<ul>
							<li><a href="#">Alicia Noemi Aballay</a></li>
							<li><a href="#">Hercilia Alvarez</a></li>
							<li><a href="#">Ana Maria (Kuki) Bensky</a></li>
							<li><a href="#">Claudia Noemi Cabral</a></li>
							<li><a href="#">Leandro Nazaret Caraballo</a></li>
						</ul>
					</div>
					<div class="col-lg-4">
						<ul>
							<li><a href="#">Sergio Ricardo Dellagnolo</a></li>
							<li><a href="#">Martin Escobar</a></li>
							<li><a href="#">Mirtha Ferreyra</a></li>
							<li><a href="#">Hugo Jose Marti</a></li>
							<li><a href="#">Luis Antonio Pastor Padron</a></li>
						</ul>
					</div>
					<div class="col-lg-4">
						<ul>
							<li><a href="#">David Nestor Schenone</a></li>
							<li><a href="#">Mauro Alejandro Solalinde</a></li>
							<li><a href="#">Rita Ines Chans Galindo</a></li>
							<li><a href="#">Pablo Daniel Rigonatto</a></li>
							<li><a href="#">Zulinda Centurion</a></li>
							<li><a href="#">Horacio Pelayo</a></li>
						</ul>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12">
						<h3 class="title color4"><b>.Córdoba</b> Art Boomerang</h3>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-4">
						<ul>
							<li><a href="#">Christian Roman</a></li>
							<li><a href="#">Cielo Galindez</a></li>
							<li><a href="#">Claudia Esther Olivera Lotito</a></li>
							<li><a href="#">Florencia Walter</a></li>
							<li><a href="#">Emilio Zarate</a></li>
							<li><a href="#">Guillermo Mena</a></li>
							<li><a href="#">Karina Plensa</a></li>
						</ul>
					</div>
					<div class="col-lg-4">
						<ul>
							<li><a href="#">Marcela Bosch</a></li>
							<li><a href="#">Martin Viecens</a></li>
							<li><a href="#">Mercedes Ferreyra</a></li>
							<li><a href="#">Mercedes Laguinge</a></li>
							<li><a href="#">Paola Sferco</a></li>
							<li><a href="#">Romina Castiñeira</a></li>
							<li><a href="#">Romina Puigdemasa</a></li>
						</ul>
					</div>
					<div class="col-lg-4">
						<ul>
							<li><a href="#">Sofía Torres Kosiba</a></li>
							<li><a href="#">Verónica Maggi</a></li>
							<li><a href="#">Verónica Molas</a></li>
							<li><a href="#">Ramiro A Gonzalez Etchague</a></li>
							<li><a href="#">Lucas Aguirre</a></li>
						</ul>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12 ">
						<h3 class="title color5"><b>.Misiones</b> Art Boomerang</h3>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-4">
						<ul>
							<li><a href="#">Christian Roman</a></li>
							<li><a href="#">Cielo Galindez</a></li>
							<li><a href="#">Claudia Esther Olivera Lotito</a></li>
							<li><a href="#">Florencia Walter</a></li>
							<li><a href="#">Emilio Zarate</a></li>
							<li><a href="#">Guillermo Mena</a></li>
							<li><a href="#">Karina Plensa</a></li>
						</ul>
					</div>
					<div class="col-lg-4">
						<ul>
							<li><a href="#">Marcela Bosch</a></li>
							<li><a href="#">Martin Viecens</a></li>
							<li><a href="#">Mercedes Ferreyra</a></li>
							<li><a href="#">Mercedes Laguinge</a></li>
							<li><a href="#">Paola Sferco</a></li>
							<li><a href="#">Romina Castiñeira</a></li>
							<li><a href="#">Romina Puigdemasa</a></li>
						</ul>
					</div>
					<div class="col-lg-4">
						<ul>
							<li><a href="#">Sofía Torres Kosiba</a></li>
							<li><a href="#">Verónica Maggi</a></li>
							<li><a href="#">Verónica Molas</a></li>
							<li><a href="#">Ramiro A Gonzalez Etchague</a></li>
							<li><a href="#">Lucas Aguirre</a></li>
						</ul>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12">
						<h3 class="title color6"><b>.Rio Negro</b> Art Boomerang</h3>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-4">
						<ul>
							<li><a href="#">Christian Roman</a></li>
							<li><a href="#">Cielo Galindez</a></li>
							<li><a href="#">Claudia Esther Olivera Lotito</a></li>
							<li><a href="#">Florencia Walter</a></li>
							<li><a href="#">Emilio Zarate</a></li>
							<li><a href="#">Guillermo Mena</a></li>
							<li><a href="#">Karina Plensa</a></li>
						</ul>
					</div>
					<div class="col-lg-4">
						<ul>
							<li><a href="#">Marcela Bosch</a></li>
							<li><a href="#">Martin Viecens</a></li>
							<li><a href="#">Mercedes Ferreyra</a></li>
							<li><a href="#">Mercedes Laguinge</a></li>
							<li><a href="#">Paola Sferco</a></li>
							<li><a href="#">Romina Castiñeira</a></li>
							<li><a href="#">Romina Puigdemasa</a></li>
						</ul>
					</div>
					<div class="col-lg-4">
						<ul>
							<li><a href="#">Sofía Torres Kosiba</a></li>
							<li><a href="#">Verónica Maggi</a></li>
							<li><a href="#">Verónica Molas</a></li>
							<li><a href="#">Ramiro A Gonzalez Etchague</a></li>
							<li><a href="#">Lucas Aguirre</a></li>
						</ul>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12">
						<h3 class="title color7"><b>.Neuquen</b> Art Boomerang</h3>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-4">
						<ul>
							<li><a href="#">Alicia Noemi Aballay</a></li>
							<li><a href="#">Hercilia Alvarez</a></li>
							<li><a href="#">Ana Maria (Kuki) Bensky</a></li>
							<li><a href="#">Claudia Noemi Cabral</a></li>
							<li><a href="#">Leandro Nazaret Caraballo</a></li>
						</ul>
					</div>
					<div class="col-lg-4">
						<ul>
							<li><a href="#">Sergio Ricardo Dellagnolo</a></li>
							<li><a href="#">Martin Escobar</a></li>
							<li><a href="#">Mirtha Ferreyra</a></li>
							<li><a href="#">Hugo Jose Marti</a></li>
							<li><a href="#">Luis Antonio Pastor Padron</a></li>
						</ul>
					</div>
					<div class="col-lg-4">
						<ul>
							<li><a href="#">David Nestor Schenone</a></li>
							<li><a href="#">Mauro Alejandro Solalinde</a></li>
							<li><a href="#">Rita Ines Chans Galindo</a></li>
							<li><a href="#">Pablo Daniel Rigonatto</a></li>
							<li><a href="#">Zulinda Centurion</a></li>
							<li><a href="#">Horacio Pelayo</a></li>
						</ul>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12 ">
						<h3 class="title color8"><b>.Chubut</b> Art Boomerang</h3>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-4">
						<ul>
							<li><a href="#">Alicia Noemi Aballay</a></li>
							<li><a href="#">Hercilia Alvarez</a></li>
							<li><a href="#">Ana Maria (Kuki) Bensky</a></li>
							<li><a href="#">Claudia Noemi Cabral</a></li>
							<li><a href="#">Leandro Nazaret Caraballo</a></li>
						</ul>
					</div>
					<div class="col-lg-4">
						<ul>
							<li><a href="#">Sergio Ricardo Dellagnolo</a></li>
							<li><a href="#">Martin Escobar</a></li>
							<li><a href="#">Mirtha Ferreyra</a></li>
							<li><a href="#">Hugo Jose Marti</a></li>
							<li><a href="#">Luis Antonio Pastor Padron</a></li>
						</ul>
					</div>
					<div class="col-lg-4">
						<ul>
							<li><a href="#">David Nestor Schenone</a></li>
							<li><a href="#">Mauro Alejandro Solalinde</a></li>
							<li><a href="#">Rita Ines Chans Galindo</a></li>
							<li><a href="#">Pablo Daniel Rigonatto</a></li>
							<li><a href="#">Zulinda Centurion</a></li>
							<li><a href="#">Horacio Pelayo</a></li>
						</ul>
					</div>
				</div>
			</article>
			<div class="col-lg-4 aside">				
				<div class="item seguinos">
					<hgroup>
						<h2>Seguinos</h2>
					</hgroup>
					<div class="social">
						<ul>
							<li>
								<a href="http://www.facebook.com/ArtBoomerangProgramaFederalParaLasArtes" target="_blank"><i class="fa fa-facebook"></i></a>
							</li>
							<li>
								<a href="https://twitter.com/art_boomerang" target="_blank"><i class="fa fa-twitter"></i></a>
							</li>
							<li>
								<a href="https://vimeo.com/artboomerang" target="_blank"><i class="fa fa-vimeo"></i></a>
							</li>						
							<li>
								<a href="https://www.youtube.com/channel/UCIaAoj_Be77FV6oUZpkDNww?view_as=subscriber" target="_blank"><i class="fa fa-youtube"></i></a>
							</li>
						</ul>
						<ul>
							<li>
								<a href="https://www.youtube.com/channel/UCIaAoj_Be77FV6oUZpkDNww?view_as=subscriber" target="_blank"><i class="fa fa-pinterest-p"></i></a>								
							</li>
							<li>
								<a href="https://www.youtube.com/channel/UCIaAoj_Be77FV6oUZpkDNww?view_as=subscriber" target="_blank"><i class="fa fa-instagram"></i></a>								
							</li>
						</ul>
					</div>
					<div class="descripcion">
						<a href="http://www.facebook.com/ArtBoomerangProgramaFederalParaLasArtes" target="_blank">www.facebook.com/ArtBoomerang<br>ProgramaFederalParaLasArtes</a>
						<span>@ArtBoomerang</span>
						<span>art.boomerang@gmail.com</span>
					</div>
					<hr>
				</div>
				<div class="item convocatoria">
					<hgroup>
						<h2>Convocatoria</h2>
					</hgroup>
					<div class="download">
						<div class="icon">
							<a href="#"><i class="glyphicon glyphicon-download"></i></a>
						</div>
						<div class="text">
							<span>DESCARGAR PDF</span>
							<p>Accede a  las bases y condiciones <br> para formar parte de Art Boomerang</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section id="map">
		<div class="container">
			<div class="row">
				<div class="col-lg-7">
					<div class="mapa">
						<h2>Los Territorios <br> del arte.</h2>
					</div>
				</div>
				<div class="col-lg-5">
					<div class="provincias">
						<h2>Provincias</h2>
						<ul>
							<li><i class="glyphicon glyphicon-map-marker"></i><b>.Chaco</b> Art Boomerang</li>
							<li><i class="glyphicon glyphicon-map-marker"></i><b>.Corrientes</b> Art Boomerang</li>
							<li><i class="glyphicon glyphicon-map-marker"></i><b>.Formosa</b> Art Boomerang</li>
							<li><i class="glyphicon glyphicon-map-marker"></i><b>.Córdoba</b> Art Boomerang</li>
							<li><i class="glyphicon glyphicon-map-marker"></i><b>.Misiones</b> Art Boomerang</li>
							<li><i class="glyphicon glyphicon-map-marker"></i><b>.Río Negro</b> Art Boomerang</li>
							<li><i class="glyphicon glyphicon-map-marker"></i><b>.Neuquén</b> Art Boomerang</li>
							<li><i class="glyphicon glyphicon-map-marker"></i><b>.Chubut</b> Art Boomerang</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</section>

	<?php include('templates/footer.php'); ?>