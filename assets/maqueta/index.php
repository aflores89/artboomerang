	<?php include('templates/head.php'); ?>

	<section id="over">
		<div class="transparencia"></div>
		<div class="slider">
			<ul>
				<li>
					<video id="video" width="100%" height="100%" autoplay>
						<source id="mp4" src="videos/MP4/" type="video/mp4">
						<source id="ogg" src="videos/OGG/" type="video/ogg">
						<source id="webm" src="videos/WEBM/" type="video/webm">
					</video>
				</li>
			</ul>
		</div>
		<div class="title">
			<hgroup class="text-center">
				<h1>LOS TERRITORIOS <br>DEL ARTE</h1>
				<span>art boomerang . programa federal para las artes</span>
			</hgroup>			
		</div>
		<div class="arrow">
			<i class="fa fa-arrow-circle-o-down"></i>
		</div>
	</section>

	<?php include('templates/menu.php'); ?>

	<section class="wrapp" id="content">
		<div class="container">
			<article class="col-lg-8">
				<div class="title">
					<h2>Art Boomerang</h2>
					<span>Programa Federal para Las Artes</span>
				</div>
				<div class="content">
					<p>Art Boomerang, es un programa que pretende cubrir las necesidades y desarrollo artístico e intelectual de artistas visuales de distintas disciplinas que vivan en Argentina. Primera etapa.</p>
					<p>También, integrar las diferentes miradas de nuestro país, por medio del intercambio artístico con otros territorios (Zona Norte, Zona Centro y Zona Sur) que participen del proyecto. Segunda Etapa.</p>
					<p>Art Boomerang intenta constituirse como una red y usina, capas de dar respuesta a las motivaciones personales que se presenten desde el trabajo artístico como así también contribuir al crecimiento profesional. <br> Dicho proyecto trabajará sobre el análisis crítico de las producciones de los artistas seleccionados, para posibilitar respuestas que surgen de diálogos diversos en donde los participantes intercambian sus miradas.</p>					
					<div class="bio">
						<div class="image">
							<figure>
								<img src="assets/images/daniel.jpg" alt="">
							</figure>
						</div>
						<div class="info">
							<h2><a href="#">Daniel Fischer</a></h2>
							<span>Curador / Coordinador</span>
							<p>Profesor y Curador independiente. Se a formado en Arquitectura y Artes Visuales. Es docente de la Facultad de Artes, Diseño y Ciencia de la Cultura, FADyC y de la Facultad de Arquitectura y Urbanismo de la U.N.N.E. Participa como investigador para la Sociedad de Estudios Morfológicos en Argentina SEMA y Grupo Argentino de Color GAC Ha obtenido becas de la Fundación Telefónica, Fondo Nacional de las Artes, Ciencia y Técnica, Oficina Cultural de la Embajada d</p>
						</div>
						<div class="clearfix"></div>
					</div>
					<!--<div class="curadores">
						<div class="col-lg-3">
							<div class="curador">
								<figure>
									<img src="assets/images/daniel.jpg" alt="" class="img-responsive">
								</figure>
								<div class="personal">
									<h2>Rodrigo Alonso</h2>
									<span class="detalle">Curador . Buenos Aires</span>
								</div>
							</div>
						</div>
						<div class="col-lg-3 col-lg-push-1">
							<div class="curador">
								<figure>
									<img src="assets/images/daniel.jpg" alt="" class="img-responsive">
								</figure>
								<div class="personal">
									<h2>Carina Cagnolo</h2>
									<span class="detalle">Curadora . Córdoba</span>
								</div>
							</div>
						</div>
						<div class="col-lg-3 col-lg-push-2">
							<div class="curador">
								<figure>
									<img src="assets/images/daniel.jpg" alt="" class="img-responsive">
								</figure>
								<div class="personal">
									<h2>Kekena Corvalan</h2>
									<span class="detalle">Curadora . Buenos Aires</span>
								</div>
							</div>
						</div>
					</div>		-->	
				</div>
			</article>
			<div class="col-lg-4 aside">				
				<div class="item seguinos">
					<hgroup>
						<h2>Seguinos</h2>
					</hgroup>
					<div class="social">
						<ul>
							<li>
								<a href="http://www.facebook.com/ArtBoomerangProgramaFederalParaLasArtes" target="_blank"><i class="fa fa-facebook"></i></a>
							</li>
							<li>
								<a href="https://twitter.com/art_boomerang" target="_blank"><i class="fa fa-twitter"></i></a>
							</li>
							<li>
								<a href="https://vimeo.com/artboomerang" target="_blank"><i class="fa fa-vimeo"></i></a>
							</li>						
							<li>
								<a href="https://www.youtube.com/channel/UCIaAoj_Be77FV6oUZpkDNww?view_as=subscriber" target="_blank"><i class="fa fa-youtube"></i></a>
							</li>
						</ul>
						<ul>
							<li>
								<a href="https://www.youtube.com/channel/UCIaAoj_Be77FV6oUZpkDNww?view_as=subscriber" target="_blank"><i class="fa fa-pinterest-p"></i></a>								
							</li>
							<li>
								<a href="https://www.youtube.com/channel/UCIaAoj_Be77FV6oUZpkDNww?view_as=subscriber" target="_blank"><i class="fa fa-instagram"></i></a>								
							</li>
						</ul>
					</div>
					<div class="descripcion">
						<a href="http://www.facebook.com/ArtBoomerangProgramaFederalParaLasArtes" target="_blank">www.facebook.com/ArtBoomerang<br>ProgramaFederalParaLasArtes</a>
						<span>@ArtBoomerang</span>
						<span>art.boomerang@gmail.com</span>
					</div>
					<hr>
				</div>
				<div class="item convocatoria">
					<hgroup>
						<h2>Convocatoria</h2>
					</hgroup>
					<div class="download">
						<div class="icon">
							<a href="#"><i class="glyphicon glyphicon-download"></i></a>
						</div>
						<div class="text">
							<span>DESCARGAR PDF</span>
							<p>Accede a  las bases y condiciones <br> para formar parte de Art Boomerang</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section id="map">
		<div class="container">
			<div class="row">
				<div class="col-lg-8">
					<div class="mapa">
						<h2>Los Territorios <br> del arte.</h2>
					</div>
				</div>
				<div class="col-lg-4">
					<div class="provincias">
						<h2>Provincias</h2>
						<ul>
							<li><i class="glyphicon glyphicon-map-marker"></i><b>.Chaco</b> Art Boomerang</li>
							<li><i class="glyphicon glyphicon-map-marker"></i><b>.Corrientes</b> Art Boomerang</li>
							<li><i class="glyphicon glyphicon-map-marker"></i><b>.Formosa</b> Art Boomerang</li>
							<li><i class="glyphicon glyphicon-map-marker"></i><b>.Córdoba</b> Art Boomerang</li>
							<li><i class="glyphicon glyphicon-map-marker"></i><b>.Misiones</b> Art Boomerang</li>
							<li><i class="glyphicon glyphicon-map-marker"></i><b>.Río Negro</b> Art Boomerang</li>
							<li><i class="glyphicon glyphicon-map-marker"></i><b>.Neuquén</b> Art Boomerang</li>
							<li><i class="glyphicon glyphicon-map-marker"></i><b>.Chubut</b> Art Boomerang</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</section>

	<?php include('templates/footer.php'); ?>