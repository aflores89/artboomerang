	<?php include('templates/head.php'); ?>

	<header>					
		<div class="container">
			<div class="row">
				<div class="col-lg-3">
					<div class="logo">
						<div class="franja"></div>	
						<img src="assets/images/logo.jpg" alt="" class="img-responsive">
					</div>
				</div>
				<div class="col-lg-6">
					<nav class="nav">
						<ul class="navbar-nav nav">
							<li><a href="#">ART BOOMERANG</a></li>						
							<li><a href="#">ARTISTAS</a></li>
							<li><a href="#">VIDEOS</a></li>
							<li><a href="#">NOTICIAS</a></li>
							<li><a href="#">CONTACTO</a></li>
						</ul>
					</nav>
				</div>
			</div>
		</div>
	</header>
	<section id="biografia">
		<div class="container">			
				<div class="col-lg-8">
					<div class="bio">
						<div class="row">
							<div class="col-lg-12 no-padding title text-left">
								<span>Artistas Córdoba Art Boomerang</span>
								<h2>Sofia Torres Kosiba</h2>
							</div>
						</div>
						<div class="row">
							
							<div class="col-lg-12 perfil texto">
								<h3>.Biografía</h3>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-4 padding">
					<img src="assets/images/daniel.jpg" class="img-responsive" alt="">
				</div>


			<!--<div class="row">
				<div class="col-lg-4 title text-left">
					<span>Artistas Córdoba Art Boomerang</span>
					<h2>Sofia Torres Kosiba</h2>
				</div>
			</div>
			<div class="row">
				
				<div class="col-lg-3 perfil imagen text-left">
					<figure>
						<img src="assets/images/daniel.jpg" class="img-responsive" alt="">
					</figure>
				</div>
				<div class="col-lg-8 perfil texto">
					<h3>.Biografía</h3>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
				</div>
			</div>-->
		</div>
	</section>
	<section class="wrapp" id="artista">
		<div class="container">					
			<article class="col-lg-8">			
				<h3>.Galeria</h3>
				<div class="galeria">
					<div id="owl-demo" class="owl-carousel owl-theme">
 
					  	<div class="item"><img src="assets/images/imagen.jpg" alt="The Last of us" class="img-responsive"></div>
					  	<div class="item"><img src="assets/images/imagen.jpg" alt="GTA V" class="img-responsive"></div>
					  	<div class="item"><img src="assets/images/imagen.jpg" alt="Mirror Edge" class="img-responsive"></div>
					 
					</div>
				</div>		
				<div class="descripcion">
					<h3>.Statement</h3>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus volutpat diam magna, at auctor magna accumsan eget. Donec faucibus ipsum nec lorem fermentum, vitae fermentum metus varius. Sed congue lectus in porttitor bibendum. Donec tempor posuere pellentesque. Vivamus eu varius elit. Ut ac fermentum quam. Ut ornare cursus tortor a mollis.</p>
				</div>	
				<div class="links">
					<h3>.Links</h3>
					<ul>
						<li><a href="#">www.google.com.ar</a></li>
					</ul>
				</div>	
			</article>
			<div class="col-lg-4 aside">
				<div class="item artistas text-left">
					<hgroup>
						<h2>Artistas</h2>
					</hgroup>
					<h3><b>.Córdoba</b> Art Boomerang</h3>
					<ul>
						<li><a href="#">Christian Román</a></li>
						<li><a href="#">Christian Román</a></li>
						<li><a href="#">Christian Román</a></li>
						<li><a href="#">Christian Román</a></li>
						<li><a href="#">Christian Román</a></li>
						<li><a href="#">Christian Román</a></li>
						<li><a href="#">Christian Román</a></li>
						<li><a href="#">Christian Román</a></li>
						<li><a href="#">Christian Román</a></li>
						<li><a href="#">Christian Román</a></li>
						<li><a href="#">Christian Román</a></li>
						<li><a href="#">Christian Román</a></li>
						<li><a href="#">Christian Román</a></li>
						<li><a href="#">Christian Román</a></li>
						<li><a href="#">Christian Román</a></li>
					</ul>
					<hr>
				</div>
				<div class="item convocatoria seguinos">
					<hgroup>
						<h2>Seguinos</h2>
					</hgroup>
					<div class="social">
						<ul>
							<li>
								<a href="http://www.facebook.com/ArtBoomerangProgramaFederalParaLasArtes" target="_blank"><i class="fa fa-facebook"></i></a>
							</li>
							<li>
								<a href="https://twitter.com/art_boomerang" target="_blank"><i class="fa fa-twitter"></i></a>
							</li>
							<li>
								<a href="https://vimeo.com/artboomerang" target="_blank"><i class="fa fa-vimeo"></i></a>
							</li>						
							<li>
								<a href="https://www.youtube.com/channel/UCIaAoj_Be77FV6oUZpkDNww?view_as=subscriber" target="_blank"><i class="fa fa-youtube"></i></a>
							</li>
						</ul>
						<ul>
							<li>
								<a href="https://www.youtube.com/channel/UCIaAoj_Be77FV6oUZpkDNww?view_as=subscriber" target="_blank"><i class="fa fa-pinterest-p"></i></a>								
							</li>
							<li>
								<a href="https://www.youtube.com/channel/UCIaAoj_Be77FV6oUZpkDNww?view_as=subscriber" target="_blank"><i class="fa fa-instagram"></i></a>								
							</li>
						</ul>
					</div>
					<div class="descripcion">
						<a href="http://www.facebook.com/ArtBoomerangProgramaFederalParaLasArtes" target="_blank">www.facebook.com/ArtBoomerang<br>ProgramaFederalParaLasArtes</a>
						<span>@ArtBoomerang</span>
						<span>art.boomerang@gmail.com</span>
					</div>
					<hr>
				</div>
			</aside>
		</div>
	</section>

	<section id="map">
		<div class="container">
			<div class="row">
				<div class="col-lg-7">
					<div class="mapa">
						<h2>Los Territorios <br> del arte.</h2>
					</div>
				</div>
				<div class="col-lg-5">
					<div class="provincias">
						<h2>Provincias</h2>
						<ul>
							<li><i class="glyphicon glyphicon-map-marker"></i><b>.Chaco</b> Art Boomerang</li>
							<li><i class="glyphicon glyphicon-map-marker"></i><b>.Corrientes</b> Art Boomerang</li>
							<li><i class="glyphicon glyphicon-map-marker"></i><b>.Formosa</b> Art Boomerang</li>
							<li><i class="glyphicon glyphicon-map-marker"></i><b>.Córdoba</b> Art Boomerang</li>
							<li><i class="glyphicon glyphicon-map-marker"></i><b>.Misiones</b> Art Boomerang</li>
							<li><i class="glyphicon glyphicon-map-marker"></i><b>.Río Negro</b> Art Boomerang</li>
							<li><i class="glyphicon glyphicon-map-marker"></i><b>.Neuquén</b> Art Boomerang</li>
							<li><i class="glyphicon glyphicon-map-marker"></i><b>.Chubut</b> Art Boomerang</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</section>

	<?php include('templates/footer.php'); ?>