<footer>
		<div class="container">
			<div class="row">
				<div class="col-lg-4 nopadding">
					<div class="item bg1">
						<figure>
							<img src="assets/images/icono1.png" class="img-responsive" alt="">
						</figure>
						<h3><a href="#">Art Boomerang</a></h3>
						<p>artboomerang art boomerang art<br>
						boomerang descripción</p>
					</div>
				</div>
				<div class="col-lg-4 nopadding">
					<div class="item bg2">
						<figure>
							<img src="assets/images/icono2.png" class="img-responsive" alt="">
						</figure>
						<h3><a href="#">Artistas</a></h3>
						<p>artboomerang art boomerang art<br>
						boomerang descripción</p>
					</div>
				</div>
				<div class="col-lg-4 nopadding">
					<div class="item bg3">
						<figure>
							<img src="assets/images/icono3.png" class="img-responsive" alt="">
						</figure>
						<h3><a href="#">Videos</a></h3>
						<p>artboomerang art boomerang art<br>
						boomerang descripción</p>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-4 nopadding">
					<div class="item bg4">
						<figure>
							<img src="assets/images/icono4.png" class="img-responsive" alt="">
						</figure>
						<h3><a href="#">Convocatorias</a></h3>
						<p>artboomerang art boomerang art<br>
						boomerang descripción</p>
					</div>
				</div>
				<div class="col-lg-4 nopadding">
					<div class="item bg5">
						<figure>
							<img src="assets/images/icono5.png" class="img-responsive" alt="">
						</figure>
						<h3><a href="#">Noticias</a></h3>
						<p>artboomerang art boomerang art<br>
						boomerang descripción</p>
					</div>
				</div>
				<div class="col-lg-4 nopadding">
					<div class="item bg6">
						<figure>
							<img src="assets/images/icono6.png" class="img-responsive" alt="">
						</figure>
						<h3><a href="#">Contacto</a></h3>
						<p>artboomerang art boomerang art<br>
						boomerang descripción</p>
					</div>
				</div>
			</div>
		</div>
	</footer>

	<script type="text/javascript" src="node_modules/jquery/dist/jquery.min.js"></script>
	<script type="text/javascript" src="node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="node_modules/owl-slider/owl.carousel.js"></script>
	<script type="text/javascript" src="assets/js/main.js"></script>

</body>
</html>