$(function() {
	var i = 1;	

	var height = $('aside').parent().parent('section').height();
	$('aside').height(height);

	$( window ).scroll(function() {
		var top = $(document).scrollTop();
		var height = $(this).height();

		$('#title').css({
			opacity: 1-(top/height)*2
		})
		$('#arrow').css({
			opacity: 1-(top/height)*2
		})
	});

	adjustHeight();
	owlSlider();
	Video();	

});

var owlSlider = function() {
	$("#owl-demo").owlCarousel({
 
		navigation : true, // Show next and prev buttons
		slideSpeed : 300,
		paginationSpeed : 400,
		singleItem:true
	 
	  });
}

var adjustHeight = function() {

	var contentHeight = $('.wrapp').height();
	console.log(contentHeight);
	$('.convocatoria').height(contentHeight-481);
}

var Video = function() {
	var videos = ['loop01_slider_boomerang', 'loop02_slider_boomerang', 'loop03_slider_boomerang', 'loop04_slider_boomerang'];
	var $video = $('#video');
	var i = 0;

	var MP4 = $('#video').children('#mp4').attr('src');
	var OGG = $('#video').children('#ogg').attr('src');
	var WEBM = $('#video').children('#webm').attr('src');

	var addVideo = function(key) {

		$('#mp4').attr('src', MP4+videos[key]+'.mp4');
		$('#ogg').attr('src', OGG+videos[key]+'.ogv');
		$('#webm').attr('src', WEBM+videos[key]+'.webm');		

	}	
	addVideo(0);
	document.getElementById('video').addEventListener('ended',myHandler,false);
    function myHandler(e) {
    	var myVideo = document.getElementsByTagName('video')[0];
    	i = i+1;
    	if (i == 4) {
    		i=0;
    	}
    	
		addVideo(i);
		myVideo.load();
		myVideo.play();
		
     	//$('#video').get(0).play();
    }

}