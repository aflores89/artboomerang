<?php

Class Artistas_model extends CI_Model {

	public function getArtistas() {
		$result = $this->db->get('artistas');
		return $result->result_array();
	}

	public function getArtistaById($id) {
		$this->db->where('idartistas', $id);
		$result = $this->db->get('artistas');
		return $result->result_array();
	}

	public function getArtistasByProvincia($idprovincia) {
		$this->db->where('idprovincia', $idprovincia);
		$result = $this->db->get('artistas');
		return $result->result_array();
	}

	public function getProvinciaById($id) {
		$this->db->where('idprovincias', $id);
		$result = $this->db->get('provincias');
		return $result->result_array();
	}

	public function getImagenesById($id) {
		$this->db->where('idartistas', $id);
		$result = $this->db->get('imagenes');
		return $result->result_array();
	}
	
}