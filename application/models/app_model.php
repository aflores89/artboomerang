<?php

Class App_model extends CI_Model {

	public function getContenido() {
        $this->db->where('idcontenidos', 1);  
        $this->db->limit(1);  
        $query = $this->db->get('contenidos');
        $result = $query->result_array();

        if(count($result) == 0)
            return false;

        return [];
    }

	public function getConvocatoria() {
		$this->db->where('activa', 1);
		$this->db->limit(1);
		$result = $this->db->get('convocatorias');
	 	if(!empty($result->result_array())) {
            return $result->result_array();
        } else {
            return false;
        }		
	}

	public function getNoticiaById($id) {
		$this->db->where('idnoticias', $id);
		$this->db->limit(1);
		$result = $this->db->get('noticias');
	 	if(!empty($result->result_array())) {
            return $result->result_array();
        } else {
            return false;
        }		
	}

	public function getCurador($id) {
		$this->db->where('idcuradores', $id);
		$this->db->limit(1);
		$result = $this->db->get('curadores');
	 	if(!empty($result->result_array())) {
            return $result->result_array();
        } else {
            return false;
        }		
	}

	public function getVideos() {
		$result = $this->db->get('videos');
	 	if(!empty($result->result_array())) {
            return $result->result_array();
        } else {
            return false;
        }		
	}

	public function getNoticias() {
		$result = $this->db->get('noticias');
	 	if(!empty($result->result_array())) {
            return $result->result_array();
        } else {
            return false;
        }		
	}

	public function getVideoDestacado() {
		$this->db->where('destacado', 1);
		$this->db->limit(1);
		$result = $this->db->get('videos');
	 	if(!empty($result->result_array())) {
            return $result->result_array();
        } else {
            return false;
        }		
	}

	public function getCuradoresInvitados() {
        $this->db->order_by('orden', 'desc');
		$result = $this->db->get('curadores');
	 	if(!empty($result->result_array())) {
            return $result->result_array();
        } else {
            return false;
        }		
	}

	public function getProvinciaById($id)
    {
        $this->db->where('idprovincias', $id);
        $result = $this->db->get('provincias');
        if(!empty($result->result_array())) {
            return $result->result_array();
        } else {
            return false;
        }
    }
	
}