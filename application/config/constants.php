<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ',							'rb');
define('FOPEN_READ_WRITE',						'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE',		'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE',	'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE',					'ab');
define('FOPEN_READ_WRITE_CREATE',				'a+b');
define('FOPEN_WRITE_CREATE_STRICT',				'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',		'x+b');


/* provincias */

define('BUENOS_AIRES', 1);
define('CATAMARCA', 2);
define('CHACO', 3);
define('CHUBUT', 4);
define('CORDOBA', 5);
define('CORRIENTES', 6);
define('ENTRE_RIOS', 7);
define('FORMOSA', 8);
define('JUJUY', 9);
define('LA_PAMPA', 10);
define('LA_RIOJA', 11);
define('MENDOZA', 12);
define('MISIONES', 13);
define('NEUQUEN', 14);
define('RIO_NEGRO', 15);
define('SALTA', 16);
define('SAN_JUAN', 17);
define('SAN_LUIS', 18);
define('SANTA_CRUZ', 19);
define('SANTA_FE', 20);
define('SANTIAGO', 21);
define('TDF', 22);
define('TUCUMAN', 23);


/* End of file constants.php */
/* Location: ./application/config/constants.php */