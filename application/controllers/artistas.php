<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Artistas extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{

		$this->load->model('artistas_model');


		$data['buenos_aires'] = $this->artistas_model->getArtistasByProvincia(BUENOS_AIRES);
		$data['catamarca'] = $this->artistas_model->getArtistasByProvincia(CATAMARCA);
		$data['chaco'] = $this->artistas_model->getArtistasByProvincia(CHACO);
		$data['chubut'] = $this->artistas_model->getArtistasByProvincia(CHUBUT);
		$data['cordoba'] = $this->artistas_model->getArtistasByProvincia(CORDOBA);
		$data['corrientes'] = $this->artistas_model->getArtistasByProvincia(CORRIENTES);
		$data['entre_rios'] = $this->artistas_model->getArtistasByProvincia(ENTRE_RIOS);
		$data['formosa'] = $this->artistas_model->getArtistasByProvincia(FORMOSA);
		$data['jujuy'] = $this->artistas_model->getArtistasByProvincia(JUJUY);
		$data['la_pampa'] = $this->artistas_model->getArtistasByProvincia(LA_PAMPA);
		$data['la_rioja'] = $this->artistas_model->getArtistasByProvincia(LA_RIOJA);
		$data['mendoza'] = $this->artistas_model->getArtistasByProvincia(MENDOZA);
		$data['misiones'] = $this->artistas_model->getArtistasByProvincia(MISIONES);
		$data['neuquen'] = $this->artistas_model->getArtistasByProvincia(NEUQUEN);
		$data['rio_negro'] = $this->artistas_model->getArtistasByProvincia(RIO_NEGRO);
		$data['salta'] = $this->artistas_model->getArtistasByProvincia(SALTA);
		$data['san_juan'] = $this->artistas_model->getArtistasByProvincia(SAN_JUAN);
		$data['san_luis'] = $this->artistas_model->getArtistasByProvincia(SAN_LUIS);
		$data['santa_cruz'] = $this->artistas_model->getArtistasByProvincia(SANTA_CRUZ);
		$data['santa_fe'] = $this->artistas_model->getArtistasByProvincia(SANTA_FE);
		$data['santiago'] = $this->artistas_model->getArtistasByProvincia(SANTIAGO);
		$data['tdf'] = $this->artistas_model->getArtistasByProvincia(TDF);
		$data['tucuman'] = $this->artistas_model->getArtistasByProvincia(TUCUMAN);

		$this->load->helper('url');
		$data['url'] = base_url();
		$data['convocatoria'] = $this->convocatoriaActiva();
		$data['active'] = 'artistas';
		$this->load->view('templates/head', $data);
		$this->load->view('artistas', $data);
		$this->load->view('templates/footer', $data);
	}

	public function perfil($id) {
		$this->load->model('artistas_model');
		$result = $this->artistas_model->getArtistaById($id);

		$data['url'] = base_url();
		$data['artista'] = $result[0];		
		$data['provincia'] = $this->artistas_model->getProvinciaById($result[0]['idprovincia']);		
		$data['artistas'] = $this->artistas_model->getArtistasByProvincia($result[0]['idprovincia']);
		$data['galeria'] = $this->artistas_model->getImagenesById($id);
		$data['convocatoria'] = $this->convocatoriaActiva();
		$data['url'] = base_url();
		
		$data['active'] = 'artistas';
		$this->load->view('templates/head', $data);
		$this->load->view('biografia', $data);
		$this->load->view('templates/footer', $data);
	}

	public function convocatoriaActiva() {

		$this->load->model('app_model');
		$result = $this->app_model->getConvocatoria();
		return $result;

	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */