<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Videos extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->model('app_model');
		$this->load->helper('url');
		$data['url'] = base_url();
		$data['convocatoria'] = $this->convocatoriaActiva();
		$data['active'] = 'videos';
		$videos = $this->app_model->getVideos();
		$destacado = $this->app_model->getVideoDestacado();		

        if(!is_null($destacado[0]['link'])) {
		    $jsonDest = file_get_contents('https://vimeo.com/api/oembed.json?url='.$destacado[0]['link']);
            $destacado['vimeo'] = json_decode($jsonDest);
            $data['destacado'] = $destacado;
        }

        if(!empty($videos)) {
            array_walk($videos, function(&$value, $key) {
                //die(var_dump($value));
                $json = file_get_contents('https://vimeo.com/api/oembed.json?url='.$value['link']);
                $obj = json_decode($json);
                $value['vimeo'] = $obj;
            });
        }

		$data['videos'] = $videos;

		$this->load->view('templates/head', $data);
		$this->load->view('videos', $data);
		$this->load->view('templates/footer', $data);
	}

	public function convocatoriaActiva() {

		$this->load->model('app_model');
		$result = $this->app_model->getConvocatoria();
		return $result;

	}

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */