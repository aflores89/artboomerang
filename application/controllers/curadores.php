<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Curadores extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index($id = 1)
	{
		$this->load->helper('url');
		$this->load->model('app_model');
		$data['url'] = base_url();
		$data['convocatoria'] = $this->convocatoriaActiva();
		$data['curador'] = $this->app_model->getCurador($id);
		$data['active'] = 'arte';
		$this->load->view('templates/head', $data);
		$this->load->view('curadores', $data);
		$this->load->view('templates/footer', $data);
	}

	public function perfil($id) {
		$this->load->model('artistas_model');
		$result = $this->artistas_model->getArtistaById($id);

		$data['url'] = base_url();
		$data['artista'] = $result[0];		
		$data['provincia'] = $this->artistas_model->getProvinciaById($result[0]['idprovincia']);		
		$data['artistas'] = $this->artistas_model->getArtistasByProvincia($result[0]['idprovincia']);
		$data['galeria'] = $this->artistas_model->getImagenesById($id);
		$data['convocatoria'] = $this->convocatoriaActiva();
		$data['url'] = base_url();
		$data['active'] = 'arte';
		
		$this->load->view('templates/head', $data);
		$this->load->view('biografia', $data);
		$this->load->view('templates/footer', $data);
	}

	public function getCuradoresInvitados() {
		
		$this->load->model('app_model');
		$result = $this->app_model->getCuradoresInvitados();
		return $result;

	}

	public function convocatoriaActiva() {

		$this->load->model('app_model');
		$result = $this->app_model->getConvocatoria();
		return $result;

	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */