<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Inicio extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->model('app_model');
		$this->load->helper('url');
		$data['url'] = base_url();
		$data['convocatoria'] = $this->convocatoriaActiva();
		$data['contenido'] = $this->app_model->getContenido();
		$data['active'] = 'home';

		$this->load->view('templates/head', $data);
		$this->load->view('index', $data);
		$this->load->view('templates/footer', $data);
	}

	public function convocatoriaActiva() {

		$this->load->model('app_model');
		$result = $this->app_model->getConvocatoria();
		return $result;

	}

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */