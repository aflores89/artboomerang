<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Arte extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->helper('url');
		$data['url'] = base_url();
		$data['convocatoria'] = $this->convocatoriaActiva();
		$data['curadores'] = $this->getCuradoresInvitados();
		$data['active'] = 'arte';
		$this->load->view('templates/head', $data);
		$this->load->view('artboomerang', $data);
		$this->load->view('templates/footer', $data);
	}

	public function getCuradoresInvitados() {
		
		$this->load->model('app_model');
		$result = $this->app_model->getCuradoresInvitados();
		return $result;

	}

	public function convocatoriaActiva() {

		$this->load->model('app_model');
		$result = $this->app_model->getConvocatoria();
		return $result;

	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */