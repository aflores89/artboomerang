<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Noticias extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->helper('url');
		$data['url'] = base_url();
		$data['convocatoria'] = $this->convocatoriaActiva();
		$data['noticias'] = $this->app_model->getNoticias();
		$data['active'] = 'noticias';
		$this->load->view('templates/head', $data);
		$this->load->view('noticias', $data);
		$this->load->view('templates/footer', $data);
	}

	public function convocatoriaActiva() {

		$this->load->model('app_model');
		$result = $this->app_model->getConvocatoria();
		return $result;

	}

	public function get($id) {

		$this->load->helper('url');
		$data['url'] = base_url();
		$data['convocatoria'] = $this->convocatoriaActiva();
		$data['noticia'] = $this->app_model->getNoticiaById($id);
		$data['active'] = 'noticias';
		
		$this->load->view('templates/head', $data);
		$this->load->view('interna', $data);
		$this->load->view('templates/footer', $data);
	}

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */