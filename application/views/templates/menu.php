<header>					
	<div class="container">
		<div class="row">
			<div class="col-lg-3">
				<div class="logo">
					<div class="franja"></div>	
					<a href="<?=$url?>"><img src="<?=$url?>assets/images/logo.jpg" alt="" class="img-responsive"></a>
				</div>
			</div>

			<div class="col-lg-6">
				<nav class="nav">
					<ul class="navbar-nav nav">
						<li><a href="<?=$url?>arte">ART BOOMERANG</a></li>						
						<li><a href="<?=$url?>artistas">ARTISTAS</a></li>
						<li><a href="<?=$url?>videos">VIDEOS</a></li>
						<li><a href="<?=$url?>noticias">NOTICIAS</a></li>
						<li><a href="<?=$url?>contacto">CONTACTO</a></li>
					</ul>
				</nav>
			</div>
		</div>
	</div>
</header>