<header class="interna" style="background-color: white;">					
	<div class="container">
		<div class="row">		
			<div class="col-lg-8">
				<nav class="nav">
					<ul class="navbar-nav nav">						
						<li id="home">
							<a href="<?=$url?>">
								<img src="<?=$url?>assets/images/home_icono.png" alt="">
								<span>HOME</span>
							</a>
						</li>						
						<li id="referentes">
							<a class="<?php echo ($active == 'arte') ? 'active' : ''; ?>" href="<?=$url?>arte"><img src="<?=$url?>assets/images/referentes_icon.png" alt="" class="img-responsive">
								<span>REFERENTES</span>
							</a>
						</li>						
						<li id="artistas">
							<a class="<?php echo ($active == 'artistas') ? 'active' : ''; ?>" href="<?=$url?>artistas"><img src="<?=$url?>assets/images/artistas_icon.png" alt="" class="img-responsive">
								<span>ARTISTAS</span>
							</a>
						</li>
						<li id="videos">
							<a class="<?php echo ($active == 'videos') ? 'active' : ''; ?>" href="<?=$url?>videos"><img src="<?=$url?>assets/images/videos_icon.png" alt="" class="img-responsive">
								<span>VIDEOS</span>
							</a>
						</li>
						<li id="convocatoria">
							<a href="#" data-toggle="modal" data-target="#modalConvocatoria">
								<img src="<?=$url?>assets/images/convocatoria_icon.png" alt="" class="img-responsive">
								<span>CONVOCATORIA</span>
							</a>
						</li>
						<li id="noticias">
							<a class="<?php echo ($active == 'noticias') ? 'active' : ''; ?>" href="<?=$url?>noticias"><img src="<?=$url?>assets/images/noticias_icon.png" alt="" class="img-responsive">
								<span>NOTICIAS</span>
							</a>
						</li>
						<li id="contacto">
							<a class="<?php echo ($active == 'contacto') ? 'active' : ''; ?>" href="<?=$url?>contacto"><img src="<?=$url?>assets/images/contacto_icon.png" alt="" class="img-responsive">
								<span>CONTACTO</span>
							</a>
						</li>
					</ul>
				</nav>
			</div>
		</div>
	</div>
</header>