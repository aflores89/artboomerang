<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<title>Art Boomerang</title>

	<link rel="icon" href="<?=$url?>assets/images/favicon.png" type="image/png" />
	<link rel="shortcut icon" href="<?=$url?>assets/images/favicon.ico" />

	<link rel="stylesheet" href="<?=$url?>assets/node_modules/bootstrap/dist/css/bootstrap.css">	
	<link rel="stylesheet" href="<?=$url?>assets/node_modules/font-awesome/css/font-awesome.css.map">	
	<link rel="stylesheet" href="<?=$url?>assets/node_modules/font-awesome/css/font-awesome.css">		
	<link rel="stylesheet" href="<?=$url?>assets/node_modules/owl-slider/owl.carousel.css">
	<link rel="stylesheet" href="<?=$url?>assets/node_modules/mediaelement/build/mediaelementplayer.css">
	<link rel="stylesheet" href="<?=$url?>assets/stylesheets/youtube-video-gallery.css">	
	<link rel="stylesheet" href="<?=$url?>assets/stylesheets/screen.css">	
</head>
<body>
	