<footer>
		<div class="container">
			<div class="row">
				<div class="col-lg-4 nopadding">
					<a href="<?=$url.'arte'?>">
						<div class="item bg1">
							<figure>
								<img src="<?=$url?>assets/images/icono1.png" class="img-responsive" alt="">
							</figure>
							<h3>Referentes</h3>
							<p>¿Que es el Art Boomerang?<br>
							Daniel Fischer, Curadores, Equipo.</p>
						</div>
					</a>
				</div>
				<div class="col-lg-4 nopadding">
					<a href="<?=$url.'artistas'?>">
						<div class="item bg2">
							<figure>
								<img src="<?=$url?>assets/images/icono2.png" class="img-responsive" alt="">
							</figure>
							<h3>Artistas</h3>
							<p>Conocé el perfil de los Artistas<br>
							que han participado.</p>
						</div>
					</a>
				</div>
				<div class="col-lg-4 nopadding">
					<a href="<?=$url.'videos'?>">
						<div class="item bg3">
							<figure>
								<img src="<?=$url?>assets/images/icono3.png" class="img-responsive" alt="">
							</figure>
							<h3>Videos</h3>
							<p>Registro, Entrevistas a los<br>
							Artistas y Curadores</p>
						</div>
					</a>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-4 nopadding">
					<?php if(!$convocatoria) : ?>
						<a href="#" data-toggle="modal" data-target="#modalConvocatoria">
					<?php else : ?>
						<a href="<?=$url?>admin/<?=$convocatoria[0]['path']?>" target="_blank">
					<?php endif; ?>
						<div class="item bg4">
							<figure>
								<img src="<?=$url?>assets/images/icono4.png" class="img-responsive" alt="">
							</figure>
							<h3>Convocatorias</h3>
							<p>Descargá  las bases para <br>
							 la próxima convocatoria Art Boomerang</p>
						</div>
					</a>	
				</div>
				<div class="col-lg-4 nopadding">
					<a href="<?=$url.'videos'?>">					
						<div class="item bg5">
							<figure>
								<img src="<?=$url?>assets/images/icono5.png" class="img-responsive" alt="">
							</figure>
							<h3>Noticias</h3>
							<p>Últimas Noticias relacionadas con<br>
							Eventos, Artistas, Convocatorias y más</p>
						</div>
					</a>
				</div>
				<div class="col-lg-4 nopadding">
					<a href="<?=$url.'contacto'?>">					
						<div class="item bg6">
							<figure>
								<img src="<?=$url?>assets/images/icono6.png" class="img-responsive" alt="">
							</figure>
							<h3>Contacto</h3>
							<p>Enterate como contactarnos o<br>
							escribirnos por cualquier consulta</p>
						</div>
					</a>
				</div>
			</div>
		</div>
	</footer>
	
	<div class="modal fade" tabindex="-1" role="dialog" id="modalConvocatoria">
	  	<div class="modal-dialog">
	    	<div class="modal-content">
	      		<div class="modal-header">
	        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	      		</div>
	      		<div class="modal-body">
	        		<p>No existe ninguna convocatoria activa.</p>
	      		</div>
	      		<div class="modal-footer">
	        		<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
	      		</div>
	    	</div><!-- /.modal-content -->
	  	</div><!-- /.modal-dialog -->
	</div><!-- /.modal -->

	<script type="text/javascript" src="<?=$url?>assets/node_modules/jquery/dist/jquery.min.js"></script>
	<script type="text/javascript" src="<?=$url?>assets/node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?=$url?>assets/node_modules/owl-slider/owl.carousel.js"></script>
	<script type="text/javascript" src="<?=$url?>assets/js/jquery.youtubevideogallery.js"></script>	
	<script type="text/javascript" src="<?=$url?>assets/node_modules/mediaelement/build/mediaelement-and-player.min.js"></script>	
	<script type="text/javascript" src="<?=$url?>assets/js/fitvids.js"></script>	
	<script type="text/javascript" src="<?=$url?>assets/js/main.js"></script>

</body>
</html>