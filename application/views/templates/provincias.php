<section id="map">
		<a href="#" id="ampliar"><i class="fa fa-arrow-circle-o-down"></i></a>
		<div class="container">
			<div class="row">
				<div class="col-lg-8">
					<div class="mapa">
						<h2>Los Territorios <br> del arte.</h2>
					</div>
				</div>
				<div class="col-lg-4">
					<div class="provincias">
						<h2>Provincias</h2>
						<ul>
							<li><a href="<?=$url?>provincia/<?=CHACO?>"><i class="fa fa-map-marker"></i><b>.Chaco</b> Art Boomerang</a></li>
							<li><a href="<?=$url?>provincia/<?=CORRIENTES?>"><i class="fa fa-map-marker"></i><b>.Corrientes</b> Art Boomerang</a></li>
							<li><a href="<?=$url?>provincia/<?=FORMOSA?>"><i class="fa fa-map-marker"></i><b>.Formosa</b> Art Boomerang</a></li>
							<li><a href="<?=$url?>provincia/<?=CORDOBA?>"><i class="fa fa-map-marker"></i><b>.Córdoba</b> Art Boomerang</a></li>
							<li><a href="<?=$url?>provincia/<?=MISIONES?>"><i class="fa fa-map-marker"></i><b>.Misiones</b> Art Boomerang</a></li>
							<li><a href="<?=$url?>provincia/<?=RIO_NEGRO?>"><i class="fa fa-map-marker"></i><b>.Río Negro</b> Art Boomerang</a></li>
							<li><a href="<?=$url?>provincia/<?=NEUQUEN?>"><i class="fa fa-map-marker"></i><b>.Neuquén</b> Art Boomerang</a></li>
							<li><a href="<?=$url?>provincia/<?=CHUBUT?>"><i class="fa fa-map-marker"></i><b>.Chubut</b> Art Boomerang</a></li>
							<li><i class="fa fa-map-marker"></i><b>.Buenos Aires</b> Art Boomerang</li>
							<li><i class="fa fa-map-marker"></i><b>.Catamarca</b> Art Boomerang</li>
							<li><i class="fa fa-map-marker"></i><b>.Entre Rios</b> Art Boomerang</li>
							<li><i class="fa fa-map-marker"></i><b>.Jujuy</b> Art Boomerang</li>
							<li><i class="fa fa-map-marker"></i><b>.La Pampa</b> Art Boomerang</li>
							<li><i class="fa fa-map-marker"></i><b>.La Rioja</b> Art Boomerang</li>
							<li><i class="fa fa-map-marker"></i><b>.Mendoza</b> Art Boomerang</li>
							<li><i class="fa fa-map-marker"></i><b>.Salta</b> Art Boomerang</li>
							<li><i class="fa fa-map-marker"></i><b>.San Juan</b> Art Boomerang</li>
							<li><i class="fa fa-map-marker"></i><b>.San Luis</b> Art Boomerang</li>
							<li><i class="fa fa-map-marker"></i><b>.Santa Cruz</b> Art Boomerang</li>
							<li><i class="fa fa-map-marker"></i><b>.Santa Fe</b> Art Boomerang</li>
							<li><i class="fa fa-map-marker"></i><b>.Santiago del Estero</b> Art Boomerang</li>
							<li><i class="fa fa-map-marker"></i><b>.Tierra del Fuego</b> Art Boomerang</li>
							<li><i class="fa fa-map-marker"></i><b>.Tucuman</b> Art Boomerang</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</section>