	<?php include('templates/menu_interna.php'); ?>

	<section id="artistas" class="wrapp">
		<div class="container">
			<article class="col-lg-8">
				<div class="title">
					<h2>Art Boomerang</h2>
					<span>Artistas.</span>
				</div>				
				
				<?php if($buenos_aires) : ?>				
					<div class="row">
						<div class="col-lg-12 ">
							<h3 class="title color1"><b>.Buenos Aires</b> Art Boomerang</h3>
						</div>
					</div>
					<div class="row">
						<?php foreach ($buenos_aires as $key => $value) : ?>
							<div class="col-lg-4">
								<ul>
									<li><a href="<?=$url.'artistas/perfil/'.$value['idartistas']?>"><?=$value['nombre']?></a></li>
								</ul>
							</div>			
						<?php endforeach; ?>								
					</div>					
				<?php endif;?>	

				<?php if($catamarca) : ?>				
					<div class="row">
						<div class="col-lg-12 ">
							<h3 class="title color2"><b>.Catamarca</b> Art Boomerang</h3>
						</div>
					</div>
					<div class="row">
						<?php foreach ($catamarca as $key => $value) : ?>
							<div class="col-lg-4">
								<ul>
									<li><a href="<?=$url.'artistas/perfil/'.$value['idartistas']?>"><?=$value['nombre']?></a></li>
								</ul>
							</div>			
						<?php endforeach; ?>								
					</div>					
				<?php endif;?>

				<?php if($chaco) : ?>				
					<div class="row">
						<div class="col-lg-12 ">
							<h3 class="title color3"><b>.Chaco</b> Art Boomerang</h3>
						</div>
					</div>
					<div class="row">
						<?php foreach ($chaco as $key => $value) : ?>
							<div class="col-lg-4">
								<ul>
									<li><a href="<?=$url.'artistas/perfil/'.$value['idartistas']?>"><?=$value['nombre']?></a></li>
								</ul>
							</div>			
						<?php endforeach; ?>								
					</div>					
				<?php endif;?>

				<?php if($chubut) : ?>				
					<div class="row">
						<div class="col-lg-12 ">
							<h3 class="title color4"><b>.Chubut</b> Art Boomerang</h3>
						</div>
					</div>
					<div class="row">
						<?php foreach ($chubut as $key => $value) : ?>
							<div class="col-lg-4">
								<ul>
									<li><a href="<?=$url.'artistas/perfil/'.$value['idartistas']?>"><?=$value['nombre']?></a></li>
								</ul>
							</div>			
						<?php endforeach; ?>								
					</div>					
				<?php endif;?>

				<?php if($cordoba) : ?>				
					<div class="row">
						<div class="col-lg-12 ">
							<h3 class="title color5"><b>.Córdoba</b> Art Boomerang</h3>
						</div>
					</div>
					<div class="row">
						<?php foreach ($cordoba as $key => $value) : ?>
							<div class="col-lg-4">
								<ul>
									<li><a href="<?=$url.'artistas/perfil/'.$value['idartistas']?>"><?=$value['nombre']?></a></li>
								</ul>
							</div>			
						<?php endforeach; ?>								
					</div>					
				<?php endif;?>

				<?php if($corrientes) : ?>				
					<div class="row">
						<div class="col-lg-12 ">
							<h3 class="title color5"><b>.Corrientes</b> Art Boomerang</h3>
						</div>
					</div>
					<div class="row">
						<?php foreach ($corrientes as $key => $value) : ?>
							<div class="col-lg-4">
								<ul>
									<li><a href="<?=$url.'artistas/perfil/'.$value['idartistas']?>"><?=$value['nombre']?></a></li>
								</ul>
							</div>			
						<?php endforeach; ?>								
					</div>					
				<?php endif;?>

				<?php if($entre_rios) : ?>				
					<div class="row">
						<div class="col-lg-12 ">
							<h3 class="title color6"><b>.Entre Ríos</b> Art Boomerang</h3>
						</div>
					</div>
					<div class="row">
						<?php foreach ($entre_rios as $key => $value) : ?>
							<div class="col-lg-4">
								<ul>
									<li><a href="<?=$url.'artistas/perfil/'.$value['idartistas']?>"><?=$value['nombre']?></a></li>
								</ul>
							</div>			
						<?php endforeach; ?>								
					</div>					
				<?php endif;?>

				<?php if($formosa) : ?>				
					<div class="row">
						<div class="col-lg-12 ">
							<h3 class="title color7"><b>.Formosa</b> Art Boomerang</h3>
						</div>
					</div>
					<div class="row">
						<?php foreach ($formosa as $key => $value) : ?>
							<div class="col-lg-4">
								<ul>
									<li><a href="<?=$url.'artistas/perfil/'.$value['idartistas']?>"><?=$value['nombre']?></a></li>
								</ul>
							</div>			
						<?php endforeach; ?>								
					</div>					
				<?php endif;?>

				<?php if($jujuy) : ?>				
					<div class="row">
						<div class="col-lg-12 ">
							<h3 class="title color8"><b>.Jujuy</b> Art Boomerang</h3>
						</div>
					</div>
					<div class="row">
						<?php foreach ($jujuy as $key => $value) : ?>
							<div class="col-lg-4">
								<ul>
									<li><a href="<?=$url.'artistas/perfil/'.$value['idartistas']?>"><?=$value['nombre']?></a></li>
								</ul>
							</div>			
						<?php endforeach; ?>								
					</div>					
				<?php endif;?>

				<?php if($la_pampa) : ?>				
					<div class="row">
						<div class="col-lg-12 ">
							<h3 class="title color1"><b>.La Pampa</b> Art Boomerang</h3>
						</div>
					</div>
					<div class="row">
						<?php foreach ($la_pampa as $key => $value) : ?>
							<div class="col-lg-4">
								<ul>
									<li><a href="<?=$url.'artistas/perfil/'.$value['idartistas']?>"><?=$value['nombre']?></a></li>
								</ul>
							</div>			
						<?php endforeach; ?>								
					</div>					
				<?php endif;?>

				<?php if($la_rioja) : ?>				
					<div class="row">
						<div class="col-lg-12 ">
							<h3 class="title color2"><b>.La Rioja</b> Art Boomerang</h3>
						</div>
					</div>
					<div class="row">
						<?php foreach ($la_rioja as $key => $value) : ?>
							<div class="col-lg-4">
								<ul>
									<li><a href="<?=$url.'artistas/perfil/'.$value['idartistas']?>"><?=$value['nombre']?></a></li>
								</ul>
							</div>			
						<?php endforeach; ?>								
					</div>					
				<?php endif;?>

				<?php if($mendoza) : ?>				
					<div class="row">
						<div class="col-lg-12 ">
							<h3 class="title color3"><b>.Mendoza</b> Art Boomerang</h3>
						</div>
					</div>
					<div class="row">
						<?php foreach ($mendoza as $key => $value) : ?>
							<div class="col-lg-4">
								<ul>
									<li><a href="<?=$url.'artistas/perfil/'.$value['idartistas']?>"><?=$value['nombre']?></a></li>
								</ul>
							</div>			
						<?php endforeach; ?>								
					</div>					
				<?php endif;?>

				<?php if($misiones) : ?>				
					<div class="row">
						<div class="col-lg-12 ">
							<h3 class="title color4"><b>.Misiones</b> Art Boomerang</h3>
						</div>
					</div>
					<div class="row">
						<?php foreach ($misiones as $key => $value) : ?>
							<div class="col-lg-4">
								<ul>
									<li><a href="<?=$url.'artistas/perfil/'.$value['idartistas']?>"><?=$value['nombre']?></a></li>
								</ul>
							</div>			
						<?php endforeach; ?>								
					</div>					
				<?php endif;?>

				<?php if($neuquen) : ?>				
					<div class="row">
						<div class="col-lg-12 ">
							<h3 class="title color5"><b>.Neuquén</b> Art Boomerang</h3>
						</div>
					</div>
					<div class="row">
						<?php foreach ($neuquen as $key => $value) : ?>
							<div class="col-lg-4">
								<ul>
									<li><a href="<?=$url.'artistas/perfil/'.$value['idartistas']?>"><?=$value['nombre']?></a></li>
								</ul>
							</div>			
						<?php endforeach; ?>								
					</div>					
				<?php endif;?>

				<?php if($rio_negro) : ?>				
					<div class="row">
						<div class="col-lg-12 ">
							<h3 class="title color6"><b>.Río Negro</b> Art Boomerang</h3>
						</div>
					</div>
					<div class="row">
						<?php foreach ($rio_negro as $key => $value) : ?>
							<div class="col-lg-4">
								<ul>
									<li><a href="<?=$url.'artistas/perfil/'.$value['idartistas']?>"><?=$value['nombre']?></a></li>
								</ul>
							</div>			
						<?php endforeach; ?>								
					</div>					
				<?php endif;?>

				<?php if($salta) : ?>				
					<div class="row">
						<div class="col-lg-12 ">
							<h3 class="title color7"><b>.Salta</b> Art Boomerang</h3>
						</div>
					</div>
					<div class="row">
						<?php foreach ($salta as $key => $value) : ?>
							<div class="col-lg-4">
								<ul>
									<li><a href="<?=$url.'artistas/perfil/'.$value['idartistas']?>"><?=$value['nombre']?></a></li>
								</ul>
							</div>			
						<?php endforeach; ?>								
					</div>					
				<?php endif;?>

				<?php if($san_juan) : ?>				
					<div class="row">
						<div class="col-lg-12 ">
							<h3 class="title color8"><b>.San Juan</b> Art Boomerang</h3>
						</div>
					</div>
					<div class="row">
						<?php foreach ($san_juan as $key => $value) : ?>
							<div class="col-lg-4">
								<ul>
									<li><a href="<?=$url.'artistas/perfil/'.$value['idartistas']?>"><?=$value['nombre']?></a></li>
								</ul>
							</div>			
						<?php endforeach; ?>								
					</div>					
				<?php endif;?>

				<?php if($san_luis) : ?>				
					<div class="row">
						<div class="col-lg-12 ">
							<h3 class="title color1"><b>.San Luis</b> Art Boomerang</h3>
						</div>
					</div>
					<div class="row">
						<?php foreach ($san_luis as $key => $value) : ?>
							<div class="col-lg-4">
								<ul>
									<li><a href="<?=$url.'artistas/perfil/'.$value['idartistas']?>"><?=$value['nombre']?></a></li>
								</ul>
							</div>			
						<?php endforeach; ?>								
					</div>					
				<?php endif;?>

				<?php if($santa_cruz) : ?>				
					<div class="row">
						<div class="col-lg-12 ">
							<h3 class="title color2"><b>.Santa Cruz</b> Art Boomerang</h3>
						</div>
					</div>
					<div class="row">
						<?php foreach ($santa_cruz as $key => $value) : ?>
							<div class="col-lg-4">
								<ul>
									<li><a href="<?=$url.'artistas/perfil/'.$value['idartistas']?>"><?=$value['nombre']?></a></li>
								</ul>
							</div>			
						<?php endforeach; ?>								
					</div>					
				<?php endif;?>

				<?php if($santa_fe) : ?>				
					<div class="row">
						<div class="col-lg-12 ">
							<h3 class="title color3"><b>.Santa Fe</b> Art Boomerang</h3>
						</div>
					</div>
					<div class="row">
						<?php foreach ($santa_fe as $key => $value) : ?>
							<div class="col-lg-4">
								<ul>
									<li><a href="<?=$url.'artistas/perfil/'.$value['idartistas']?>"><?=$value['nombre']?></a></li>
								</ul>
							</div>			
						<?php endforeach; ?>								
					</div>					
				<?php endif;?>

				<?php if($santiago) : ?>				
					<div class="row">
						<div class="col-lg-12 ">
							<h3 class="title color4"><b>.Santiago del Estero</b> Art Boomerang</h3>
						</div>
					</div>
					<div class="row">
						<?php foreach ($santiago as $key => $value) : ?>
							<div class="col-lg-4">
								<ul>
									<li><a href="<?=$url.'artistas/perfil/'.$value['idartistas']?>"><?=$value['nombre']?></a></li>
								</ul>
							</div>			
						<?php endforeach; ?>								
					</div>					
				<?php endif;?>

				<?php if($tdf) : ?>				
					<div class="row">
						<div class="col-lg-12 ">
							<h3 class="title color5"><b>.Tierra del Fuego</b> Art Boomerang</h3>
						</div>
					</div>
					<div class="row">
						<?php foreach ($tdf as $key => $value) : ?>
							<div class="col-lg-4">
								<ul>
									<li><a href="<?=$url.'artistas/perfil/'.$value['idartistas']?>"><?=$value['nombre']?></a></li>
								</ul>
							</div>			
						<?php endforeach; ?>								
					</div>					
				<?php endif;?>

				<?php if($tucuman) : ?>				
					<div class="row">
						<div class="col-lg-12 ">
							<h3 class="title color6"><b>.Tucumán</b> Art Boomerang</h3>
						</div>
					</div>
					<div class="row">
						<?php foreach ($tucuman as $key => $value) : ?>
							<div class="col-lg-4">
								<ul>
									<li><a href="<?=$url.'artistas/perfil/'.$value['idartistas']?>"><?=$value['nombre']?></a></li>
								</ul>
							</div>			
						<?php endforeach; ?>								
					</div>					
				<?php endif;?>

			</article>
			<div class="col-lg-4 aside">				
				<div class="item seguinos">
					<hgroup>
						<h2>Seguinos</h2>
					</hgroup>
					<div class="social">
						<ul>
							<li>
								<a href="http://www.facebook.com/ArtBoomerangProgramaFederalParaLasArtes" target="_blank"><i class="fa fa-facebook"></i></a>
							</li>
							<li>
								<a href="https://twitter.com/art_boomerang" target="_blank"><i class="fa fa-twitter"></i></a>
							</li>
							<li>
								<a href="https://vimeo.com/artboomerang" target="_blank"><i class="fa fa-vimeo"></i></a>
							</li>						
							<li>
								<a href="https://www.youtube.com/channel/UCIaAoj_Be77FV6oUZpkDNww?view_as=subscriber" target="_blank"><i class="fa fa-youtube"></i></a>
							</li>
						</ul>
						<ul>
							<li>
								<a href="https://www.youtube.com/channel/UCIaAoj_Be77FV6oUZpkDNww?view_as=subscriber" target="_blank"><i class="fa fa-pinterest-p"></i></a>								
							</li>
							<li>
								<a href="https://www.youtube.com/channel/UCIaAoj_Be77FV6oUZpkDNww?view_as=subscriber" target="_blank"><i class="fa fa-instagram"></i></a>								
							</li>
						</ul>
					</div>
					<div class="descripcion">
						<a href="http://www.facebook.com/ArtBoomerangProgramaFederalParaLasArtes" target="_blank">www.facebook.com/ArtBoomerang<br>ProgramaFederalParaLasArtes</a>
						<span>@ArtBoomerang</span>
						<span>art.boomerang@gmail.com</span>
					</div>
				</div>
				<div class="item convocatoria">
					<hgroup>
						<h2>Convocatoria</h2>
					</hgroup>
					<div class="download">						
						<div class="text">
							<span>DESCARGAR PDF</span>
							<p>Accede a  las bases y condiciones <br> para formar parte de Art Boomerang</p>
						</div>
						<div class="icon">
							<?php if(!$convocatoria) : ?>
								<a href="#" data-toggle="modal" data-target="#modalConvocatoria"><i class="fa fa-arrow-circle-down"></i></a>
							<?php else : ?>
								<a href="<?=$url?>admin/<?=$convocatoria[0]['path']?>" target="_blank"><i class="fa fa-arrow-circle-down"></i></a>
							<?php endif; ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<?php include('templates/provincias.php'); ?>	
