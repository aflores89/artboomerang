
	<?php include('templates/menu_interna.php'); ?>	

	<section class="wrapp" id="noticias">
		<div class="container">
			<article class="col-lg-8">
				<div class="title">
					<h2>Noticias</h2>
					<span>Últimas noticias de Art Boomerang</span>
				</div>
				<div class="content">
					<?php if(!empty($noticias)) : ?>
					<?php foreach ($noticias as $key => $value) : ?>
					<div class="item">
						<figure>
							<img src="<?=$url.'admin/'.$value['filepath']?>" alt="" class="img-responsive">
						</figure>
						<div class="contenido">
							<div class="titular">							
								<a href="<?=$url.'noticias/'.$value['idnoticias']?>"><?=$value['titular']?></a>
							</div>
							<div class="leads">
								<p>
									<?=$value['lead']?>
								</p>
							</div>
						</div>
						<div class="foot">
							<div class="fecha">
								<span><?=$value['fecha']?></span>
							</div>
							<div class="social">
								<ul>
									<li>
										<a href=""><i class="fa fa-facebook"></i></a>
									</li>
									<li>
										<a href=""><i class="fa fa-twitter"></i></a>
									</li>								
								</ul>
								<div class="clearfix"></div>
							</div>
							<div class="clearfix"></div>
						</div>						
					</div>
					<?php endforeach; ?>		
					<?php endif; ?>		
				</div>
			</article>
			<div class="col-lg-4 aside">				
				<div class="item seguinos">
					<hgroup>
						<h2>Seguinos</h2>
					</hgroup>
					<div class="social">
						<ul>
							<li>
								<a href="http://www.facebook.com/ArtBoomerangProgramaFederalParaLasArtes" target="_blank"><i class="fa fa-facebook"></i></a>
							</li>
							<li>
								<a href="https://twitter.com/art_boomerang" target="_blank"><i class="fa fa-twitter"></i></a>
							</li>
							<li>
								<a href="https://vimeo.com/artboomerang" target="_blank"><i class="fa fa-vimeo"></i></a>
							</li>						
							<li>
								<a href="https://www.youtube.com/channel/UCIaAoj_Be77FV6oUZpkDNww?view_as=subscriber" target="_blank"><i class="fa fa-youtube"></i></a>
							</li>
						</ul>
						<ul>
							<li>
								<a href="https://www.youtube.com/channel/UCIaAoj_Be77FV6oUZpkDNww?view_as=subscriber" target="_blank"><i class="fa fa-pinterest-p"></i></a>								
							</li>
							<li>
								<a href="https://www.youtube.com/channel/UCIaAoj_Be77FV6oUZpkDNww?view_as=subscriber" target="_blank"><i class="fa fa-instagram"></i></a>								
							</li>
						</ul>
					</div>
					<div class="descripcion">
						<a href="http://www.facebook.com/ArtBoomerangProgramaFederalParaLasArtes" target="_blank">www.facebook.com/ArtBoomerang<br>ProgramaFederalParaLasArtes</a>
						<span>@ArtBoomerang</span>
						<span>art.boomerang@gmail.com</span>
					</div>
				</div>
				<div class="item convocatoria">
					<hgroup>
						<h2>Convocatoria</h2>
					</hgroup>
					<div class="download">						
						<div class="text">
							<span>DESCARGAR PDF</span>
							<p>Accede a  las bases y condiciones <br> para formar parte de Art Boomerang</p>
						</div>
						<div class="icon">
							<?php if(!$convocatoria) : ?>
								<a href="#" data-toggle="modal" data-target="#modalConvocatoria"><i class="fa fa-arrow-circle-down"></i></a>
							<?php else : ?>
								<a href="<?=$url?>admin/<?=$convocatoria[0]['path']?>" target="_blank"><i class="fa fa-arrow-circle-down"></i></a>
							<?php endif; ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<?php include('templates/provincias.php'); ?>	