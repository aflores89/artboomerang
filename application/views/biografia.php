	<?php include('templates/menu_interna.php'); ?>

	<section class="wrapp" id="artista" data-artista="true">
		<div class="container">				
			
			<article class="col-lg-8">		
				<div class="title-artista">
					<span>Artistas <b><?=$provincia[0]['nombre']?></b> Art Boomerang</span>
					<h1><?=$artista['nombre']?></h1>
				</div>				
				<div class="content-artista">
					<figure class="visible-xs"><img src="<?=$url.'admin/'.$artista['filepath']?>" alt=""></figure>
					<div class="avatar hidden-xs" style="background-image: url(<?=$url.'admin/'.$artista['filepath']?>)"></div>
					<div class="statment">
						<h3>Statment</h3>
						<p><?=$artista['statment']?></p>
					</div>
					<div class="clearfix"></div>
				</div>
				<h3>.Galeria</h3>
				<div class="galeria">
					<div id="owl-demo" class="owl-carousel owl-theme">
 						<?php foreach ($galeria as $key => $value) : ?>
					  		<div class="item">
					  			<img src="<?=$url.'admin/'.$value['path']?>" alt="The Last of us" class="img-responsive">
					  			<div class="descripcion">
					  				<?=$value['statment']?>
					  			</div>
					  		</div>
					 	<?php endforeach; ?>
					</div>
				</div>		
				<div class="descripcion">
					<h3>.Biografía</h3>
					<p><?=$artista['biografia']?></p>
				</div>	
				<div class="links">
					<h3>.Links</h3>
					<ul>
						<li><a href="#"><?=$artista['url']?></a></li>
					</ul>
				</div>	
			</article>
			<div class="col-lg-4 aside">
				<div class="item artistas text-left">
					<hgroup>
						<h2>Artistas</h2>
					</hgroup>
					<h3><b>.<?=$provincia[0]['nombre']?></b> Art Boomerang</h3>
					<ul>
						<?php foreach ($artistas as $key => $value) : ?>
							<li><a href="<?=$url.'artistas/perfil/'.$value['idartistas']?>"><?=$value['nombre']?></a></li>
						<?php endforeach; ?>
					</ul>
					<hr>
				</div>
				<div class="item seguinos">
					<hgroup>
						<h2>Seguinos</h2>
					</hgroup>
					<div class="social">
						<ul>
							<li>
								<a href="http://www.facebook.com/ArtBoomerangProgramaFederalParaLasArtes" target="_blank"><i class="fa fa-facebook"></i></a>
							</li>
							<li>
								<a href="https://twitter.com/art_boomerang" target="_blank"><i class="fa fa-twitter"></i></a>
							</li>
							<li>
								<a href="https://vimeo.com/artboomerang" target="_blank"><i class="fa fa-vimeo"></i></a>
							</li>						
							<li>
								<a href="https://www.youtube.com/channel/UCIaAoj_Be77FV6oUZpkDNww?view_as=subscriber" target="_blank"><i class="fa fa-youtube"></i></a>
							</li>
						</ul>
						<ul>
							<li>
								<a href="https://www.youtube.com/channel/UCIaAoj_Be77FV6oUZpkDNww?view_as=subscriber" target="_blank"><i class="fa fa-pinterest-p"></i></a>								
							</li>
							<li>
								<a href="https://www.youtube.com/channel/UCIaAoj_Be77FV6oUZpkDNww?view_as=subscriber" target="_blank"><i class="fa fa-instagram"></i></a>								
							</li>
						</ul>
					</div>
					<div class="descripcion">
						<a href="http://www.facebook.com/ArtBoomerangProgramaFederalParaLasArtes" target="_blank">www.facebook.com/ArtBoomerang<br>ProgramaFederalParaLasArtes</a>
						<span>@ArtBoomerang</span>
						<span>art.boomerang@gmail.com</span>
					</div>
				</div>
				<div class="item convocatoria">
					<hgroup>
						<h2>Convocatoria</h2>
					</hgroup>
					<div class="download">						
						<div class="text">
							<span>DESCARGAR PDF</span>
							<p>Accede a  las bases y condiciones <br> para formar parte de Art Boomerang</p>
						</div>
						<div class="icon">
							<?php if(!$convocatoria) : ?>
								<a href="#" data-toggle="modal" data-target="#modalConvocatoria"><i class="fa fa-arrow-circle-down"></i></a>
							<?php else : ?>
								<a href="<?=$url?>admin/<?=$convocatoria[0]['path']?>" target="_blank"><i class="fa fa-arrow-circle-down"></i></a>
							<?php endif; ?>
						</div>
					</div>
				</div>
			</aside>
		</div>
	</section>

	<?php include('templates/provincias.php'); ?>	