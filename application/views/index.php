
	<section id="over">
		<div class="transparencia"></div>
		<div class="slider">
			<ul>
				<li>
					<video id="video" width="100%" height="100%" autoplay loop>
						<source id="mp4" src="assets/videos/MP4/loop_completo.mp4" type="video/mp4">
						<source id="ogg" src="assets/videos/OGG/loop_completo.ogv" type="video/ogg">
						<source id="webm" src="assets/videos/WEBM/loop_completo.webm" type="video/webm">
					</video>
				</li>
			</ul>
		</div>
		<div class="title opacity">
			<hgroup class="text-center">
				<h1>LOS TERRITORIOS <br>DEL ARTE</h1>
				<span>Art Boomerang . Programa Federal para las Artes</span>
			</hgroup>			
		</div>
		<div class="arrow opacity">
			<i class="fa fa-arrow-circle-o-down"></i>
		</div>
	</section>

	<?php include('templates/menu_interna.php'); ?>	

	<section class="wrapp" id="content">
		<div class="container">
			<article class="col-lg-8">
				<div class="title">
					<h2><?=$contenido['titulo']?></h2>
					<span><?=$contenido['subtitulo']?></span>
				</div>
				<div class="content">
					<p><?=nl2br($contenido['contenido'])?></p>
					<div class="bio">
						<div class="image">
							<figure>
								<img src="assets/images/daniel.jpg" alt="">
							</figure>
						</div>
						<div class="info">
							<h2><a href="#">Daniel Fischer</a></h2>
							<span>Director</span>
							<p>Profesor y Curador independiente. Se a formado en Arquitectura y Artes Visuales. Es docente de la Facultad de Artes, Diseño y Ciencia de la Cultura, FADyC y de la Facultad de Arquitectura y Urbanismo de la U.N.N.E. Participa como investigador para la Sociedad de Estudios Morfológicos en Argentina SEMA y Grupo Argentino de Color GAC Ha obtenido becas de la Fundación Telefónica, Fondo Nacional de las Artes, Ciencia y Técnica, Oficina Cultural de la Embajada <a href="<?=$url?>arte">Leer más</a></p>
						</div>
						<div class="clearfix"></div>
					</div>
                    <h3>Marco General</h3>

                    <p>Art Boomerang es un programa que pretende cubrir las necesidades y desarrollo artístico e intelectual de artistas visuales de distintas disciplinas que vivan en Argentina. Primera Etapa</p>
                    <p>También, integrar las diferentes miradas de nuestro país, por medio del intercambio artístico con otros territorios (Zona Norte, Zona Centro y Zona Sur) que participen del proyecto. Segunda Etapa.</p>
                    <p>Art Boomerang intenta constituirse como una red y usina, capas de dar respuesta a las motivaciones personales que se presenten desde el trabajo artístico como así también contribuir al crecimiento profesional. <br> Dicho proyecto trabajará sobre el análisis crítico de las producciones de los artistas seleccionados, para posibilitar respuestas que surgen de diálogos diversos en donde los participantes intercambian sus miradas.</p>

                    <h3>Dirigido A:</h3>

                    <p>Artistas visuales de todo el país y de todas las edades, pertenecientes a las siguientes disciplinas: pintura, escultura, objetos, fotografía, grabado, instalación, diseño o artes expandidas, y en general, todas aquellas que se inscriban en el rango de Artes Visuales. Asimismo, podrán aplicar al programa personas que tengasn trabajos o profesiones que se vinculen con las artes.</p>

                    <dl>
                        <dt>Dirección General</dt>
                        <dd>Curador Arq. Daniel Fischer</dd>
                        <dt>Coordinación</dt>
                        <dd>Prof. Ana María Goycochea</dd>
                        <dt>Recursos Humanos</dt>
                        <dd>Leopoldo Fieg</dd>
                        <dt>Contacto</dt>
                        <dd>programa.artboomerang@gmail.com</dd>
                    </dl>
					<!--<div class="curadores">
						<div class="col-lg-3">
							<div class="curador">
								<figure>
									<img src="assets/images/daniel.jpg" alt="" class="img-responsive">
								</figure>
								<div class="personal">
									<h2>Rodrigo Alonso</h2>
									<span class="detalle">Curador . Buenos Aires</span>
								</div>
							</div>
						</div>
						<div class="col-lg-3 col-lg-push-1">
							<div class="curador">
								<figure>
									<img src="assets/images/daniel.jpg" alt="" class="img-responsive">
								</figure>
								<div class="personal">
									<h2>Carina Cagnolo</h2>
									<span class="detalle">Curadora . Córdoba</span>
								</div>
							</div>
						</div>
						<div class="col-lg-3 col-lg-push-2">
							<div class="curador">
								<figure>
									<img src="assets/images/daniel.jpg" alt="" class="img-responsive">
								</figure>
								<div class="personal">
									<h2>Kekena Corvalan</h2>
									<span class="detalle">Curadora . Buenos Aires</span>
								</div>
							</div>
						</div>
					</div>		-->	
				</div>
			</article>
			<div class="col-lg-4 aside">				
				<div class="item seguinos">
					<hgroup>
						<h2>Seguinos</h2>
					</hgroup>
					<div class="social">
						<ul>
							<li>
								<a href="http://www.facebook.com/ArtBoomerangProgramaFederalParaLasArtes" target="_blank"><i class="fa fa-facebook"></i></a>
							</li>
							<li>
								<a href="https://twitter.com/art_boomerang" target="_blank"><i class="fa fa-twitter"></i></a>
							</li>
							<li>
								<a href="https://vimeo.com/artboomerang" target="_blank"><i class="fa fa-vimeo"></i></a>
							</li>						
							<li>
								<a href="https://www.youtube.com/channel/UCIaAoj_Be77FV6oUZpkDNww?view_as=subscriber" target="_blank"><i class="fa fa-youtube"></i></a>
							</li>
						</ul>
						<ul>
							<li>
								<a href="https://www.youtube.com/channel/UCIaAoj_Be77FV6oUZpkDNww?view_as=subscriber" target="_blank"><i class="fa fa-pinterest-p"></i></a>								
							</li>
							<li>
								<a href="https://www.youtube.com/channel/UCIaAoj_Be77FV6oUZpkDNww?view_as=subscriber" target="_blank"><i class="fa fa-instagram"></i></a>								
							</li>
						</ul>
					</div>
					<div class="descripcion">
						<a href="http://www.facebook.com/ArtBoomerangProgramaFederalParaLasArtes" target="_blank">www.facebook.com/ArtBoomerang<br>ProgramaFederalParaLasArtes</a>
						<span>@ArtBoomerang</span>
						<span>art.boomerang@gmail.com</span>
					</div>					
				</div>
				<div class="item convocatoria">
					<hgroup>
						<h2>Convocatoria</h2>
					</hgroup>
					<div class="download">						
						<div class="text">
							<span>DESCARGAR PDF</span>
							<p>Accede a  las bases y condiciones <br> para formar parte de Art Boomerang</p>
						</div>
						<div class="icon">
							<?php if(!$convocatoria) : ?>
								<a href="#" data-toggle="modal" data-target="#modalConvocatoria"><i class="fa fa-arrow-circle-down"></i></a>
							<?php else : ?>
								<a href="<?=$url?>admin/<?=$convocatoria[0]['path']?>" target="_blank"><i class="fa fa-arrow-circle-down"></i></a>
							<?php endif; ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<?php include('templates/provincias.php'); ?>	
