
	<?php include('templates/menu_interna.php'); ?>	

	<section class="wrapp curador" id="content">
		<div class="container" >
			<article class="col-lg-8">
				<div class="title">
					<h2><?=$curador[0]['nombre']?></h2>
					<span>Curadores Art Boomerang</span>
				</div>
				<div class="content">
					<!--<p>Art Boomerang, es un programa que pretende cubrir las necesidades y desarrollo artístico e intelectual de artistas visuales de distintas disciplinas que vivan en Argentina. Primera etapa.</p>
					<p>También, integrar las diferentes miradas de nuestro país, por medio del intercambio artístico con otros territorios (Zona Norte, Zona Centro y Zona Sur) que participen del proyecto. Segunda Etapa.</p>
					<p>Art Boomerang intenta constituirse como una red y usina, capas de dar respuesta a las motivaciones personales que se presenten desde el trabajo artístico como así también contribuir al crecimiento profesional. <br> Dicho proyecto trabajará sobre el análisis crítico de las producciones de los artistas seleccionados, para posibilitar respuestas que surgen de diálogos diversos en donde los participantes intercambian sus miradas.</p>					
					<div class="bio">
						<div class="image">
							<figure>
								<img src="assets/images/daniel.jpg" alt="">
							</figure>
						</div>
						<div class="info">
							<h2><a href="#">Daniel Fischer</a></h2>
							<span>Director</span>
							<p>Profesor y Curador independiente. Se a formado en Arquitectura y Artes Visuales. Es docente de la Facultad de Artes, Diseño y Ciencia de la Cultura, FADyC y de la Facultad de Arquitectura y Urbanismo de la U.N.N.E. Participa como investigador para la Sociedad de Estudios Morfológicos en Argentina SEMA y Grupo Argentino de Color GAC Ha obtenido becas de la Fundación Telefónica, Fondo Nacional de las Artes, Ciencia y Técnica, Oficina Cultural de la Embajada <a href="<?=$url?>arte">Leer más</a></p>
						</div>
						<div class="clearfix"></div>
					</div>-->
									  					  
					<div class="col-lg-6 image">
						<img src="<?=$url.'admin/'.$curador[0]['imagen']?>" class="img-responsive" alt="<?=$curador[0]['nombre']?>">
					</div>
					<div class="col-lg-6 descripcion">
						<p> <?=$curador[0]['extracto']?></p>						
					</div>
					<div class="col-lg-12 nopadding">
						<a href="<?=$url.'arte'?>">Volver</a>
					</div>
				</div>
			</article>
			<div class="col-lg-4 aside">				
				<div class="item seguinos">
					<hgroup>
						<h2>Seguinos</h2>
					</hgroup>
					<div class="social">
						<ul>
							<li>
								<a href="http://www.facebook.com/ArtBoomerangProgramaFederalParaLasArtes" target="_blank"><i class="fa fa-facebook"></i></a>
							</li>
							<li>
								<a href="https://twitter.com/art_boomerang" target="_blank"><i class="fa fa-twitter"></i></a>
							</li>
							<li>
								<a href="https://vimeo.com/artboomerang" target="_blank"><i class="fa fa-vimeo"></i></a>
							</li>						
							<li>
								<a href="https://www.youtube.com/channel/UCIaAoj_Be77FV6oUZpkDNww?view_as=subscriber" target="_blank"><i class="fa fa-youtube"></i></a>
							</li>
						</ul>
						<ul>
							<li>
								<a href="https://www.youtube.com/channel/UCIaAoj_Be77FV6oUZpkDNww?view_as=subscriber" target="_blank"><i class="fa fa-pinterest-p"></i></a>								
							</li>
							<li>
								<a href="https://www.youtube.com/channel/UCIaAoj_Be77FV6oUZpkDNww?view_as=subscriber" target="_blank"><i class="fa fa-instagram"></i></a>								
							</li>
						</ul>
					</div>
					<div class="descripcion">
						<a href="http://www.facebook.com/ArtBoomerangProgramaFederalParaLasArtes" target="_blank">www.facebook.com/ArtBoomerang<br>ProgramaFederalParaLasArtes</a>
						<span>@ArtBoomerang</span>
						<span>art.boomerang@gmail.com</span>
					</div>
				</div>
				<div class="item convocatoria">
					<hgroup>
						<h2>Convocatoria</h2>
					</hgroup>
					<div class="download">						
						<div class="text">
							<span>DESCARGAR PDF</span>
							<p>Accede a  las bases y condiciones <br> para formar parte de Art Boomerang</p>
						</div>
						<div class="icon">
							<?php if(!$convocatoria) : ?>
								<a href="#" data-toggle="modal" data-target="#modalConvocatoria"><i class="fa fa-arrow-circle-down"></i></a>
							<?php else : ?>
								<a href="<?=$url?>admin/<?=$convocatoria[0]['path']?>" target="_blank"><i class="fa fa-arrow-circle-down"></i></a>
							<?php endif; ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<?php include('templates/provincias.php'); ?>	
	