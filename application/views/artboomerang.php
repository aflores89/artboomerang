
	<?php include('templates/menu_interna.php'); ?>

	<section class="wrapp" id="content">
		<div class="container">
			<article class="col-lg-8">
				<div class="title">
					<h2>Art Boomerang</h2>
					<span>Referentes</span>
				</div>
				<div class="content">
					<div class="bio">
						<div class="image">
							<figure>
								<img src="assets/images/daniel.jpg" alt="">
							</figure>
						</div>
						<div class="info">
							<h2><a href="#">Daniel Fischer</a></h2>
							<span>Curador / Coordinador</span>
							<p>Profesor y Curador independiente. Se a formado en Arquitectura y Artes Visuales. Es docente de la Facultad de Artes, Diseño y Ciencia de la Cultura, FADyC y de la Facultad de Arquitectura y Urbanismo de la U.N.N.E. Participa como investigador para la Sociedad de Estudios Morfológicos en Argentina SEMA y Grupo Argentino de Color GAC Ha obtenido becas de la Fundación Telefónica, Fondo Nacional de las Artes, Ciencia y Técnica, Oficina Cultural de la Embajada d</p>
						</div>
						<div class="clearfix"></div>						
					</div>					
					
					<?php if($curadores)  : ?>
					<h3>Curadores Invitados.</h3>
						<div id="section-curadores">
							<?php foreach ($curadores as $key => $value) : ?>
								<div class="bio">
									<div class="image">
										<figure>
											<img src="<?=$url.'admin/'.$value['imagen']?>" alt="">
										</figure>
									</div>
									<div class="info">
										<h2><a href="<?=$url.'curadores/'.$value['idcuradores']?>"><?=$value['nombre']?></a></h2>
										<span><?=$value['subtitulo']?></span>
									</div>
									<div class="clearfix"></div>
								</div>
							<?php endforeach; ?>
						</div>
					<?php endif;?>



					<!--<div class="curadores">
						<div class="col-lg-3">
							<div class="curador">
								<figure>
									<img src="assets/images/daniel.jpg" alt="" class="img-responsive">
								</figure>
								<div class="personal">
									<h2>Rodrigo Alonso</h2>
									<span class="detalle">Curador . Buenos Aires</span>
								</div>
							</div>
						</div>
						<div class="col-lg-3 col-lg-push-1">
							<div class="curador">
								<figure>
									<img src="assets/images/daniel.jpg" alt="" class="img-responsive">
								</figure>
								<div class="personal">
									<h2>Carina Cagnolo</h2>
									<span class="detalle">Curadora . Córdoba</span>
								</div>
							</div>
						</div>
						<div class="col-lg-3 col-lg-push-2">
							<div class="curador">
								<figure>
									<img src="assets/images/daniel.jpg" alt="" class="img-responsive">
								</figure>
								<div class="personal">
									<h2>Kekena Corvalan</h2>
									<span class="detalle">Curadora . Buenos Aires</span>
								</div>
							</div>
						</div>
					</div>		-->	
				</div>
			</article>
			<div class="col-lg-4 aside">				
				<div class="item seguinos">
					<hgroup>
						<h2>Seguinos</h2>
					</hgroup>
					<div class="social">
						<ul>
							<li>
								<a href="http://www.facebook.com/ArtBoomerangProgramaFederalParaLasArtes" target="_blank"><i class="fa fa-facebook"></i></a>
							</li>
							<li>
								<a href="https://twitter.com/art_boomerang" target="_blank"><i class="fa fa-twitter"></i></a>
							</li>
							<li>
								<a href="https://vimeo.com/artboomerang" target="_blank"><i class="fa fa-vimeo"></i></a>
							</li>						
							<li>
								<a href="https://www.youtube.com/channel/UCIaAoj_Be77FV6oUZpkDNww?view_as=subscriber" target="_blank"><i class="fa fa-youtube"></i></a>
							</li>
						</ul>
						<ul>
							<li>
								<a href="https://www.youtube.com/channel/UCIaAoj_Be77FV6oUZpkDNww?view_as=subscriber" target="_blank"><i class="fa fa-pinterest-p"></i></a>								
							</li>
							<li>
								<a href="https://www.youtube.com/channel/UCIaAoj_Be77FV6oUZpkDNww?view_as=subscriber" target="_blank"><i class="fa fa-instagram"></i></a>								
							</li>
						</ul>
					</div>
					<div class="descripcion">
						<a href="http://www.facebook.com/ArtBoomerangProgramaFederalParaLasArtes" target="_blank">www.facebook.com/ArtBoomerang<br>ProgramaFederalParaLasArtes</a>
						<span>@ArtBoomerang</span>
						<span>art.boomerang@gmail.com</span>
					</div>
				</div>
				<div class="item convocatoria">
					<hgroup>
						<h2>Convocatoria</h2>
					</hgroup>
					<div class="download">						
						<div class="text">
							<span>DESCARGAR PDF</span>
							<p>Accede a  las bases y condiciones <br> para formar parte de Art Boomerang</p>
						</div>
						<div class="icon">
							<?php if(!$convocatoria) : ?>
								<a href="#" data-toggle="modal" data-target="#modalConvocatoria"><i class="fa fa-arrow-circle-down"></i></a>
							<?php else : ?>
								<a href="<?=$url?>admin/<?=$convocatoria[0]['path']?>" target="_blank"><i class="fa fa-arrow-circle-down"></i></a>
							<?php endif; ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<?php include('templates/provincias.php'); ?>	